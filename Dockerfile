FROM image-mirror-prod-registry.cloud.duke.edu/library/ruby:3.1 AS base
#LABEL org.opencontainers.image.authors="derrek.croney@duke.edu"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
     ca-certificates \
     git \
     wait-for-it \
     curl \
     vim \
     libxml2-utils \
     golang \
  && rm -rf /var/lib/apt/lists/*

RUN gem install bundler

# CURRENTLY UNUSED
ARG MTA_BRANCH=main
ARG SPOFFORD_BRANCH=main

RUN mkdir -p /build-mta /build-spofford/lib/spofford/client /gems

# build marc-to-argot
WORKDIR /opt
RUN git clone https://github.com/trln/marc-to-argot.git
WORKDIR /opt/marc-to-argot
RUN cp Gemfile /build-mta/ && \
    cp VERSION /build-mta/ && \
    cp marc_to_argot.gemspec /build-mta/
WORKDIR /build-mta
RUN bundle install
WORKDIR /opt/marc-to-argot
RUN bundle exec rake install

# build spofford-client
WORKDIR /opt
RUN git clone https://github.com/trln/spofford-client.git
WORKDIR /opt/spofford-client
RUN cp Gemfile /build-spofford/ && \
    cp VERSION /build-spofford/ && \
    cp lib/spofford/client/version.rb /build-spofford/lib/spofford/client/version.rb
RUN cp spofford-client.gemspec /build-spofford/
WORKDIR /build-spofford
RUN bundle install
WORKDIR /opt/spofford-client
RUN bundle exec rake install

# Copy the send-to-trln-solr.sh Bash script
WORKDIR /usr/local/bin

COPY pipeline.sh .
COPY files-in-harvest.sh .
COPY join-argot-files.sh .
COPY run-argot .
COPY argot.sh .
COPY run-ingest .
COPY ingest.sh .
COPY remove-old-files .
COPY clean-discovery-dir.sh .
COPY make-primo-dirs.sh .

WORKDIR /usr/bin/local/scripts
COPY scripts/liblog.sh .

RUN chmod -R a+x /usr/local/bin/*

FROM base AS cli

# These ENV variables are used by the go compiler
# ensuring we build on the appropriate architecture
ENV GO111MODULE=on \
  CGO_ENABLED=0 \
  GOOS=linux \
  GOARCH=amd64

WORKDIR /usr/src/go
ENV GOPATH=/usr/src/go
RUN echo $GOPATH
RUN export GOPATH=/usr/src/go

WORKDIR /etc/
WORKDIR /usr/src/go/oai_pmh_harvest
COPY oai_pmh_harvest/*.go ./
COPY oai_pmh_harvest/*.sum .
COPY oai_pmh_harvest/locations.json .
RUN go mod init gitlab.oit.duke.edu/alma-integrations/discovery-integration/oai_pmh_harvest
RUN go get gitlab.oit.duke.edu/dul-go/dsc-credentials@latest
RUN go mod tidy
RUN go build . && cp oai_pmh_harvest /usr/local/bin/oai_pmh_harvest

# AVAILABILITY (post primo-publishing)
WORKDIR /usr/src/go/availability
COPY availability/*.go ./
WORKDIR /usr/src/go/availability/movePrimoPublished
COPY availability/movePrimoPublished/*.go ./
WORKDIR /usr/src/go/availability

RUN ls -la /usr/src/go/availability
RUN go mod init gitlab.oit.duke.edu/alma-integrations/discovery-integration/availability
RUN go get gitlab.oit.duke.edu/dul-go/dsc-credentials@latest
RUN go mod tidy
RUN go build . && cp availability /usr/local/bin/availability

# moveToPod
WORKDIR /usr/src/go/movePodFiles
COPY movePodFiles/*.go ./
RUN go mod init gitlab.oit.duke.edu/alma-integrations/discovery-integration/movePodFiles
RUN go mod tidy
RUN go build -o movePodFiles .
RUN cp movePodFiles /usr/local/bin/movePodFiles

WORKDIR /usr/local/etc/
COPY aws_credentials ./
RUN cat /usr/local/etc/aws_credentials

# Copy the entrypoint script into the container
WORKDIR /usr/local/bin
COPY entrypoint.sh .

# Give execute permissions to the entrypoint script
RUN chmod +x entrypoint.sh

EXPOSE 4000

# "dlc32" note
# I'm trying to recall the purpose of this line
RUN mkdir -p /app/output && chmod +rwx /app/output

# ...these lines, as well.
WORKDIR /opt/dul
RUN chmod -R a+rw /opt/dul

WORKDIR /app
COPY oai_pmh_harvest/locations.json .
RUN chmod +rw /app
RUN chown 1001:1001 /app

COPY oai_pmh_harvest/oph_config.yml .
COPY transport/transport.yml .
COPY availability/availability.yml .

RUN mkdir -m 0777 -p logs/movePrimo
# copy the 'summon-over-alma' config when ready

WORKDIR /opt/dul
RUN chmod -R a+rw /opt/dul

USER 1001

# This is the local current working directory
# when the container (running this image) starts up.
# 
# our docker-compose files will have folders mounted at this location
# like "enrichX", "harvestX", etc, etc
WORKDIR /app

# The entrypoint script (referenced here) will have the following 
# commands (but not limited to):
# - harvest
# - enrich <folder-number>
# - primo
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

