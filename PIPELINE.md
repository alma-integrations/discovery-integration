# DUL LSIS Pipeline Flowchart

## CI Pipeline: Harvest
```mermaid
flowchart TD
    HarvestStart[Start] --> RemoveTokens[Remove Previous API Tokens]
    RemoveTokens --> TSLabel[Create Timestamp label]
    TSLabel-- use TS string as arg -->PrintComposeFile[[Print docker compose file]]
    PrintComposeFile-.-VerifyFile(Verify file contents)
    PrintComposeFile --> RunHarvest[[Docker: Run Harvest Job]]
    RunHarvest --> PrepSOA(Create Timestamp Folder)
    PrepSOA --> MakeSOADirs[[Make dirs used for SOA Enrich]]
    MakeSOADirs --> HarvestEnd[End]
```

## CI Pipeline: SOA Enrich
```mermaid
flowchart TD
    SoaStart[Start] --> JobReady{Job Available?}
    JobReady-->|No| SoaDone[End]
    JobReady-->|Yes| SoaEnrich[[Run SOA Enrich]]
    SoaEnrich --> MoveFolders["Move 'enriched' dirs to ingest queue"]
    MoveFolders-->SoaDone[End]

```
  
**Note:**  
We need to define what `Job Available?` means:  
* If timestamp-labelled folders are located under `alma-share/pipeline/soa-enrich`,
* * Locate the oldest directory
