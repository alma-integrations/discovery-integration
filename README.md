# Discovery Integration
[[_TOC_]]
## Preamble
TRLN is the Triangle Research Libraries Network, POD is the Platform for Open Discovery through IvyPlus, and Summon is an ExLibris tool that enables patrons to search on our library catalog website across all of our electronic resources as well as library catalog holdings.

## Harvesting Files From OAI
Coming soon.

## Enriching Harvested Files with Summon-Over-Alma (SoA) Data
Coming soon.

## Tranforming MARC and Sending to TRLN Solr

### Important Files
#### `argot.sh <TIMESTAMP_DIRNAME> [-options] (NEW)`
* Transform MARC XML files located in `enrichNN` directories into argot-flavored JSON files
* Combines up to 5 individual JSON files by default (use `--disable-batch` to disable)
* A marker file, `.argot` is used to keep track of recently processed XML files.

#### `ingest.sh <trln|sandbox> [-options]` (NEW)
Send argot-flavored JSON files to one of our known Spofford instances.  
By default, the script will send 5 transactions in a batch, then 
pause 20 minutes before continuing.  
  
***Notes***
* The script will look for JSON files under `/share/alma-pipeline/ingest-queue/<trln|sandbox>`.  
* A marker file, `.ingested` will be created and used to keep track of which files have been processed. If the script exits 
before all files have been sent, `ingest.sh` will resume where it left off.

**Argument(s)**  
* Solr instance: `trln` (default) or `sandbox`

**Options**
| OPTION | NOTES |
| ------ | ------ |
| `-I, --ingest-interval`       | Amount of "sleep" time between batch ingests (defaults: `20m`)       |
| `-i, --ingest-batch-count`       | Number of transactions per "batch" (default: `5`)       |
| `-B, --disable-batch`         | Send transactions one at a time, *preferred for incrementals* (default: `false`)  |
| `-C, --show-config`           | Display configured settings, then exit |
| `-T, --stop-ingest-at=<time>` | Stop ingests at this time (e.g. `2024-11-01 00:00:00`).<br />Use `STOP_INGEST_AT` env variable for "pipeline" jobs. |
| `-f, --file-ext`              | File extension for search "glob" (default: `json`)      |
| `-l, --local-run`             | Run the job using files in your local (project) directory. See below. |
##### Local (Dev) Use
Create an `ingest-queue/<trln|sandox>` directory at `/path/to/discovery-integration`.  
  
Then, `$ ./ingest.sh <trln|sandbox> -l ...`

#### `pipeline.sh [options] <DIR-WITH-XML-FILES>`
```bash
-A, --argot-dir=<some-dir>  # (default: "argot" / if it doesn't exist, it will be created under <DIR-WITH-XML-FILES>  
-o, --outputdir             # (NOT USED / DEPRECATED)
-s, --skip-argot            # do not transform MARC to Argot
-b, --bypass-ingest         # do not ingest anything
-S, --spofford-cfg          # specify the spofford client config file/settings / default: .spofford-client.yml)
-d, --dry-run
```

#### `print-harvest-compose.sh`
  
This script is intended to run on `lsis-ci.lib.duke.edu` as part of a Gitlab Runner job.  
It will create a `/alma/pipeline/harvest/yyyy-mm-dd-HH-MM-SS` folder, then write out a 
`docker-compose` file for you.
  
```bash
$ print-harvest-compose.sh

-D, --alma-pipeline-dir     # path to alma 'pipeline' directory (default, "/share/alma-pipeline")
-M, --dir-create-mode       # permissions for harvest/yyyy-mm-dd-H-M-S directory (default "0777")
-o, --output-file           # write output this file
-N, --no-create-file        # don't create a file, write to STDOUT (default "false")
```
  

### Script Examples

#### Preparing Files for Ingest (`marc-to-argot`)
Let's assume SoA "enriched" XML files are located in `alma/pipeline/send-to-index`:  
  
```bash
$ ./pipeline.sh -b alma/pipeline/send-to-index
```
  
**When I want to have the job run in the background...**  
  
```bash
$ ./pipeline.sh -b alma/pipeline/send-to-index > ~/sendindex.log 2>&1 &
```

#### Sending Files To Solr

##### Spofford Config file for production use
I have placed a `spofford-prod.yml` at `alma/data/conf/spofford-prod.yml`. You'll need this file when sending requests to 
the TRLN ingest app for processing.  
  
```bash
$ ./pipeline.sh --skip-argot --spofford-cfg alma/data/conf/spofford-prod.yml alma/pipeline/send-to-index
```
  
**Run in the background like this...**  
  
```bash
./pipeline.sh --skip-argot --spofford-cfg alma/data/conf/spofford-prod.yml alma/pipeline/send-to-index > sendindex.log 2>&1 &
```


## Timing of Individual Parts

- Primo Publishing: ~9.5 hours [05/22/2024]
- Extracting Data from Primo to DynamoJSON: ~1 hour [05/25/2024]
- Upload to S3 and DynamoDB: ~1 hour [05/29/2024]
- OAI-PMH Harvesting: 11 hours [05/27/2024]

## Added fields and fields of note

- 852 8 and 940e: Holding ID
- 940t: totalItems
- 852x: availability

## Legacy Documentation

[Confluence](https://duldev.atlassian.net/wiki/spaces/DSTP/pages/3368550401)

### Running the OAI-PMH Harvest script

    git clone git@gitlab.oit.duke.edu:alma-integrations/discovery-integration.git
    cd discovery-integration
    go run oai_pmh_harvest.go

The "Max Cycles" constant sets how many pages of 100 records will be harvested.

The output format is a .xml file saved to the project `./output` directory.

#### Containerized Version

Build and start the container:
```
git clone git@gitlab.oit.duke.edu:alma-integrations/discovery-integration.git
cd discovery-integration
docker build -t oai_pmh_harvest .
docker run --name oai_pmh oai_pmh_harvest
```

Open another terminal tab and shell into the container to view the harvest:
```
docker exec -u 0 -it oai_pmh /bin/bash
xmllint --format output/oai_pmh_records.xml
```

### Summon Over Alma API

```
curl https://soaduke.summon.serialssolutions.com/2.0.0/availability/W8JY2QR8FC\?s.id\=01DUKE_INST_CAT%20{SOA ID}
// for example
curl https://soaduke.summon.serialssolutions.com/2.0.0/availability/W8JY2QR8FC\?s.id\=01DUKE_INST_CAT%2021119121790008501
```

### SFTP

```
sftp alma_01duke_inst@customers.na%40custdata-dc01.hosted.exlibrisgroup.com:10022
```
## Gitlab CI/CD Notes (.gitlab-ci.yml)
### Jobs
Currently one(1) job is configured -- `harvest`
### CI Variables (Settings > CI/CD > Variables)
`aws_credentials` - credentials needed for AWS connection, used in `.gitlab-ci.yml`
