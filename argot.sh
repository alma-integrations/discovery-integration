#!/bin/bash

# argot.sh [-options] TIMESTAMP_DIRNAME

# originating author: Derrek Croney (dlc32)
# contributing author(s): Stewart Engart (hse9)

# ARGUMENT(S):
# TIMESTAMP_DIRNAME - the "TIMESTAMP_DIRNAME" location under /share/alma-pipeline/soa-enrich.
#                     e.g. 2024-09-18-09-00-00
#
# The script will glob for all enrich*/*.xml files, and run "mta" (marc-to-argot) for each, 
# leaving the resulting .JSON file at /share/alma-pipeline/ingest-queue/SOLR_INSTANCE_DIR
#
# During the processing run, a ".argot" file will be created at:
# /share/alma-pipeline/soa-enrich/WHERE/.argot
# This file will be used to keep track of which files have already been processed.
#
# use the --reset-marker (-R) flag to remove this marker file.

ALMA_PIPELINE_SHARE="/share/alma-pipeline"

# If the script's "run" file is present, then exit and allow the current job
# to complete
if [ -e "$ALMA_PIPELINE_SHARE/run/argot.run" ]; then
  echo "there is an 'argot' job already in progress. exiting..."
  exit 0
fi

# Get the basename for logging purposes
[ -e "./scripts/liblog.sh" ] && . ./scripts/liblog.sh
[ ! -e "./scripts/liblog.sh" ] && . /usr/local/bin/scripts/liblog.sh

filename=$(basename "$0")

# Process flags first (getopt)
SHORTOPTS="A:df:CRlBSh"
LONGOPTS="argot-dir:,dry-run,file-ext:,show-config,reset-marker,local-run,disable-batch,sandbox,box-number:,help"
OPTS=`getopt -o ${SHORTOPTS} -l ${LONGOPTS} -n '${filename}' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

# The 'argot' directory is relative to $WHERE (aka the argument passed to us)
# DEPRECATED
ARGOTDIR="generated-argot"

# When ingesting argot files, this is the default extension for our file (search) glob
FILE_EXT="xml"

# FLAGS and VARIABLES
# Some of these may have been defined outside this script,
# so use those values -- otherwise, provide default values.
DRY_RUN=${DRY_RUN:-false}
SHOW_CONFIG=${SHOW_CONFIG:-false}
ARGOT_FILES_TO_JOIN=${ARGOT_FILES_TO_JOIN:-5}
SOLR_INSTANCE_DIR=${SOLR_INSTANCE_DIR:-trln}
RESET_MARKER=${RESET_MARKER:-false}
DISABLE_BATCH=${DISABLE_BATCH:-false}
BATCH_COUNT=${BATCH_COUNT:-5}
LOCAL_RUN=false
BOX_NUMBER=${BOX_NUMBER:-false}
XTRA_FIND_ARGS=""

usage() {
  # Display help
  echo "Initiate ingest of records to a Solr index via Spofford"
  echo
  echo "Syntax: [$filename] [-options] TIMESTAMP_DIRNAME"
  echo "where TIMESTAMP_DIRNAME is something like 2024-09-18-09-00-00"
  echo "Options:"
  echo "  -i, --argot-batch-count    Number of files to transform in batch mode (${ARGOT_FILE_TO_JOIN})"
  echo "  -B, --disable-batch         Start transactions without stopping"
  echo "  -f, --file-ext              Look for Argot files with this file extension (${FILE_EXT})"
  echo "  -l, --local-run             Run in developer's local setup"
  echo "  -C, --show-config           Show the script's configuration, then exit"
  echo "  -S, --sandbox               Store files under ALMA_PIPELINE_SHARE/ingest-queue/sandbox"
  echo "  -d, --dry-run               Do a dry run of this operation"
  echo "  -h, --help                  Show this help, then exit"
  echo
}

while true; do
  case "$1" in
    -A | --argot-dir ) ARGOTDIR="$2"; shift 2;;
    -d | --dry-run ) DRY_RUN=true; shift ;;
    -f | --file-ext ) FILE_EXT="$2"; shift 2;;
    -C | --show-config ) SHOW_CONFIG=true; shift;;
    -B | --disable-batch ) DISABLE_BATCH=true; shift;;
    -R | --reset-marker ) RESET_MARKER=true; shift;;
    -l | --local-run ) ALMA_PIPELINE_SHARE="."; shift;;
    -S | --sandbox ) SOLR_INSTANCE_DIR="sandbox"; shift;;
    -h | --help ) usage; exit 0 ;;
    --box-number ) BOX_NUMBER=$2; XTRA_FIND_ARGS="-path \"*enrich${2}*\""; shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [ "$#" -ne 1 ]; then
  echo "TIMESTAMP_DIRNAME is required (e.g. 2024-09-18-09-00-00)"
  exit 1
fi

########## SIGNAL TRAPPING #############
function handle_ctrlc()
{
  rm ${ALMA_PIPELINE_SHARE}/run/argot.run
  IFS=$OFS
}

# trap the SIGINT signal
trap handle_ctrlc SIGINT

# Concatenate several files (represented by array) into one file
# Remove the individual files
function merge_files() {
  local dest_file="$1"
  shift
  local arr=("$@")
  cat "${arr[@]}" > $dest_file
  rm "${arr[@]}"
}

function print_last_line() {
  local string=$1
  local IFS='\n'
  read -r -a lines <<< "$string"
  info "$lines[${#lines[@]}]"
}

# $1 represents the parent TIMESTAMP_DIRNAME holding XML files
TIMESTAMP_DIRNAME=$1
WHERE=$ALMA_PIPELINE_SHARE/soa-enrich/$1

INGEST_QUEUE="$ALMA_PIPELINE_SHARE/ingest-queue/$SOLR_INSTANCE_DIR"

# Currently, this Bash script will process XML files located in 
# the directory provided in the first argument $1

[ "$RESET_MARKER" == true ] && rm $WHERE/.argot

RESUMING=false
[[ -f "$WHERE/.argot" ]] && RESUMING=true
[[ ! -f "$WHERE/.argot" ]] && touch "$WHERE/.argot"

info "WHERE = [$WHERE]"
info "BOX_NUMBER = [$BOX_NUMBER]"
info "XTRA_FIND_ARGS = [$XTRA_FIND_ARGS]"
info "INGEST_QUEUE = [$INGEST_QUEUE]"
info "RESUMING = [$RESUMING]"

[ "$SHOW_CONFIG" == true ] && exit 0;

echo $(date "+%s") > ${ALMA_PIPELINE_SHARE}/run/argot.run

# Adjust the Input File Separator so we can glob a file listing
OFS=$IFS
IFS=$'\n'

# starting with the 2024-09-18-09-00-00 data load, 
# the 'ingest-queue' directory includes a "formatted-xml" 
# directory where all formatted XML files are sent to.
#
# make sure we don't include those XML files in this search.

FIND_WHERE=$WHERE
[ "$BOX_NUMBER" != false ] && FIND_WHERE="$WHERE/enrich${BOX_NUMBER}"
paths=($(find $FIND_WHERE -name "*.xml" ! -path "*harvest*" ! -path "*formatted*"))

info "processing ${#paths[@]} XML files / starting..."

# We'll create two(2) "linted" XML files from the original enriched XML
# 1) a non-human-formatted file, used for marc-to-argot transformation,
#    to be removed after MTA
# 2) a human-formatted file, one we'll "keep" for our "records"
counter=1
how_many_files=${#paths[@]}

# keep track of the files processed.
# we'll use this value in mod division throughout the run
files_processed=0

# Add the paths of files we intend to join (concat) 
# in this array
big_argot=()

########## BIG 'OL PROCESSING LOOP ##########
for (( i = 0; i < ${#paths[@]} ; i++ )); do

  CAN_PROCEED=true
  if [ "$RESUMING" == true ]; then
    grep_result=$(grep -l "${paths[$i]}" $WHERE/.argot)
    if [ -n "$grep_result" ]; then
      CAN_PROCEED=false
    fi
  fi

  if [ "$CAN_PROCEED" == false ]; then info "skipping ${paths[$i]}"; ((counter++)); continue; fi

  info "[$filename] ($counter of $how_many_files) processing ${paths[$i]}..."
  basen=`basename ${paths[$i]}`

  # Make sure to remove the '.xml' part of the filename
  # Doing this allows the ingest app to recognize the appropriate
  # routing
  argot_fname="add-${basen//\.xml/}.json"

  # info "[$filename] [$counter of $how_many_files] xmllint processing ${paths[$i]} / resulting file: $WHERE/xmllint-${basen}...\n"

  # We're using the non-formatted, but linted, XML file for MTA
  # and we'll remove it later
  outp=`xmllint --recover --output $WHERE/.xmllint-${basen} ${paths[$i]}`
  # Make sure any <collection xmlns=""> strings are corrected
  # This is a known issue in the oai_pmh_harvest (enrich) codebase.
  sed -i 's|xmlns=""|xmlns="http://www.loc.gov/MARC21/slim"|g' $WHERE/.xmllint-${basen}

  # info "[$filename] mta processing file: $WHERE/.xmllint-${basen} / resulting file: $INGEST_QUEUE/$argot_fname ...\n"
  if [ "$DRY_RUN" == true ]; then
    info "[$filename][DRY-RUN] mta create duke $WHERE/.xmllint-${basen} $INGEST_QUEUE/$argot_fname"
    echo $(date -d "now" "+%s") > $INGEST_QUEUE/$argot_fname
  else
    outp=`/usr/local/bin/mta create duke $WHERE/.xmllint-${basen} $INGEST_QUEUE/$argot_fname 2>/dev/null`
    #echo $outp
  fi

  # KEEP TRACK of what files have been processed.
  echo "${paths[$i]}" >> $WHERE/.argot

  debug "removing linted file: xmllint-${basen}..."
  rm $WHERE/.xmllint-${basen}

  # TODO -- add error checking here
  # something to the effect of regex'ing the last 
  # line of output and looking for a known pattern

  # Once we know MTA ran successfully, increment files_processed
  # and add the file to the big_argot queue
  ((files_processed++))
  big_argot[${#big_argot[@]}]="$INGEST_QUEUE/$argot_fname"

  mod=$(($files_processed % ARGOT_FILES_TO_JOIN))
  if [[ $mod -eq 0 ]] && [ "$DISABLE_BATCH" == false ]; then
    merge_files "$INGEST_QUEUE/add-$(date -d 'now' '+%s').json" "${big_argot[@]}"
    big_argot=()
  fi

  # But, we are keeping the linted and formatted copy for later reference
  # Place the resulting file in the 'formatted-xml' directory
  [ "$DRY_RUN" == false ] && outp=`xmllint --recover --format --output $ALMA_PIPELINE_SHARE/formatted-xml/${TIMESTAMP_DIRNAME}/${basen} ${paths[$i]}`
  [ "$DRY_RUN" == false ] && sed -i 's|xmlns=""|xmlns="http://www.loc.gov/MARC21/slim"|g' $ALMA_PIPELINE_SHARE/formatted-xml/${TIMESTAMP_DIRNAME}/${basen}

  ((counter++))
done

# join (concat) any remaining files 
if [ ${#big_argot[@]} -gt 0 ] && [ "$DISABLE_BATCH" == false ]; then 
  merge_files $INGEST_QUEUE/add-$(date -d "now" "+%s").json "${big_argot[@]}"
fi

IFS=$OFS

rm ${ALMA_PIPELINE_SHARE}/run/argot.run

info "[$filename] processing done..."

