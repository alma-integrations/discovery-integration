package main

import (
	"flag"
)

// CmdLineService
type CmdLineService struct {
	ConfigFilename    string
	TarDirectory      string
	XmlDirectory      string
	DeleteTar         bool
	DeleteFiles       bool
	DSCIniFile        string
	FilePrefix        string
	PrintJson         bool
	JsonFilename      string
	PrettyJson        bool
	PerformDryRun     bool
	TimestampFilename string
	AwsTableName      string
}

func NewCmdLineService() *CmdLineService {
	cs := CmdLineService{}

	flag.StringVar(&cs.ConfigFilename, "cfg", "availability.yml", "config file location")
	flag.StringVar(&cs.TarDirectory, "tar-dir", ".", "location of tar files")
	flag.StringVar(&cs.FilePrefix, "file-prefix", "IEP", "Prefix of files to process")
	flag.StringVar(&cs.XmlDirectory, "xml-folder", "xml_files/", "directory for XML files")
	flag.BoolVar(&cs.DeleteTar, "delete-tars", true, "don't remove Tar files after processing")
	flag.BoolVar(&cs.DeleteFiles, "delete-files", true, "don't remove XML files after processing")
	flag.StringVar(&cs.DSCIniFile, "dsc-ini", "../aws_credentials", "INI file with Data Service Center creds")
	flag.BoolVar(&cs.PrintJson, "print-json", false, "Print JSON to Stdout instead of saving to Data Service Center")
	flag.BoolVar(&cs.PrettyJson, "pretty", false, "Print 'pretty' JSON. Used when -print-json=true")
	flag.BoolVar(&cs.PerformDryRun, "dry-run", false, "Do a dry run")
	flag.StringVar(&cs.TimestampFilename, "stampfile", "available-timestamp.txt", "Use this file to record job timestamp")
	flag.StringVar(&cs.JsonFilename, "json-out", "ava.json", "Name of JSON output file")
	flag.StringVar(&cs.AwsTableName, "aws-table-name", "alma-availability-production-20241031", "Name of table in AWS")

	flag.Parse()

	return &cs
}
