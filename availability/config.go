package main

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	DscCredentialsFile string
	AwsTableName       string `yaml:"aws_tablename"`
}

// NewConfig returns a new (application-specific) Config
func NewConfig(cs *CmdLineService) *Config {
	c := &Config{}
	c.parseConfigSources(cs.ConfigFilename)
	c.applyCommandLineFlags(cs)

	return c
}

// ParseConfigSource
func (c *Config) parseConfigSources(filename string) error {
	if len(filename) == 0 {
		return fmt.Errorf("%s was not specified", filename)
	}
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(c)
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) applyCommandLineFlags(cs *CmdLineService) {
	// may be implemented in the future
	c.DscCredentialsFile = cs.DSCIniFile
	if len(cs.AwsTableName) > 0 {
		c.AwsTableName = cs.AwsTableName
	}
}

// String prints the representation of this config
func (c *Config) String() string {
	return fmt.Sprintf("%+v\n", c)
}
