package connectToDSC

// add to go.mod
// github.com/aws/aws-sdk-go v1.51.14
// github.com/jmespath/go-jmespath v0.4.0
import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	dsc_credentials "gitlab.oit.duke.edu/dul-go/dsc-credentials"
)

var tableName = "alma-availability-ids"
var awsRegion = "us-east-1"

func ConnectToDSC(mms string, almaNumber string, holdingId string) {
	dsc_creds, c_err := dsc_credentials.NewCredentials("../aws_credentials")
	if c_err != nil {
		fmt.Println(c_err)
	}
	//sess, err := session.NewSession(&aws.Config{
	//	Region:      aws.String("us-east-1"),
	//	Credentials: credentials.NewStaticCredentials(dsc_creds.AccessKeyId, dsc_creds.SecretAccessKey, ""),
	//})
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		// Provide SDK Config options, such as Region and Endpoint
		Config: aws.Config{
			Region:      aws.String(awsRegion),
			Credentials: credentials.NewStaticCredentials(dsc_creds.AccessKeyId, dsc_creds.SecretAccessKey, ""),
		},
	}))

	return sess

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeInternalServerError:
				fmt.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
		return
	}

	svc := dynamodb.New(sess)

	// use this for adding records
	jsonStr := `{"mms": "` + mms + `","almaNumber": "` + almaNumber + `","holdingId": "` + holdingId + `"}`
	upsertItem(svc, jsonStr)
}

func getItem(svc *dynamodb.DynamoDB, mms string) string {

	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"mms": {
				S: aws.String(mms),
			},
		},
	})
	if err != nil {
		log.Fatalf("Got error calling GetItem: %s", err)
	}
	if result.Item == nil {
		log.Fatalf("Could not find '" + mms + "'")
	}

	var jsonMap map[string]interface{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &jsonMap)
	if err != nil {
		log.Fatalf("Failed to unmarshal Record, %v", err)
	}
	jsonBytes, err := json.Marshal(jsonMap)
	if err != nil {
		log.Fatalf("Failed to marshal Record, %v", err)
	}
	return string(jsonBytes)
}

func upsertItem(svc *dynamodb.DynamoDB, jsonStr string) {
	fmt.Println("Upserting item " + jsonStr)

	var jsonMap map[string]interface{}
	json.Unmarshal([]byte(jsonStr), &jsonMap)

	jsonMap["updatedate"] = time.Now().Format(time.RFC3339)

	av, err := dynamodbattribute.MarshalMap(jsonMap)
	if err != nil {
		log.Fatalf("Got error marshalling item: %s", err)
	}
	fmt.Println("av:")
	for key, value := range av {
		fmt.Printf("\t%s: %v\n", key, value)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	_, err = svc.PutItem(input)
	if err != nil {
		log.Fatalf("Got error calling PutItem: %s", err)
	}

	// fmt.Println("Successfully added '" + item.mmsid + " to table " + tableName)
}

func createTable(svc *dynamodb.DynamoDB) {

	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("mms"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("mms"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
		TableName: aws.String(tableName),
	}

	_, err := svc.CreateTable(input)
	if err != nil {
		log.Fatalf("Got error calling CreateTable: %s", err)
	}

	fmt.Println("Created the table", tableName)

}

func listTables(svc *dynamodb.DynamoDB) {
	// create the input configuration instance
	input := &dynamodb.ListTablesInput{}

	fmt.Printf("Tables:\n")

	for {
		// Get the list of tables
		result, err := svc.ListTables(input)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodb.ErrCodeInternalServerError:
					fmt.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
				default:
					fmt.Println(aerr.Error())
				}
			} else {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				fmt.Println(err.Error())
			}
			return
		}

		for _, n := range result.TableNames {
			fmt.Println(*n)
		}

		// assign the last read tablename as the start for our next call to the ListTables function
		// the maximum number of table names returned in a call is 100 (default), which requires us to make
		// multiple calls to the ListTables function to retrieve all table names
		input.ExclusiveStartTableName = result.LastEvaluatedTableName

		if result.LastEvaluatedTableName == nil {
			break
		}
	}
}
