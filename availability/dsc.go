package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	dsc_credentials "gitlab.oit.duke.edu/dul-go/dsc-credentials"
	journal "gitlab.oit.duke.edu/dul-go/journal"
)

type DataSvcCenter struct {
	dynamoDB  *dynamodb.DynamoDB
	tableName string
}

// AvailabilityMap will be used to unmarshal JSON strings when needed.
type AvailabilityMap struct {
	MMS              string `json:"mms"`
	AlmaID           string `json:"almaNumber"`
	HoldingID        string `json:"holdingId"`
	TotalItems       uint16 `json:"totalItems"`
	UnavailableItems string `json:"unavailableItems"`
}

// DSC_Config manages a CredentialsFile(name)
type DSC_Config struct {
	CredentialsFile string
}

// NewDataSvcCenter returns a representation of access to our "Data Service Center"
func NewDataSvcCenter(c *Config) (*DataSvcCenter, error) {
	dsc := &DataSvcCenter{}

	dsc.tableName = c.AwsTableName
	journal.Debug.Println("Table name set to:", dsc.tableName)

	// read credentials from a file designated in our configuration
	dsc_creds, c_err := dsc_credentials.NewCredentials(c.DscCredentialsFile)
	if c_err != nil {
		journal.Error.Println("Failed to read DSC creds:", c_err)
		return nil, c_err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
		Credentials: credentials.NewStaticCredentials(dsc_creds.AccessKeyId,
			dsc_creds.SecretAccessKey, ""),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeInternalServerError:
				journal.Error.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
				return nil, aerr
			default:
				journal.Error.Println(aerr.Error())
				return nil, aerr
			}
		} else {
			journal.Error.Println(err.Error())
			return nil, err
		}
	}

	dsc.dynamoDB = dynamodb.New(sess)
	journal.Debug.Printf("Connected to Data Service Center...")

	return dsc, nil
}

func (dsc *DataSvcCenter) Write(b []byte) (int, error) {
	aMap := AvailabilityMap{}
	err := json.Unmarshal(b, &aMap)
	if err != nil {
		journal.Error.Println("Error unmarshalling JSON:", err)
		return 0, err
	}

	// journal.Debug.Printf("Unmarshalled aMap: %+v", aMap)

	// journal.Debug.Printf("Checking if AlmaID %s exists for MMS %s", aMap.AlmaID, aMap.MMS)
	almaID, err := dsc.AlmaNumberFromMMS(aMap.MMS)
	if err != nil {
		journal.Error.Println("Error fetching AlmaID:", err)
		return 0, err
	}
	if almaID == aMap.AlmaID {
		journal.Debug.Printf("AlmaID %s already exists for MMS %s, skipping write", aMap.AlmaID, aMap.MMS)
		return len(b), nil
	}

	jsonString := string(b[:])
	journal.Debug.Println("Upserting item to DSC:", jsonString)
	upsertErr := dsc.UpsertItem(jsonString)
	if upsertErr != nil {
		journal.Error.Printf("[dsc.Write] Unable to update/insert MMS/AlmaNumber/HoldingID mapping: %s\n", upsertErr)
		return 0, upsertErr
	}

	return len(b), nil
}

// AlmaNumberFromMMS returns an Alma number mapped to the
// (provided) MMS number
func (dsc *DataSvcCenter) AlmaNumberFromMMS(mms string) (string, error) {
	// journal.Debug.Printf("Fetching Alma number for MMS %s", mms)

	if mms == "" {
		journal.Error.Printf("MMS key attribute cannot be an empty string")
		return "", fmt.Errorf("MMS key attribute cannot be an empty string")
	}

	result, err := dsc.dynamoDB.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(dsc.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"mms": {
				S: aws.String(mms),
			},
		},
	})
	if err != nil {
		journal.Error.Printf("Got error calling (aws) GetItem: %s\n", err)
		return "", err
	}
	if result.Item == nil {
		journal.Debug.Printf("MMS %s not found", mms)
		return "", fmt.Errorf("MMS %s not found", mms)
	}
	almaNumber := result.Item["almaNumber"]
	// journal.Debug.Printf("Found Alma number %s for MMS %s", *almaNumber.S, mms)
	return string(*almaNumber.S), nil
}

// UpsertItem will either update or insert a new item into the DSC
func (dsc *DataSvcCenter) UpsertItem(jsonStr string) error {
	journal.Debug.Println("Upserting item " + jsonStr)

	var jsonMap map[string]interface{}
	err := json.Unmarshal([]byte(jsonStr), &jsonMap)
	if err != nil {
		journal.Error.Println("[dsc.UpsertItem] Error unmarshalling JSON:", err)
		return err
	}

	jsonMap["updatedate"] = time.Now().Format(time.RFC3339)

	av, err := dynamodbattribute.MarshalMap(jsonMap)
	if err != nil {
		journal.Error.Println("[dsc.UpsertItem] Unable to marshal item:", err)
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(dsc.tableName),
	}

	_, err = dsc.dynamoDB.PutItem(input)
	if err != nil {
		journal.Error.Printf("Got error calling PutItem: %s", err)
		return err
	}

	journal.Debug.Println("Successfully upserted item to DSC")
	return nil
}
