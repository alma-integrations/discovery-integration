module gitlab.oit.duke.edu/alma-integrations/discovery-integration/availability

go 1.21.3

require (
	github.com/aws/aws-sdk-go v1.51.18
	gitlab.oit.duke.edu/dul-go/journal v0.0.0-20190219194120-2a5b5450b878
	go.uber.org/dig v1.17.1
	gopkg.in/yaml.v2 v2.2.8
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	gitlab.oit.duke.edu/dul-go/dsc-credentials v0.2.5 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
