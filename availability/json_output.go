package main

import "os"

func NewJsonOutputFile(cli *CmdLineService) (*os.File, error) {
	file, err := os.Create(cli.JsonFilename)
	if err != nil {
		return nil, err
	}
	return file, nil
}
