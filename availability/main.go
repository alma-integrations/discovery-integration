package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.oit.duke.edu/alma-integrations/discovery-integration/availability/movePrimoPublished"
	journal "gitlab.oit.duke.edu/dul-go/journal"
	"go.uber.org/dig"
)

const XML_FOLDER = "xml_files/"
const tar_dir = "."

type dataBase struct {
	XMLName   xml.Name    `xml:"OAI-PMH"`
	ID_string string      `xml:"ListRecords>record>header>identifier"`
	D_fields  []datafield `xml:"ListRecords>record>metadata>record>datafield"`
}

type datafield struct {
	Tag      string     `xml:"tag,attr"`
	Value    string     `xml:",chardata"`
	S_fields []subfield `xml:"subfield"`
}

type subfield struct {
	Tag   string `xml:"code,attr"`
	Value string `xml:",chardata"`
}

// ReadDataFromXMLFile read an xml file and return values
func ReadDataFromXMLFile(file string) (mms string, almaNumber string, holding string, total uint16, avaError error) {
	xmlFile, err := os.Open(file)
	if err != nil {
		journal.Error.Printf("Error opening file: " + file)
		return "", "", "", 0, err
	}
	defer xmlFile.Close()

	b, err := io.ReadAll(xmlFile)
	if err != nil {
		journal.Error.Printf("Error reading file: " + file)
		return "", "", "", 0, err
	}

	var x dataBase
	if err := xml.Unmarshal(b, &x); err != nil {
		return "", "", "", 0, err
	}

	alma_number := strings.Split(x.ID_string, ":")[1]

	var mms_id string
	var holding_id string
	var totalItems uint16
	ava := false

	for _, dField := range x.D_fields {
		if dField.Tag == "AVA" {
			if !ava {
				ava = true
				for _, sField := range dField.S_fields {
					switch sField.Tag {
					case "0":
						mms_id = sField.Value
					case "8":
						holding_id = sField.Value
					case "f":
						scannedInt, err := strconv.Atoi(sField.Value)
						if err != nil {
							journal.Error.Printf("Error converting total items to integer: %v", err)
							return "", "", "", 0, err
						}
						totalItems = uint16(scannedInt)
					}
				}
			} else {
				for _, sField := range dField.S_fields {
					if sField.Tag == "f" {
						scannedInt, err := strconv.Atoi(sField.Value)
						if err != nil {
							journal.Error.Printf("Error converting total items to integer: %v", err)
							return "", "", "", 0, err
						}
						totalItems += uint16(scannedInt)
					}
				}
			}
		}
	}
	if !ava {
		journal.Debug.Println(fmt.Sprintf("No AVA Tag for MMS: %s, Alma Number: %s", mms, alma_number))
		return "", "", "", 0, fmt.Errorf("No AVA tag for MMS: %s, Alma Number: %s\n", mms, alma_number)
	}
	return mms_id, alma_number, holding_id, totalItems, nil
}

// BuildContainer uses "Dependency Injections" to build
// our known dependencies
func BuildContainer(container *dig.Container) {
	container.Provide(NewCmdLineService)
	container.Provide(NewConfig)
	container.Provide(NewDataSvcCenter)
	container.Provide(func(cli *CmdLineService) (*os.File, error) {
		return NewJsonOutputFile(cli)
	})
}

func main() {
	container := dig.New()
	BuildContainer(container)

	journal.InitLogging(
		os.Stdout,      // DEBUG
		ioutil.Discard, // INFO
		os.Stderr,      // WARNING
		os.Stderr,      // ERROR
	)

	err := container.Invoke(run)
	if err != nil {
		journal.Error.Printf("container invocation failed: %s\n", err)
		os.Exit(1)
	}
}

func run(cli *CmdLineService, dsc *DataSvcCenter, jsonOutFile *os.File) error {
	defer jsonOutFile.Close()

	timestampFile, err := os.OpenFile(cli.TimestampFilename, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		journal.Error.Printf("unable to open %s to record timestamps: %w", cli.TimestampFilename, err)
		return err
	}
	defer timestampFile.Close()

	jobStartTime := time.Now()
	if err := writeTimestamp(timestampFile, jobStartTime); err != nil {
		return err
	}

	journal.Debug.Printf("command-line flags:\n%+v\n", cli)
	workingDir, _ := os.Getwd()
	journal.Debug.Printf("working directory: %s", workingDir)

	tarFiles, err := movePrimoPublished.WalkDir(cli.TarDirectory, []string{"tar.gz"})
	if err != nil {
		return err
	}

	IEP := filterFiles(tarFiles, cli.TarDirectory, cli.FilePrefix)

	if err := os.MkdirAll(cli.XmlDirectory, os.ModePerm); err != nil {
		journal.Error.Printf("error creating directory %s: %w", cli.XmlDirectory, err)
		return err
	}

	for _, tgzFile := range IEP {
		if err := processTarFile(tgzFile, cli, dsc, jsonOutFile); err != nil {
			return err
		}
	}

	jobEndTime := time.Now()
	if err := writeTimestamp(timestampFile, jobEndTime); err != nil {
		return err
	}

	duration := jobEndTime.Sub(jobStartTime)
	if _, err := timestampFile.Write([]byte(fmt.Sprintf("%s\n", duration.String()))); err != nil {
		journal.Error.Printf("error writing duration: %w", err)
		return err
	}

	return nil
}

func writeTimestamp(file *os.File, timestamp time.Time) error {
	timestampStr := fmt.Sprintf("%d|%s\n", timestamp.Unix(), timestamp.Format(time.UnixDate))
	if _, err := file.Write([]byte(timestampStr)); err != nil {
		journal.Error.Printf("error writing timestamp: %w", err)
		return err
	}
	return nil
}

func filterFiles(files []string, dir, prefix string) []string {
	var filtered []string
	for _, file := range files {
		if strings.HasPrefix(file, fmt.Sprintf("%s/%s", dir, prefix)) {
			filtered = append(filtered, file)
		}
	}
	return filtered
}

func processTarFile(tgzFile string, cli *CmdLineService, dsc *DataSvcCenter, jsonOutFile *os.File) error {
	journal.Debug.Printf("Processing tgzFile = [%s]\n", tgzFile)
	movePrimoPublished.Move(tgzFile, cli.TarDirectory, cli.XmlDirectory)
	if cli.DeleteTar {
		if err := os.Remove(tgzFile); err != nil {
			journal.Error.Printf("Error removing tgz file: %s", err)
		}
	}

	entries, err := os.ReadDir(cli.XmlDirectory)
	if err != nil {
		journal.Error.Printf("error reading directory %s: %w", cli.XmlDirectory, err)
		return err
	}

	for _, e := range entries {
		if err := processEntry(e, cli, dsc, jsonOutFile); err != nil {
			return err
		}
	}

	return nil
}

func processEntry(e os.DirEntry, cli *CmdLineService, dsc *DataSvcCenter, jsonOutFile *os.File) error {
	if strings.HasSuffix(e.Name(), ".xml") {
		if strings.HasPrefix(e.Name(), "._") {
			journal.Debug.Printf("Removing file: %s", e.Name())
			os.Remove(cli.XmlDirectory + e.Name())
		} else {
			mms, almaNumber, holdingId, totalItems, err := ReadDataFromXMLFile(cli.XmlDirectory + e.Name())
			if err != nil {
				journal.Error.Printf("%s\n", err)
				return err
			}

			if recordIsValid(mms, almaNumber, holdingId, fmt.Sprint(totalItems)) {
				if err := writeJsonOutput(cli, jsonOutFile, mms, almaNumber, holdingId, totalItems); err != nil {
					return err
				}

				if err := writeDscOutput(dsc, mms, almaNumber, holdingId, totalItems); err != nil {
					return err
				}
			} else {
				journal.Debug.Printf("Record not valid: %s, MMS: %s", e.Name(), mms)
			}
		}
	}

	if cli.DeleteFiles {
		journal.Debug.Printf("Deleting file: %s", e.Name())
		os.Remove(cli.XmlDirectory + e.Name())
	}

	return nil
}

func writeJsonOutput(cli *CmdLineService, jsonOutFile *os.File, mms, almaNumber, holdingId string, totalItems uint16) error {
	dynamoDBFormat := map[string]interface{}{
		"Item": map[string]interface{}{
			"mms":        map[string]string{"S": mms},
			"almaNumber": map[string]string{"S": almaNumber},
			"holdingId":  map[string]string{"S": holdingId},
			"totalItems": map[string]uint16{"N": totalItems},
		},
	}

	dynamoDBFormatStr, err := json.Marshal(dynamoDBFormat)
	if err != nil {
		journal.Error.Printf("error marshaling DynamoDB format: %w", err)
		return err
	}

	if cli.PrintJson {
		fmt.Println(string(dynamoDBFormatStr))
	} else {
		if jsonOutFile != nil {
			if dynamoDBFormatStr != nil {
				journal.Debug.Println(string(dynamoDBFormatStr))
				if _, err := jsonOutFile.Write([]byte(fmt.Sprintf("%s\n", dynamoDBFormatStr))); err != nil {
					journal.Error.Printf("error writing JSON to file: %w", err)
					return err
				}
			} else {
				journal.Debug.Println("dynamoDBFormatStr is empty, not writing to file")
			}

		}
	}

	return nil
}

func writeDscOutput(dsc *DataSvcCenter, mms, almaNumber, holdingId string, totalItems uint16) error {
	jsonStr := map[string]interface{}{
		"mms":        mms,
		"almaNumber": almaNumber,
		"holdingId":  holdingId,
		"totalItems": totalItems,
	}

	var buffer bytes.Buffer
	encoder := json.NewEncoder(&buffer)
	if err := encoder.Encode(jsonStr); err != nil {
		journal.Error.Printf("error encoding JSON: %w", err)
		return err
	}

	if _, err := dsc.Write(buffer.Bytes()); err != nil {
		journal.Error.Printf("[dsc.Write] unable to process the jsonStr and add the mapping: %w", err)
		return err
	}

	return nil
}

func recordIsValid(mmsId, almaNumber, holdingId, totalNumber string) bool {
	return len(mmsId) > 0 &&
		len(almaNumber) > 0 &&
		len(holdingId) > 0 &&
		len(totalNumber) > 0
}
