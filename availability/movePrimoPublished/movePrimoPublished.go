package movePrimoPublished

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"encoding/xml"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

const XML_Destination = "xml_files/"
const TAR_Destination = "old_tar/"

// Does this need to go in a different place?
type OAIPMH struct {
	XMLName        xml.Name `xml:"OAI-PMH"`
	Text           string   `xml:",chardata"`
	Xmlns          string   `xml:"xmlns,attr"`
	Xsi            string   `xml:"xsi,attr"`
	SchemaLocation string   `xml:"schemaLocation,attr"`
	ListRecords    struct {
		Text   string `xml:",chardata"`
		Record struct {
			Text   string `xml:",chardata"`
			Header struct {
				Text       string `xml:",chardata"`
				Status     string `xml:"status,attr"`
				Identifier string `xml:"identifier"`
			} `xml:"header"`
			Metadata struct {
				Text   string `xml:",chardata"`
				Record struct {
					Text           string `xml:",chardata"`
					Xmlns          string `xml:"xmlns,attr"`
					Xsi            string `xml:"xsi,attr"`
					SchemaLocation string `xml:"schemaLocation,attr"`
					Leader         string `xml:"leader"`
					Controlfield   []struct {
						Text string `xml:",chardata"`
						Tag  string `xml:"tag,attr"`
					} `xml:"controlfield"`
					Datafield []struct {
						Text     string `xml:",chardata"`
						Tag      string `xml:"tag,attr"`
						Ind1     string `xml:"ind1,attr"`
						Ind2     string `xml:"ind2,attr"`
						Subfield []struct {
							Text string `xml:",chardata"`
							Code string `xml:"code,attr"`
						} `xml:"subfield"`
					} `xml:"datafield"`
				} `xml:"record"`
			} `xml:"metadata"`
		} `xml:"record"`
	} `xml:"ListRecords"`
}

// Extract Tar
func ExtractTarGz(gzipStream io.Reader, xml_dir string) {
	uncompressedStream, err := gzip.NewReader(gzipStream)
	if err != nil {
		journal.Error.Printf("Failed to create gzip reader: %s\n", err)
		return
	}
	defer uncompressedStream.Close()

	tarReader := tar.NewReader(uncompressedStream)

	for true {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		}

		if err != nil {
			journal.Error.Printf("%s\n", err)
			return
		}

		switch header.Typeflag {
		case tar.TypeDir:
			journal.Debug.Printf("[ExtractTarGz]: creating folder: %s\n", header.Name)
			if err := os.Mkdir(header.Name, 0755); err != nil {
				journal.Error.Printf("[ExtractTarGz]: %s\n", err)
				return
			}
		case tar.TypeReg:
			outFile, err := os.Create(fmt.Sprintf("%s/%s", xml_dir, header.Name))
			if err != nil {
				journal.Error.Printf("[ExtractTarGz]:%s\n", err)
				return
			}
			if _, err := io.Copy(outFile, tarReader); err != nil {
				journal.Error.Printf("[ExtractTarGz]: %s\n", err)
				return
			}
			outFile.Close()

		default:
			journal.Debug.Printf("Unknown tar header type: %v\n", header.Typeflag)
		}

	}
}

// Moves one file from and to a location
func MoveFile(sourcePath, destPath string) {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		journal.Error.Printf("Couldn't open source file: %s\n", err)
		return
	}
	defer inputFile.Close()

	outputFile, err := os.Create(destPath)
	if err != nil {
		journal.Error.Printf("Couldn't open dest file: %s\n", err)
		return
	}
	defer outputFile.Close()

	if _, err := io.Copy(outputFile, inputFile); err != nil {
		journal.Error.Printf("Couldn't copy to dest from source: %s\n", err)
		return
	}

	if err := os.Remove(sourcePath); err != nil {
		journal.Error.Printf("Couldn't remove source file: %s\n", err)
		return
	}
}

// returns all files in current directory given a string of file format
func WalkDir(root string, exts []string) ([]string, error) {
	var files []string
	sepCount := strings.Count(root, string(os.PathSeparator))

	err := filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			journal.Error.Printf("Error walking directory: %s\n", err)
			return err
		}
		if d.IsDir() && d.Name() == ".git" {
			return filepath.SkipDir
		}
		if d.IsDir() && strings.Count(path, string(os.PathSeparator)) > sepCount {
			return fs.SkipDir
		}
		for _, ext := range exts {
			if strings.HasSuffix(path, "."+ext) {
				files = append(files, path)
				return nil
			}
		}
		return nil
	})

	return files, err
}

func zipDirectory(zipfilename string, files []string) error {
	outFile, err := os.Create(zipfilename)
	if err != nil {
		journal.Error.Printf("Couldn't create zip file: %s\n", err)
		return err
	}
	defer outFile.Close()

	w := zip.NewWriter(outFile)
	defer func() {
		if err := w.Close(); err != nil {
			journal.Error.Printf("Warning: closing zipfile writer failed: %s\n", err)
		}
	}()

	for _, file := range files {
		if err := addFilesToZip(w, zipfilename, file); err != nil {
			journal.Error.Printf("Error adding file to zip: %s\n", err)
			return err
		}
	}

	return nil
}

func addFilesToZip(w *zip.Writer, basePath, baseInZip string) error {
	files, err := ioutil.ReadDir(basePath)
	if err != nil {
		journal.Error.Printf("Couldn't read directory: %s\n", err)
		return err
	}

	for _, file := range files {
		fullfilepath := filepath.Join(basePath, file.Name())
		// ensure the file exists. For example a symlink pointing to a non-existing location might be listed but not actually exist
		if _, err := os.Stat(fullfilepath); os.IsNotExist(err) {
			continue
		}

		// ignore symlinks alltogether
		if file.Mode()&os.ModeSymlink != 0 {
			continue
		}

		if file.IsDir() {
			if err := addFilesToZip(w, fullfilepath, filepath.Join(baseInZip, file.Name())); err != nil {
				return err
			}
		} else if file.Mode().IsRegular() {
			dat, err := ioutil.ReadFile(fullfilepath)
			if err != nil {
				journal.Error.Printf("Couldn't read file: %s\n", err)
				return err
			}
			f, err := w.Create(filepath.Join(baseInZip, file.Name()))
			if err != nil {
				journal.Error.Printf("Couldn't create file in zip: %s\n", err)
				return err
			}
			if _, err := f.Write(dat); err != nil {
				journal.Error.Printf("Couldn't write to zip file: %s\n", err)
				return err
			}
		}
	}
	return nil
}

func Move(tarFilename string, xmlDir string, xmlDest string) {
	journal.Debug.Printf("[movePrimoPublished:move] %s | %s | %s\n", tarFilename, xmlDir, xmlDest)

	tarFile, err := os.Open(tarFilename)
	if err != nil {
		journal.Error.Printf("Error opening file: %s\n", err)
		return
	}
	defer tarFile.Close()

	ExtractTarGz(tarFile, xmlDir)

	files, err := WalkDir(xmlDir, []string{"xml"})
	if err != nil {
		journal.Error.Printf("Error walking directory: %s\n", err)
		return
	}
	journal.Debug.Printf("Files after WalkDir(%s): %s", xmlDir, files)

	for _, xmlf := range files {
		t := strings.Split(xmlf, "/")
		dest := filepath.Join(xmlDest, t[len(t)-1])
		MoveFile(xmlf, dest)
	}
}
