# OKD Setup for Discovery Catalog

## Checklist
**Create Secrets for Database and Secret Keys**  
  
```bash
$ kubectl create secret generic app-postgres --from-literal=postgres-password=$(openssl rand -base64 14) --from-literal=password=$(openssl rand -base64 14)

$ kubectl create secret generic app-secrets \
--from-literal=worldcat-apikey=<worldcatapikey> \
--from-literal=secret-key-base=<secret-key-base>
```
  
## Deploying the application
### Redis
Deploy `redis` separately from the `trln-ingest` application:  
  
```sh
$ helm install redis oci://registry-1.docker.io/bitnamicharts/redis -f redis.yaml
```

### Ingest App
For now, only the `dev` instance is active.  
  
```
helm upgrade --install dev . --reset-values -f db.yaml -f values-dev.yaml --set image.spoffordTag=spofford-769d33e
```
