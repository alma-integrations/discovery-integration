#!/bin/bash

# Clean up the /share/alma-production/discovery/ directory from the Alma job
# Logged to through >> to `/share/alma-pipeline/logs/clean-discovery.log`

set -euo pipefail

DISCOVERY_DIR="/share/alma-production/discovery"
PRIMO_DIR="/share/alma-pipeline/primo"
JSON_DIR="/share/alma-pipeline/ava/json"
DOW="Saturday"
HOUR="11"

STARTING_COUNT=$(find "$DISCOVERY_DIR" -type f -name '*.tar.gz' | wc -l)
printf "STARTING_COUNT: %d\n" "$STARTING_COUNT"

move_files() {
  local file_type=$1
  local file_count
  file_count=$(find "$DISCOVERY_DIR" -type f -name "${file_type}*" | wc -l)
  printf "%s_COUNT: %d\n" "$file_type" "$file_count"

  if [ "$file_count" -eq "0" ]; then
    printf "No %s files, skipping\n" "$file_type"
  else
    if [ "$(date +%A)" != "$DOW" ] || [ "$(date +%H)" -ne "$HOUR" ]; then
      printf "> Not %s at %02d:00, only copying files\n" "$DOW" "$HOUR"
      rsync -a --checksum --partial "$DISCOVERY_DIR/${file_type}*" "$PRIMO_DIR/" || {
        printf "Error: rsync failed\n"
        exit 1
      }
    else
      printf "> %s at %02d:00, moving files and not deleting because still testing\n" "$DOW" "$HOUR"
      #
      # This is what will ultimately be used in this part of the code but holding off for testing
      #
      # printf "> %s at %02d:00, moving deleting files\n" "$DOW" "$HOUR"
      # rsync -a --remove-source-files --checksum --partial $DISCOVERY_DIR/IEP* "$PRIMO_DIR/" || {
      rsync -a --checksum --partial "$DISCOVERY_DIR/${file_type}*" "$PRIMO_DIR/" || {
        printf "Error: rsync failed\n"
        exit 1
      }
    fi
  fi
}

remove_files() {
  local pattern=$1
  local description=${pattern%?}

  local count
  count=$(find "$DISCOVERY_DIR" -type f -name "$pattern" | wc -l)
  if [ "$count" -eq 0 ]; then
    printf "No %s files, skipping\n" "$description"
  else
    printf "%s_COUNT: %d\n" "$description" "$count"
    find "$DISCOVERY_DIR" -type f -name "$pattern" -exec rm -f {} +
  fi
}

# Move IEP files
move_files 'IEP'

# Move IEE files
# move_files 'IEE'

# Remove IE_MMS files
remove_files 'IE_MMS*'

# Print count of remaining files
REMAINING_COUNT=$(find "$DISCOVERY_DIR" -type f -name '*.tar.gz' | wc -l)
if [ "$REMAINING_COUNT" -eq 0 ]; then
  printf "All files have been processed and removed.\n"
else
  printf "REMAINING_COUNT: %d\n" "$REMAINING_COUNT"
fi


DELETED_COUNT=$(find "$JSON_DIR" -type f -name '*.json' -size 0 -exec rm -f {} + -print | wc -l)
printf "Removed %d 0-byte JSON files in %s.\n" "$DELETED_COUNT" "$JSON_DIR"

printf "======================================\n"
