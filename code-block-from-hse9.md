## Stewart's Code Block for "Enrich"

```go


		// STEWART -- this is where the refactoring ends -- let the program stop here.

		// THIS IS WHERE THE OLD/LEGACY CODE STARTS
		runLegacyCode := false
		if runLegacyCode == true {
			for index, record := range collection.Record {

				//journal.Info.Println("Processing record", index+1, "of", len(collection.Record))
				journal.Info.Printf("Processing record %d of %d [%s]\n", index+1, len(collection.Record), oaiPmhRecordsFilename)

				mms = ""
				isPhysicalItem = false
				onlineResource = false
				isDeleted = false

				// Skip if we don't find a 940 tag
				for _, controlfield := range record.Controlfield {
					if controlfield.Tag == "001" {
						mms = controlfield.Text
						for _, datafield := range record.Datafields {
							// This check is added to deal with related records being added
							// to electronic resources. To avoid an extra DSC lookup, filter
							// based on the 338a
							if datafield.Tag == "338" {
								for _, subfield := range datafield.Subfields {
									if subfield.Code == "a" {
										if subfield.Text == "online resource" {
											journal.Debug.Println(subfield.Text)
											onlineResource = true
										}
									}
								}
							}
							if datafield.Tag == "940" {
								if onlineResource != true {
									isPhysicalItem = true
								}
							}
						}
					}
				}

				// We're adding the MMS ID to a list of "deleted" records
				// that can sent to POD (and maybe even TRLN)

				// TODO:
				// Replaced looking for a header with reading the Leader as it seems
				// More reliable
				if string(record.Leader[5]) == "d" && len(mms) > 0 {
					var delMMS string
					journal.Debug.Println("Adding deleted record to POD file")
					isDeleted = true

					// If the bib is from Aleph trim to match the index
					if len(mms) == 18 {
						if bytes.HasSuffix([]byte(mms), []byte("0108501")) {
							delMMS = strings.TrimSuffix(mms, "0108501")
							delMMS = strings.TrimPrefix(delMMS, "99")
						} else {
							delMMS = mms
						}
					} else {
						delMMS = mms
					}

					mmsWithNewline := "DUKE" + delMMS + "\n"
					if _, err = deletedFile.WriteString(mmsWithNewline); err != nil {
						panic(err)
					}
				}
				// journal.Debug.Println(cli.DeleteOnly)
				// journal.Debug.Println(cli.BypassHarvest)

				if len(mms) > 0 && isPhysicalItem && isDeleted == false && cli.DeleteOnly == false {
					journal.Debug.Printf("Working with MMS: %s\n", mms)

					// Hit the DSC to get the AlmaNumber
					almaNumber, totalItems, a_err := dsc.AlmaNumberFromMMS(mms)
					if a_err != nil {
						journal.Error.Printf("Error retrieving ALMA number for MMS %s: %s\n", mms, a_err)
						continue
					}

					if len(almaNumber) > 0 {
						journal.Debug.Printf("located Alma Number (%s) for MMS: %s\n", almaNumber, mms)
						// availability comes back as "not available" and "unknown" when we want it to be "unavailable"

						url := configData.SoaUrl + almaNumber
						response, err := makeSoaRequest(url, cli)
						if err != nil {
							journal.Error.Println("Error making HTTP request:", err)
							return
						}
						defer response.Body.Close()
						body, err := io.ReadAll(response.Body)

						var result SoaResponse
						// sometimes the response is wrapped in a callback
						adjustedBody := string(body[:])
						removeEnd := strings.TrimSuffix(adjustedBody, ")")
						removeFront := strings.TrimPrefix(removeEnd, "callback(")
						if err := json.Unmarshal([]byte(removeFront), &result); err != nil {
							journal.Error.Println("Can not unmarshal JSON")
						}

						// result is the status
						// NEED a safety guard for result.AvailabilityItems
						if len(result.AvailabilityItems) > 0 {
							for k := range result.AvailabilityItems[0].Availabilities {
								availability := result.AvailabilityItems[0].Availabilities[k].Status

								if availability == "available" {
									availability = "Available"
								} else if availability == "not available" {
									availability = "Unavailable"
								} else if availability == "check_holdings" {
									availability = "Check holdings"
								} else if availability == "unknown" {
									availability = "Check holdings"
								} else {
									availability = "Check holdings"
								}

								// This is where you select which subfield to put the data

								// TODO: Need to add sub_total to 941, testing
								sub_total := MarcSubfield{Code: "t", Text: totalItems}
								data_total := MarcDatafield{Tag: "941", Ind1: " ", Ind2: " ", Subfields: []MarcSubfield{sub_total}}

								for i, datafield := range record.Datafields {
									if datafield.Tag == "852" {
										sub_x := MarcSubfield{Code: "x", Text: availability}
										isInRubenstein := false
										cutting_number := ""
										call_number := ""
										complete_i := ""
										subLoc := ""
										var which int
										for _, subfield := range datafield.Subfields {
											if subfield.Code == "b" {
												//journal.Debug.Printf("Inside of %s subfield", subfield.Code)
												subLoc = strings.TrimSpace(subfield.Text)
												//journal.Debug.Printf("subLoc %s", subLoc)
												//journal.Debug.Printf("Found subfield '852b' : %+v\n", subfield.Text)
												if subLoc == "ARCH" || subLoc == "SCL" && isInRubenstein == false && which != i {
													which = i
													if sub_x.Text != "Available - Reading Room Use Only" && isInRubenstein == false {
														// "Available - Reading Room Use Only" {
														sub_x.Text = "Available - Reading Room Use Only"
														isInRubenstein = true
														// TODO: this is the problem

														// dlc32 NOTE -- I don't think you want to append here so I'm commenting it out
														// record.Datafields[i].Subfields = append(record.Datafields[i].Subfields, sub_x)
														//datafield = append(datafield, sub_x)
														//journal.Debug.Println(record.Datafields[i].Subfields)
													}
													break
												}
											}
											if subfield.Code == "h" && isInRubenstein == false {
												cutting_number = strings.TrimSpace(subfield.Text)
												//journal.Debug.Printf("Found cutting number in '852h' : %+v\n", subfield.Text)
											}
											if subfield.Code == "i" && isInRubenstein == false {
												//journal.Debug.Printf("LocationString: %+v\n", result.AvailabilityItems[0].Availabilities[k].LocationString)
												if complete_i != "" {
													complete_i = complete_i + " " + strings.TrimSpace(subfield.Text)
												} else {
													complete_i = strings.TrimSpace(complete_i) + " " + strings.TrimSpace(subfield.Text)
												}
												call_number = cutting_number + " " + strings.TrimSpace(complete_i)

											}
											fmt.Printf("call number = [%s]\n", call_number)
										}
										// TODO: This is set up on the 852b
										// it could be set for the 852c as well
										/*
											if call_number != "" && cutting_number != "" && isInRubenstein == false {
												for _, locCode := range locations {
													if locCode.Code == subLoc { // 852b
														subLocName := locCode.Name

														continue
													}
												}

											} else if call_number == "" && cutting_number != "" && isInRubenstein == false {
												for _, locCode := range locations {
													if locCode.Code == subLoc { // 852b
														subLocName := locCode.Name
														//for _, loc := range locCode.Locations {
														//if loc.Name == shelvingLoc { // 852c
														//journal.Debug.Println("Outside IF loop of adding non-SCL/ARCH Avail, pt. 2")
														if strings.Contains(result.AvailabilityItems[0].Availabilities[k].LocationString, cutting_number) {
															if strings.Contains(result.AvailabilityItems[0].Availabilities[k].LocationString, subLocName) {
																if sub_x.Text != "Available - Reading Room Use Only" {
																	//journal.Debug.Printf("Matched on cutting_number only because no 852i")
																	//journal.Debug.Println(record.Datafields[i].Subfields)
																	// dlc32
																	//record.Datafields[i].Subfields = append(record.Datafields[i].Subfields, sub_x)
																}
															}
														}
													}
												}
											}
										*/
										// HSE
										record.Datafields[i].Subfields = append(record.Datafields[i].Subfields, sub_x)
									}
								}
								// Adding the total item count to the 941t
								collection.Record[index].Datafields = append(collection.Record[index].Datafields, data_total)
								journal.Debug.Printf("Added total item count to 941t")
							}
						} else {
							journal.Warning.Println(a_err)
						}
					} else {
						// The Alma ID is not in the Data Service Center.
						// What I'd like to do is flag this file and not
						// save its "enriched" output file -- but rather
						// put this output file in a different location.
						journal.Warning.Println(a_err)
					}
				} else {
					if onlineResource != true {
						journal.Debug.Printf("Skipping MMS because there is not a 940: %s\n", mms)
					} else {
						journal.Debug.Printf("Skipping MMS because 338a is 'online resource': %s\n", mms)
					}
					journal.Debug.Printf("Is it physical? %t\n", isPhysicalItem)
				}
			}
		}
```

Preserved in case we need to refer to it.  
--dlc32
