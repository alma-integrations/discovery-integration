#!/bin/bash

./print-ava-compose.sh 1 60 --loader-vm lsis-loader-01
./print-ava-compose.sh 61 120 --loader-vm lsis-loader-02
./print-ava-compose.sh 121 180 --loader-vm lsis-loader-03
./print-ava-compose.sh 181 240 --loader-vm lsis-loader-04
./print-ava-compose.sh 241 300 --loader-vm lsis-loader-05
./print-ava-compose.sh 301 360 --loader-vm lsis-loader-06
./print-ava-compose.sh 361 420 --loader-vm lsis-loader-07
./print-ava-compose.sh 421 480 --loader-vm lsis-loader-08
./print-ava-compose.sh 481 540 --loader-vm lsis-loader-09
./print-ava-compose.sh 541 600 --loader-vm lsis-loader-10
