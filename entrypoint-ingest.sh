#!/bin/bash

set -e

# NOTES
# 
# 1) make sure your active 'docker-compose.yml' file has a 
#    directory mounted to /pipeline
#
# 2) make sure this same folder includes an 'argot' folder
#

case $1 in
  make-argot)
    send-to-trln-solr.sh \
      --bypass-ingest \
      /pipeline
    ;;

  run-ingrest)
    send-to-trln-solr.sh \
      --skip-argot
    ;;

  *)
    exec "$@"
    ;;
esac
