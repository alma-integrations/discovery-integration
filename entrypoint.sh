#!/bin/bash

set -e

# Run the Go script, oai_pmh_harvest, with the appropriate options

case $1 in
  almanumber)
    [ -n "$2" ] && oai_pmh_harvest --dsc-ini /usr/local/etc/aws_credentials almanumber $2
    ;;

  primo)
    availability -delete-files=false -delete-tars=false -dsc-ini /usr/local/etc/aws_credentials -tar-dir /opt/dul/primo
    ;;

  harvest)
    # attempt to read the last time "harvest" ran (hopefully, successfully)
    # this date would be located in a file we define

    # volume mounting note:
    # "timestamps" needs to be mounted into the container
    # see "print-harvest-compose.sh"
    # --from 2024-07-19T01:55:00Z \

    # CHANGES_FROM is an environment var setup in the docker-compose-harvest.yml file
    if [[ -z "$CHANGES_FROM" ]]; then
      [[ -f "timestamps/last-harvest.txt" ]] && FROM_TIMESTAMP=$(cat timestamps/last-harvest.txt)
      FROM_TIMESTAMP=${FROM_TIMESTAMP:-$(date -d '-1 day' -u +"%Y-%m-%dT%H:%M:%SZ")}
    else
      FROM_TIMESTAMP=$CHANGES_FROM
    fi

    # refresh "timestamps/last-harvest" with the current (now) timestamp
    # (for the next 'run')
    date -u +"%Y-%m-%dT%H:00:00Z" > timestamps/last-harvest.txt

    oai_pmh_harvest \
      --harvest-file harvest-$(date +%s).xml \
      --retries 10 \
      --interval 10 \
      --skip-enrich \
      --resTokenOut tokens/resumptionToken.txt \
      --pages 100 \
      --changes-from $FROM_TIMESTAMP \
      --dsc-ini /usr/local/etc/aws_credentials \
      --oai-set ${OAI_SET:-trln_discovery_spec} \
      --pmh-dataset ${OAI_SET:-trln_discovery_spec}

    TOKEN=$(cat tokens/resumptionToken.txt)
    if [[ -z "$TOKEN" ]]; then
      echo "no token available after initial harvest run..."
    else
      echo "initial token: ${TOKEN}"
    fi

    while [[ -n "$TOKEN" ]]; do
      echo "running with token: $TOKEN"
      oai_pmh_harvest \
        --harvest-file harvest-$(date +%s).xml \
      	--retries 10 \
        --interval 10 \
        --skip-enrich \
        --pages 100 \
        --resTokenOut tokens/resumptionToken.txt \
        --resToken ${TOKEN} \
        --dsc-ini /usr/local/etc/aws_credentials \
        --oai-set ${OAI_SET:-trln_discovery_spec} \
        --pmh-dataset ${OAI_SET:-trln_discovery_spec}

      TOKEN=$(cat tokens/resumptionToken.txt)
    done
    ;;

# HSE: added retries here because of timeouts
  enrich)
    # the folder (or box) number is provided as the 2nd argument
    ENRICH_DIR="enrich${2}"
    HARVEST_DIR="harvest${2}"
    PROCSTATS_DIR="procStats${2}"
    STAMPFILE="enrich-out-${2}.txt"
    DELETES_DIR="deletes${2}"
    MARC_RECORDS_DIR="marc${2}"
    ERRORS_DIR="errors${2}"
    FORMATTED_DIR="formatted${2}"

    # step 2: determine if the "enrichX" directory has a .locked "indicator" file or not
    # if the .locked file exists, this folder (or box) is currently being processed.
    # So, exit gracefully
    if [[ -e "$ENRICH_DIR/.locked" ]]; then
      exit 0
    fi

    # If there is a .processed "indicator flag, the folder has been processed
    # so do the same thing.
    if [[ -e "$ENRICH_DIR/.processed" ]]; then
      exit 0
    fi

    echo "running oai_pmh_harvest - enrich mode"
    # Otherwise, create the .locked file, then start processing
    touch $ENRICH_DIR/.locked && chmod a+rw $ENRICH_DIR/.locked
    oai_pmh_harvest \
      -enrich-all=true \
      -skip-harvest \
      -harvest-dir=$HARVEST_DIR \
      -enriched-dir=$ENRICH_DIR \
      -dsc-ini=/usr/local/etc/aws_credentials \
      -proc-stats-dir=$PROCSTATS_DIR \
      -stampfile=$STAMPFILE \
      -records-dir=$MARC_RECORDS_DIR \
      -formatted-xmldir=$FORMATTED_DIR \
      -errors-dir=$ERRORS_DIR \
      -deletes-dir=$DELETES_DIR \
      -interval 10 \
      -retries 20

    rm $ENRICH_DIR/.locked
    touch $ENRICH_DIR/.processed

    # Perhaps we trigger the process to start transforming 
    # MARC to ARGOT here, using $ENRICH_DIR..
    ;;

  #ingest)
  #  ./ingest.sh trln
  #  ;;

  *)
    exec "$@"
    ;;
esac
# Keep the container running
