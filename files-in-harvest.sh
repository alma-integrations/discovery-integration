#!/bin/bash

# liblog.sh provides the following routines:
# log()
# warn()
# error()
# debug()
. ./scripts/liblog.sh

filename=$(basename "$0")

ALMA_PIPELINE_SHARE="/share/alma-pipeline"
TIMESTAMP_DIRNAME=${1:?timestamp_dir is required}

paths=($(find ${ALMA_PIPELINE_SHARE}/harvest/${TIMESTAMP_DIRNAME} -name "*.xml"))
count=(${#paths[@]})

for fname in "${paths[@]}"
do
  line_count=($(wc -l $fname))
  if [ $line_count -le 3 ]; then
    # this harvest file doesn't have any records
    # rm $fname
    ((count--))
  fi
done

echo "$count"

