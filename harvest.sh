#!/bin/bash

# ASSUMPTIONS:
# - aws_credentials is located at /var/data/lsis/aws_credentials
# - latest version of "docker" is present and "docker compose" works

# Exit immediately if "harvest.pid" is present
if [ -e $ALMA_PIPELINE_SHARE/run/harvest.pid ]; then
  echo "Another job ($(cat $ALMA_PIPELINE_SHARE/run/harvest.pid)) is currently running. Skipping."
  exit 0
fi

BIBLAKE_DEBUG=false
[ -e "./scripts/liblog.sh" ] && . ./scripts/liblog.sh
[ ! -e "./scripts/liblog.sh" ] && . /usr/local/bin/scripts/liblog.sh

# Get the basename for logging purposes
filename=$(basename "$0")

ALMA_PIPELINE_SHARE="/share/alma-pipeline"
OAI_SET=${OAI_SET:-trln_discovery_spec}
TIMESTAMP_DIRNAME=${TIMESTAMP_DIRNAME:-}
DRY_RUN=${DRY_RUN:-false}
SHOW_CONFIG=${SHOW_CONFIG:-false}
RUN_HARVEST_NATIVE=${RUN_HARVEST_NATIVE:-false}

########## SIGNAL TRAPPING #############
function handle_ctrlc()
{
  rm ${ALMA_PIPELINE_SHARE}/run/ingest.run
  IFS=$OFS
}

# trap the SIGINT signal
trap handle_ctrlc SIGINT

########## SHOW HELP ##########
usage() {
  # Display help
  echo "Download a 'harvest' of OAI records from Alma"
  echo
  echo "Syntax: [$filename] [-options] CHANGES_FROM"
  echo "CHANGES_FROM: date in the format of YYYY-dd-mmTHH:MM:SSZ"
  echo "              value is auto-detected if absent"
  echo "Options:"
  echo "  -O, --oai-set               Published dataset to download [$OAI_SET]"
  echo "  -l, --local-run             Run in developer's local setup"
  echo "  -N, --run-native            Run native code instead of Docker image"
  echo "  -d, --dry-run               Show commands but don't run anything"
  echo "  -C, --show-config           Show the script's configuration, then exit"
  echo "  -h, --help                  Show this help, then exit"
  echo
}

# Process flags first (getopt)
SHORTOPTS="O:ldCNh"
LONGOPTS="oai-set:,local-run,dry-run,show-config,run-native,help"
OPTS=`getopt -o $SHORTOPTS -l $LONGOPTS -n '${filename}' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    -O | --oai-set ) OAI_SET="$2"; shift 2;;
    -d | --dry-run ) DRY_RUN=true; shift ;;
    -N | --run-native ) RUN_HARVEST_NATIVE=true; shift;;
    -C | --show-config ) SHOW_CONFIG=true; shift;;
    -l | --local-run ) ALMA_PIPELINE_SHARE="."; shift;;
    -h | --help ) usage; exit 0 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [ -z "$1" ]; then
  if [ -e $ALMA_PIPELINE_SHARE/timestamps/last-harvest.txt ]; then
    CHANGES_FROM=$(cat $ALMA_PIPELINE_SHARE/timestamps/last-harvest.txt)
  else
    CHANGES_FROM="2024-01-01T00:00:00Z"
  fi
else
  CHANGES_FROM="$1"
fi


NOW_EPOCH=$(date -d 'now' +%s)
TIMESTAMP_DIRNAME=${TIMESTAMP_DIRNAME:-$(date -d @${NOW_EPOCH} +"%Y-%m-%d-%H-00-00")}
TIMESTAMP_TZ=${TIMESTAMP_TZ:-$(date -d @${NOW_EPOCH} +"%Y-%m-%dT%H:00:00Z")}

# show config and exit if the option is specified
if [ "$SHOW_CONFIG" == true ]; then
  echo "ALMA_PIPELINE_SHARE: $ALMA_PIPELINE_SHARE"
  echo "OAI_SET: $OAI_SET"
  echo "TIMESTAMP_DIRNAME: $TIMESTAMP_DIRNAME"
  echo "TIMESTAMP_TZ: $TIMESTAMP_TZ"
  exit 0;
fi

# create the 'run' file
echo $TIMESTAMP_DIRNAME > $ALMA_PIPELINE_SHARE/run/harvest.pid

# Remove existing API tokens, as we don't care about 'em
rm -f $ALMA_PIPELINE_SHARE/tokens/*

# Create the new harvest directory
mkdir -m 0777 $ALMA_PIPELINE_SHARE/harvest/$TIMESTAMP_DIRNAME

if [ "$RUN_HARVEST_NATIVE" == true ]; then
  echo "Running native code is not implemented yet"
  exit 0
fi

./print-harvest-compose.sh $TIMESTAMP_DIRNAME \
  --oai-set ${OAI_SET:-trln_discovery_spec} \
  --changes-from $CHANGES_FROM > docker-compose-harvest.yml

# RUN THE HARVEST JOB by way of the docker container
# Build the docker image, the run it (using docker-compose-harvest.yml as context)
docker compose -f docker-compose-harvest.yml up --remove-orphans harvest-ci

# REVISIT LOGGING for the above call^^^^^^

# Run harvest XML file corrections (like removing errant `<record></record>`
# per "hse9" (Stewart)
sed -i '/<record><\/record>/d' $ALMA_PIPELINE_SHARE/harvest/$TIMESTAMP_DIRNAME/*.xml

mkdir -m 0777 $ALMA_PIPELINE_SHARE/soa-enrich/$TIMESTAMP_DIRNAME
mkdir -p -m -0777 $ALMA_PIPELINE_SHARE/formatted-xml/$TIMESTAMP_DIRNAME

./make-soa-dirs.sh $ALMA_PIPELINE_SHARE/harvest/$TIMESTAMP_DIRNAME $ALMA_PIPELINE_SHARE/soa-enrich/$TIMESTAMP_DIRNAME

./make-payload-dirs.pl $TIMESTAMP_DIRNAME -S soa-enrich/$TIMESTAMP_DIRNAME

# DEPRECATED file (next-soa-run.txt)
echo $TIMESTAMP_DIRNAME > $ALMA_PIPELINE_SHARE/timestamps/next-soa-run.txt
echo $TIMESTAMP_DIRNAME >> $ALMA_PIPELINE_SHARE/timestamps/available-harvests.txt

echo $TIMESTAMP_TZ > $ALMA_PIPELINE_SHARE/timestamps/last-harvest.txt

rm $ALMA_PIPELINE_SHARE/run/harvest.pid

