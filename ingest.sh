#!/bin/bash

# ingest.sh [-options] (trln|sandbox)

# originating author: Derrek Croney (dlc32)
# contributing author(s): Stewart Engart (hse9)

ALMA_PIPELINE_SHARE="/share/alma-pipeline"

# If the script's "run" file is present, then exit and allow the current job
# to complete
if [ -e "$ALMA_PIPELINE_SHARE/run/ingest.run" ]; then
  echo "there is an 'ingest' job already in progress. exiting..."
  exit 0
fi

NEXUS_DEBUG=false
[ -e "./scripts/liblog.sh" ] && . ./scripts/liblog.sh
[ ! -e "./scripts/liblog.sh" ] && . /usr/local/bin/scripts/liblog.sh

# Get the basename for logging purposes
filename=$(basename "$0")

# Process flags first (getopt)
SHORTOPTS="m:M:lA:S:df:I:i:CT:BDh"
LONGOPTS="max-batches:,max-transactions:,local-run,argot-dir:,spofford-cfg:,dry-run,file-ext:,ingest-interval:,ingest-batch-count:,show-config,stop-ingest-at:,disable-batch,delete-argot-files,deletes,help"
OPTS=`getopt -o ${SHORTOPTS} -l ${LONGOPTS} -n '${filename}' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

# The 'argot' directory is relative to $WHERE (aka the argument passed to us)
ARGOTDIR="generated-argot"

# When ingesting argot files, this is the default extension for our file (search) glob
FILE_EXT="json"

INGEST_QUEUE_DIR="ingest-queue"

# FLAGs
DRY_RUN=${DRY_RUN:-false}
RUN_DELETE_JOBS=${RUN_DELETE_JOBS:-false}
INGEST_INTERVAL=${INGEST_INTERVAL:-"20m"}
INGEST_BATCH_COUNT=${INGEST_BATCH_COUNT:-5}
MAX_BATCHES=${MAX_BATCHES:-0}
MAX_TRANSACTIONS=${MAX_TRANSACTIONS:-0}
DISABLE_BATCH=${DISABLE_BATCH:-false}
[ -n "$STOP_INGEST_AT" ] && STOP_INGEST_AT=$(date -d "$STOP_INGEST_AT" +%s) || STOP_INGEST_AT=false
STOP_INGEST_AT=${STOP_INGEST_AT:-false}
SHOW_CONFIG=${SHOW_CONFIG:-false}
DELETE_ARGOT_FILES=${DELETE_ARGOT_FILES:-false}
SPOFFORD_CONFIG=""

########## SIGNAL TRAPPING #############
function handle_ctrlc()
{
  rm ${ALMA_PIPELINE_SHARE}/run/ingest.run
  IFS=$OFS
}

# trap the SIGINT signal
trap handle_ctrlc SIGINT

########## SHOW HELP ##########
usage() {
  # Display help
  echo "Initiate ingest of records to a Solr index via Spofford"
  echo
  echo "Syntax: $filename [-options] SPOFFORD_INSTANCE"
  echo "where SPOFFORD_INSTANCE is one of 'trln' (default) or 'sandbox'"
  echo "Options:"
  echo "  -i, --ingest-batch-count    Number of transactions to start in batch mode [${INGEST_BATCH_COUNT}]"
  echo "      --deletes               Initiate 'delete' transactions (instead of add/updates)"
  echo "  -m, --max-batches           Max number of 'transaction' batches to process [$MAX_BATCHES]"
  echo "  -M, --max-transactions      Max number of individual transactions to process [$MAX_TRANSACTIONS]"
  echo "                              (implies --disable-batch)"
  echo "  -I, --ingest-interval       Number of seconds to wait before sending the next batch [${INGEST_INTERVAL}]"
  echo "  -S, --spofford-cfg          Specify your spofford config file [${SPOFFORD_CONFIG}]"
  echo "  -B, --disable-batch         Start transactions without stopping [${DISABLE_BATCH}]"
  echo "  -T, --stop-ingest-at        Stop this process at this time, in 'YYYY-mm-dd HH:MM:SS' format"
  echo "  -f, --file-ext              Look for Argot files with this file extension [${FILE_EXT}]"
  echo "  -l, --local-run             Run in developer's local setup"
  echo "  -C, --show-config           Show the script's configuration, then exit"
  echo "  -D, --delete-argot-files    Delete JSON files after creating ingest transactions [${DELETE_ARGOT_FILES}]"
  echo "                              (disables using .ingested marker file)"
  echo "  -d, --dry-run               Do a 'dry' run, but don't actually send transactions [$DRY_RUN]"
  echo "  -h, --help                  Show this help, then exit"
  echo
}

while true; do
  case "$1" in
    -A | --argot-dir ) ARGOTDIR="$2"; shift 2;;
    -d | --dry-run ) DRY_RUN=true; shift ;;
    --deletes ) RUN_DELETE_JOBS=true; shift;;
    -f | --file-ext ) FILE_EXT="$2"; shift 2;;
    -S | --spofford-cfg ) SPOFFORD_CONFIG="$2"; shift 2;;
    -I | --ingest-interval ) INGEST_INTERVAL="$2"; shift 2;;
    -m | --max-batches ) MAX_BATCHES="$2"; shift 2;;
    -M | --max-transactions ) MAX_TRANSACTIONS="$2"; DISABLE_BATCH=true; shift 2;;
    -i | --ingest-batch-count ) INGEST_BATCH_COUNT="$2"; shift 2;;
    -B | --disable-batch ) DISABLE_BATCH=true; shift;;
    -C | --show-config ) SHOW_CONFIG=true; shift;;
    -T | --stop-ingest-at ) STOP_INGEST_AT=$(date -d "$2" +%s); shift 2;;
    -l | --local-run ) ALMA_PIPELINE_SHARE="."; shift;;
    -D | --delete-files ) DELETE_ARGOT_FILES=true; shift;;
    -h | --help ) usage; exit 0 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

# Currently, this Bash script will process XML files located in 
# the directory provided in the first argument $1

# $1 represents the parent directory holding XML files

SOLR_INSTANCE_DIR=${1:-trln}

# By default for spofford config files, use a config file that 
# points to either a locally-hosted instance, or a sandbox instance.
if [ -z "$SPOFFORD_CONFIG" ]; then 
  SPOFFORD_CONFIG="${ALMA_PIPELINE_SHARE}/conf/spofford-prod.yml"
  if [ "$SOLR_INSTANCE_DIR" == "sandbox" ]; then SPOFFORD_CONFIG="${ALMA_PIPELINE_SHARE}/conf/spofford-sandbox.yml"; fi
fi

# This is WHERE we'll locate our JSON files (or *.${FILE_EXT}) from ...
WHERE="${ALMA_PIPELINE_SHARE}/${INGEST_QUEUE_DIR}/${SOLR_INSTANCE_DIR}"
if [ ! -d "$WHERE" ]; then
  error "!!! $WHERE does not exist. Exiting."
  exit 1
fi

# A "transactions" folder is located (or should be) under $WHERE
# We'll store the individual Spofford transaction IDs here
TRANSACTIONS_DIR="${WHERE}/transactions"

LOG_FILE="${ALMA_PIPELINE_SHARE}/logs/ingest-${SOLR_INSTANCE_DIR}.log"

# Locate JSON files, except for the ones that are prefixed 'add-enriched*'
# as those files are merged (via 'cat') -- so we'd be double-ingesting
#
# this glob will also include "deletes" and since we're sorting in alphabetical 
# order, this will take care of the add/update, then deletes ordering.
if [ "$RUN_DELETE_JOBS" == true ]; then NAME_PREDICATE="*delete*"; else NAME_PREDICATE="*add*"; fi

#paths=($(find $WHERE -name "*.${FILE_EXT}" ! -path "*add-enriched*" | sort -z))
paths=($(find $WHERE -type f -name "$NAME_PREDICATE" ! -path "*add-enriched*" | sort -z))
if [ ${#paths[@]} -eq 0 ]; then info "There are no files, of the pattern '$NAME_PREDICATE', to process. Exiting."; exit 0; fi
how_many_files=${#paths[@]}

info "Spofford Config = [$SPOFFORD_CONFIG]"
info "Argot files located at : ${WHERE}"
info "Transactions located at : ${TRANSACTIONS_DIR}"
info "Log file: ${LOG_FILE}"
info "File extension: ${FILE_EXT}"
[ "$DISABLE_BATCH" == false ] && info "Transactions Per Batch = [$INGEST_BATCH_COUNT]" && info "Ingest (batch) interval = [$INGEST_INTERVAL]"
[ "$DISABLE_BATCH" == false ] && info "Max Batches = [$MAX_BATCHES]"
[ "$DISABLE_BATCH" == true ] && info "Max Transactions = [$MAX_TRANSACTIONS]"
[ ! "$STOP_INGEST_AT" == false ] && info "Stop Ingest at = [$(date -d @${STOP_INGEST_AT})]"
info "Files to process: ${#paths[@]}"

[ "$SHOW_CONFIG" == true ] && exit 0;

# Adjust the Input File Separator so we can glob a file listing
OFS=$IFS
IFS=$'\n'

info "[$filename] processing ${#paths[@]} JSON files / starting..."

# these patterns will need to be revisited if/when 
# Spofford is upgraded in a way that results in its
# output changing.
success_pattern="Upload accepted"
transaction_pattern='^(.*)Transaction ID: ([0-9]{1,})'

# Keeps track of loop iterations (still not sure if needed)
counter=1

# Keeps track of how many files have been sent to Spofford
files_ingested=0
batches_completed=0

last_file_processed=""

RESUMING=false
[[ -f "$WHERE/.ingested" ]] && RESUMING=true
[[ ! -f "$WHERE/.ingested" ]] && [[ "$DELETE_ARGOT_FILES" == false ]] && touch "$WHERE/.ingested"

if [ $how_many_files -eq 0 ]; then
  info "There are no argot files to process. Exiting."
  exit 0
fi

echo $(date "+%s") > ${ALMA_PIPELINE_SHARE}/run/ingest.run

############# MAIN PROCESSING BODY ##############

for i in "${paths[@]}"
do
  # if NOW is after STOP_INGEST_AT, then stop ingesting...
  NOW=$(date +%s)
  if [ ! "$STOP_INGEST_AT" == false ] && [ $NOW -gt $STOP_INGEST_AT ]; then
    info "[$filename] stop ingest at $(date -d @${STOP_INGEST_AT})"
    break
  fi

  CAN_INGEST=true
  if [ "$RESUMING" == true ]; then
    grep_result=$(grep -l "$i" $WHERE/.ingested)
    if [ -n "$grep_result" ]; then
      CAN_INGEST=false
    fi
  fi

  if [ "$CAN_INGEST" == true ]; then 
    info "[$filename] [$counter of $how_many_files] Ingesting: $i"
  else
    info "[$filename] [$counter of $how_many_files] Skipping: $i"
    ((counter++))
    continue
  fi

  if [ "$DRY_RUN" == false ]; then
    # Run "spofford ingest", and test for successful upload.
    outp=`/usr/local/bin/spofford ingest --verbose --config=${SPOFFORD_CONFIG} $i`
    if [[ "$outp" =~ $success_pattern ]]; then
      info "[$filename] UPLOADED $i"

      [[ "$outp" =~ $transaction_pattern ]]
      if [ -n "${BASH_REMATCH[2]}" ]; then
        transaction_id="${BASH_REMATCH[2]}"
        info "[$filename] transaction id: ${BASH_REMATCH[2]}"
        echo $transaction_id > "${TRANSACTIONS_DIR}/${transaction_id}.id"
      fi

      # either remove the JSON file, or make note that 
      # the script "processed it"
      if [ "$DELETE_ARGOT_FILES" == false ]; then echo "$i" >> $WHERE/.ingested; fi
      if [ "$DELETE_ARGOT_FILES" == true ]; then rm $i; fi
      #if [ "$DELETE_ARGOT_FILES" == true ]; then rm $i; echo "$i --> removed" >> $WHERE/.ingested; fi
    else
      error "[$filename] FAILED UPLOAD on $i"
    fi
    # debug $outp
  else
    # Display what would be the call made to "spofford ingest", but don't run it
    info "[$filename][DRY-RUN] spofford ingest --verbose --config=${SPOFFORD_CONFIG} $i"
  fi
  ((files_ingested++))

  # Keep track of the last file processed
  # (this will be reported at the end of the run)
  last_file_processed=$(basename $i)

  info "[$filename] done with $i..."

  if [[ "$DISABLE_BATCH" == true ]]; then
    if [ $MAX_TRANSACTIONS -gt 0 ]; then
      if [ $files_ingested -ge $MAX_TRANSACTIONS ]; then
        info "Reached maximum transactions to process"
        break
      fi
    fi
  else
    # we're in batch mode.
    mod=$(($files_ingested % INGEST_BATCH_COUNT))
    if [ $mod -eq 0 ]; then
      ((batches_completed++))
      if [ $MAX_BATCHES -eq 0 ]; then
        info "Waiting ${INGEST_INTERVAL} before running next batch of ${INGEST_BATCH_COUNT} transaction(s)"
        sleep ${INGEST_INTERVAL}
      elif [ $batches_completed -ge $MAX_BATCHES ]; then
        info "Reached maximum batches to process"
        break
      fi
    fi
  fi
  ((counter++))
done

IFS=$OFS

rm ${ALMA_PIPELINE_SHARE}/run/ingest.run

info "Last file processed: $last_file_processed"
info "[$filename] processing done..."

# Find and delete files older than the target date
INGEST_DIR="$ALMA_PIPELINE_SHARE/$INGEST_QUEUE_DIR/trln"
# TARGET_DATE is the date of which to keep ingest files
TARGET_DATE=$(date -d "-30 days" +%Y-%m-%d)
if find "$INGEST_DIR" -maxdepth 1 -type f -not -newermt "$TARGET_DATE" | read; then
        printf "%s - Finding and deleting files older than %s in %s\n" "$(date +'%Y-%m-%d %H:%M:%S')" "$TARGET_DATE" "$INGEST_DIR"
        find "$INGEST_DIR" -maxdepth 1 -type f -not -newermt "$TARGET_DATE" -exec printf "Deleting file: %s\n" {} \; -exec rm {} \;
else
        printf "All files older than the %s in %s are already deleted\n" "$TARGET_DATE" "$INGEST_DIR"
fi
