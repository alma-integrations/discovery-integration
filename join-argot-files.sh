#!/bin/bash

# Join 5 (or files_per_dir) argot .json files together -- using 'cat'
# to create one file containing at most 50,000 records that can be sent
# to Spofford (TRLN) for Solr ingest

# liblog.sh provides the following routines:
# log()
# warn()
# error()
# debug()
. ./scripts/liblog.sh

files_per_dir=5

# Get the basename for logging purposes
filename=$(basename "$0")

RED="\e[31m"
BLUE="\e[34m"
YELLOW="\e[33m"
ENDCOLOR="\e[0m"


# BASE_ARGOT_DIR=/share/alma-pipeline/resends/argot
WHERE=$1
ALMA_PIPELINE_SHARE="/share/alma-pipeline"

ARGOT_DIRNAME="argot-processed"
ARGOT_INGEST_DIRNAME="argot-ready"
FILE_EXT="json"

BASE_RESENDS_DIR=/share/alma-pipeline/resends

#[ ! -e "$WHERE/$ARGOT_INGEST_DIRNAME" ] && echo "Creating $WHERE/$ARGOT_INGEST_DIRNAME ..." && mkdir -m 0777 "$WHERE/$ARGOT_INGEST_DIRNAME"

set -- ${WHERE}/${ARGOT_DIRNAME}/*.${FILE_EXT}

n=0
counter=1
per_page=5

ndx=0
big_argot=()
for filename do
  printf "[%d] %s\n" "${counter}" "${filename}"
  big_argot[$ndx]="$filename"
  ((ndx++))
  mod=$(($counter % per_page))
  if [[ $mod -eq 0 ]]; then
    printf "${BLUE}combining ${#big_argot[@]} files...${ENDCOLOR}\n"
    #cat $(echo ${big_argot[*]}) > /share/alma-pipeline/resends/argot-to-ingest/add-${counter}.json
    cat $(echo ${big_argot[*]}) > $ALMA_PIPELINE_SHARE/ingest-queue/trln/add-$(date -d "now" "+%s").json
    ndx=0
    big_argot=()
  fi
  ((counter++))
done
echo "size of big_argot is: ${#big_argot[@]}"
if [ ${#big_argot[@]} -gt 0 ]; then cat $(echo ${big_argot[*]}) > $ALMA_PIPELINE_SHARE/ingest-queue/trln/add-$(date -d "now" "+%s").json; fi
# cat $(echo ${big_argot[*]}) > /share/alma-pipeline/resends/argot-to-ingest/add-${counter}.json
