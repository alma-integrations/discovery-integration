#!/usr/bin/perl
#
use strict;
use Getopt::Long;

Getopt::Long::Configure(qw( gnu_getopt ));

# alma_pipeline_dir refers to the share location 
# for alma pipeline.
#
# since this program is (by default) designed to run on 
# lsis-ci-01, default to '/share/alma-pipeline', allowing 
# callers to use -D (or --pipeline-dir) to change it.
my $alma_pipeline_dir = "/share/alma-pipeline";

# soa_dir is where we look for "enrichXX" directories
# default to current working directory.
#
# callers, use -S (or --soa-dir) to override
my $soa_dir = "soa-enrich";

# payload "base" directory (typically 'payload')
my $payload_base_dir = 'payload';

# display the settings, then exit
my $early_exit = 0;

my $dev_run = 0;

Getopt::Long::GetOptions(
  'pipeline-dir|D=s' => \$alma_pipeline_dir,
  'soa-dir|S=s' => \$soa_dir,
  'payload-base-dir|P=s' => \$payload_base_dir,
  'early-exit|E' => sub { $early_exit = 1 },
  'dev-run|d' => sub { $dev_run = 1; $alma_pipeline_dir = '.' },
  'help|h' => \&print_usage
);

sub print_usage {
  my $usage = <<'END_USAGE';
usage: make-payload-dirs.pl [options] TIMESTAMP_DIRNAME

where "TIMESTAMP_DIRNAME" is a directory where "loader-NN" folders will be created.
Each "loader-NN" folder will contain files named "enrichN", representing 
a job to be processed.

Options:
-D, --pipeline-dir=[alma-pipeline-dir]; (default: "/share/alma-pipeline")
-S, --soa-dir=[soa-context-folder] (folder containing "enrichNN" directories; default ".")
-P, --payload-base-dir=[location-of-payload-dir] (default: "payload")
-E, --early-exit (exit before processing)
-d, --dev-run (ignore "alma share" folders, run in local dev folder)
-h, --help (this help)
END_USAGE

  print "$usage\n\n";
  exit 0;
}

die "$0 requires 1 argument: location of 'payload' folder to be created\n" if scalar(@ARGV) != 1;

my $payload_dir_arg = shift @ARGV;
my $payload_dir = "$payload_base_dir/$payload_dir_arg";
$payload_dir = "$alma_pipeline_dir/$payload_dir" unless $dev_run;

$soa_dir = $soa_dir . "/$payload_dir_arg";

my $enrich_dirs_location = $alma_pipeline_dir ?
  "$alma_pipeline_dir/$soa_dir" :
  $soa_dir;

print "alma_pipeline_dir = [$alma_pipeline_dir]\nsoa-dir = [$soa_dir]\nenrich_dirs_location = [$enrich_dirs_location]\npayload_dir=[$payload_dir]\n";

my @enrichdirs = glob("$enrich_dirs_location/enrich*");
my $total_folders = scalar @enrichdirs;
print "total_folders = [$total_folders]\n";

exit 0 if $early_exit;

(!-e $payload_dir) && `mkdir -m 0777 $payload_dir`;

# We know of ten(10) lsis-loader VMs, so we'll model them here:
my @servers = ((), (), (), (), (), (), (), (), (), ());

# For those times when there are 3 (or less) "enrichNN" 
# directories, simply store the 'payload' data on the first 
# VM.

my @a = (1..$total_folders);
my $server = 1;

# First pass, "deal" the 'enrichX' folder names to the 10 arrays
for (@a) {
  push @{$servers[$server-1]}, "enrich$_";
  $server++;
  $server = 1 if $server > 10;
}

# Second pass, create "marker" files
for (my $i = 0; $i < scalar(@servers); $i++) {
  my $loader_dirname = sprintf("%s/loader-%02d", $payload_dir, $i+1);
  `mkdir -m 0777 $loader_dirname`;
  map {
    print "$_\n";
    `touch $loader_dirname/$_; chmod a+rw $loader_dirname/$_`;
  } @{$servers[$i]};
}
__END__
