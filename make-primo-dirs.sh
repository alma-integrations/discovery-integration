#!/bin/bash

files_per_dir=150

ALMA_PIPELINE_SHARE="${1:-/share/alma-pipeline}"

# this currently is/was not being used but might be in the future
# ALMA_INSTANCE="production"

# Instead of building an array, just iterate through them
# Find files and process them in batches
find "$ALMA_PIPELINE_SHARE/primo" -type f -name "*IEP_*" | while read -r filename; do
  n=$((n + 1))
  N=$(( (n / files_per_dir) + 1 ))
  dir="$ALMA_PIPELINE_SHARE/primo/dir_$N"

  if [ ! -d "$dir" ]; then
    mkdir -m 0777 -p "$dir" || { echo "Failed to create directory $dir"; exit 1; }
    echo "Created directory $dir"
  fi

  mv "$filename" "$dir" || { echo "Failed to move $filename to $dir"; exit 1; }
done


