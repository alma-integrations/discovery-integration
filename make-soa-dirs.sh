#!/bin/bash

# make-soa-dirs.sh [options] HARVEST_DIR DESTINATION_DIR

# takes two(2) arguments as well as CLI options
# $HARVEST_DIR -- first argument is the 'harvest' directory (required)
# $DEST_DIR -- second arugment is where you want the folder structure created

files_per_dir=11

filename=$(basename "$0")

HARVEST_FILE_PREFIX="harvest*"

# process cli options
# -f | --files-per-dir (default is 11)
# -D | --destination-dir (default is './pipeline')

OPTS=`getopt -o f:p: -l files-per-dir:,harvest-file-prefix: -n '${filename}' -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2; exit 1; fi

eval set -- "$OPTS"
while true; do
  case "$1" in
    -f | --files-per-dir ) files_per_dir=$2; shift 2 ;;
    -p | --harvest-file-prefix ) HARVEST_FILE_PREFIX=$2; shift 2 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

HARVEST_DIR=$1
DEST_DIR=${2:-./pipeline}

printf "HARVEST_DIR = [%s]; DEST_DIR = [%s]; files-per-dir = [%s]\n" "$HARVEST_DIR" "$DEST_DIR" "$files_per_dir"

[ ! -d $DEST_DIR ] && echo "creating destination dir ($DEST_DIR)" && mkdir -m 0777 $DEST_DIR

# set -- published/discovery/IEP__*
# set -- /home/dlc32/lib-datastore/alma/data/retakes/IEP__*
# set -- /home/dlc32/Workspace/alma/discovery-integration/volumes/harvest/harvest*
set -- $HARVEST_DIR/$HARVEST_FILE_PREFIX

n=0

for filename do
  n=$(( n + 1 ))
  N=$(( (n/files_per_dir) + 1 ))
  [ ! -d "${DEST_DIR}/harvest$N" ] && 
    mkdir -m 0777 "${DEST_DIR}/harvest$N" && 
    mkdir -m 0777 "${DEST_DIR}/enrich$N" &&
    mkdir -m 0777 "${DEST_DIR}/errors$N" &&
    mkdir -m 0777 "${DEST_DIR}/formatted$N" &&
    mkdir -m 0777 "${DEST_DIR}/procStats$N" &&
    mkdir -m 0777 "${DEST_DIR}/stats$N" && 
    mkdir -m 0777 "${DEST_DIR}/deletes$N" &&
    mkdir -m 0777 "${DEST_DIR}/marc$N" &&
    echo "folder: $N"
  cp $filename ${DEST_DIR}/harvest$N
done

