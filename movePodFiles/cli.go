package main

import (
	"flag"
)

// CmdLineService
type CmdLineService struct {
	ConfigFilename string

	// This is the URL for the chosen POD Stream
	podURL string
	// This is a name supplied to POD for the upload
	uploadName string
	// This is where the program will look for delete files to send to POD
	podDir string
	// This is where the program will look for add files to send to POD
	harvestDir string
	// This is where the program will look for config file with the POD secret
	configFile string
}

func NewCmdLineService() *CmdLineService {
	cs := CmdLineService{}
	flag.StringVar(&cs.podURL, "pod-url", "https://pod.stanford.edu/organizations/duke/uploads?stream=2024-07-10", "POD URL, including which stream to publish")
	flag.StringVar(&cs.uploadName, "upload-name", "Duke Incremental Upload", "name of upload")
	flag.StringVar(&cs.podDir, "pod-dir", "../oai_pmh_harvest/pod", "location of delete file(s)")
	flag.StringVar(&cs.harvestDir, "harvest-dir", "../oai_pmh_harvest/harvest", "location of add file(s)")
	flag.StringVar(&cs.configFile, "config-file", "config.yaml", "location of config file")

	flag.Parse()

	return &cs
}
