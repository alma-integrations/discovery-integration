package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/fs"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	journal "gitlab.oit.duke.edu/dul-go/journal"
	"go.uber.org/dig"
	"gopkg.in/yaml.v2"
)

var podURL string
var config conf
var uploadName string
var podDir string
var harvestDir string
var configFile string

type conf struct {
	Secret string `yaml:"secret"`
}

func BuildContainer(container *dig.Container) {
	container.Provide(NewCmdLineService)
}

func main() {
	// Intialize logging settings
	journal.InitLogging(os.Stdout, // DEBUG
		os.Stdout, // INFO
		os.Stdout, // WARNING
		os.Stdout, // ERROR
	)

	// Deal with CLI
	container := dig.New()
	BuildContainer(container)
	err := container.Invoke(func(cli *CmdLineService) {
		if cli.podURL != "" {
			podURL = cli.podURL
		}
		if cli.uploadName != "" {
			uploadName = cli.uploadName
		}
		if cli.podDir != "" {
			podDir = cli.podDir
		}
		if cli.harvestDir != "" {
			harvestDir = cli.harvestDir
		}
		if cli.configFile != "" {
			configFile = cli.configFile
		}
	})
	if err != nil {
		journal.Debug.Fatal(err)
	}

	config.getConfig()
	//journal.Debug.Println(config.Secret)
	//journal.Debug.Println(podURL)
	//journal.Debug.Println(uploadName)
	//journal.Debug.Println(podDir)
	//journal.Debug.Println(harvestDir)
	//journal.Debug.Println(configFile)

	// Send Delete Files
	root := podDir
	journal.Debug.Println(root)
	exts := []string{"txt"}
	files, err := WalkDir(root, exts)
	journal.Debug.Println(files)
	if err != nil {
		journal.Debug.Fatal(err)
	}
	for _, file := range files {
		if strings.Contains(file, "deleted") {
			// if file is 0 bytes, delete it
			fi, err := os.Stat(file)
			if err != nil {
				journal.Debug.Fatal(err)
			}
			if fi.Size() == 0 {
				// Need to figure out what to do with Delete Files...currently not letting them get deleted
				//err := os.Remove(file)
				if err != nil {
					journal.Debug.Fatal(err)
				}
				journal.Debug.Printf("Deleted Empty Delete File: %s\n", file)
			}
			sendToPod(file)
			journal.Debug.Printf("Sending Delete File:  %s\n", file)
		} else {
			if strings.Contains(file, "add") {
				sendToPod(file)
				journal.Debug.Printf("Sending Add File:  %s\n", file)
			} else {
				journal.Debug.Printf("File does not contain 'add' or 'delete': %s\n", file)
			}
		}
	}

	// Rename, Gzip, and Send Harvest Files
	addRoot := harvestDir
	addExts := []string{"xml"}
	addFiles, err := WalkDir(addRoot, addExts)
	if err != nil {
		journal.Debug.Fatal(err)
	}
	journal.Debug.Println(addFiles)
	//os.Exit(1)
	for _, file := range addFiles {
		journal.Debug.Printf("Copying Harvest File:  %s\n", file)
		renameHarvestSendToPod(file)
	}

}

func (c *conf) getConfig() *conf {
	yamlFile, err := os.ReadFile(configFile)
	if err != nil {
		journal.Debug.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		journal.Debug.Printf("Unmarshal: %v", err)
	}

	return c
}

func WalkDir(root string, exts []string) ([]string, error) {
	var files []string
	err := filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}

		for _, s := range exts {
			if strings.HasSuffix(path, "."+s) {
				files = append(files, path)
				return nil
			}
		}

		return nil
	})
	return files, err
}

func sendToPod(fileName string) {
	form := new(bytes.Buffer)
	writer := multipart.NewWriter(form)
	formField, err := writer.CreateFormField("upload[name]")
	if err != nil {
		journal.Debug.Fatal(err)
	}

	name := uploadName + time.Now().Format("20060102150405")
	_, err = formField.Write([]byte(name))
	if err != nil {
		journal.Debug.Fatal(err)
	}

	fw, err := writer.CreateFormFile("upload[files][]", filepath.Base(fileName))
	if err != nil {
		journal.Debug.Fatal(err)
	}
	fd, err := os.Open(fileName)
	if err != nil {
		journal.Debug.Fatal(err)
	}
	defer fd.Close()
	_, err = io.Copy(fw, fd)
	if err != nil {
		journal.Debug.Fatal(err)
	}

	writer.Close()

	client := &http.Client{}
	//journal.Debug.Println(form)
	req, err := http.NewRequest("POST", podURL, form)
	if err != nil {
		journal.Debug.Fatal(err)
	}
	req.Header.Set("Authorization", config.Secret)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp, err := client.Do(req)
	if err != nil {
		journal.Debug.Fatal(err)
	}
	defer resp.Body.Close()
	//bodyText, resp_err := io.ReadAll(resp.Body)
	_, resp_err := io.ReadAll(resp.Body)
	if resp_err != nil {
		journal.Debug.Fatal(resp_err)
	}
	//journal.Debug.Printf("%s\n", bodyText)
	journal.Debug.Println("Sent to POD")
}

func gzipXMLfile(fileName string) string {
	inFile, err := os.Open(fileName)
	if err != nil {
		journal.Debug.Println(err)
	}
	read := bufio.NewReader(inFile)

	data, err := io.ReadAll(read)
	if err != nil {
		fmt.Println(err)
	}

	fileName = strings.Replace(fileName, ".xml", ".xml.gz", -1)

	outFile, err := os.Create(fileName)
	if err != nil {
		fmt.Println(err)
	}

	w := gzip.NewWriter(outFile)
	w.Write(data)
	journal.Debug.Println("XML compressed successfully into xml.gz")
	w.Close()
	return fileName
}

func renameHarvestSendToPod(fileName string) {
	formattedDatetime := time.Now().Format("20060102150405")
	dst := fmt.Sprintf("%s/add_%s_00_mins.xml", podDir, formattedDatetime)
	journal.Debug.Println("Renamed to " + dst)
	// TODO: Copy instead of rename
	e := os.Rename(fileName, dst)
	if e != nil {
		journal.Debug.Fatal(e)
	}
	XMLGZFile := gzipXMLfile(dst)
	journal.Debug.Println("Gzipped File")
	sendToPod(XMLGZFile)
}
