#!/bin/bash
while read p; do
echo "https://find.library.duke.edu/catalog/$p"
if wget -q --method=HEAD https://find.library.duke.edu/catalog/$p;
 then
  echo "This page exists."
 else
  echo "This page does not exist."
fi
done <$1
