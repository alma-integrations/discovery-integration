package main

import (
	"flag"
)

// CmdLineService tracks provided flags and resolves
// differences between cli args and config settings.
type CmdLineService struct {
	ConfigFilename string

	// OutDirectory is used to store the "OutFilename" file when
	// processing an OAI Harvest.
	// These are ignored when BypassHarvest is true
	HarvestDir      string
	HarvestFilename string

	// When BypassHarvest is false, InDirectory and InFilename are
	// set to the values of OutDirectory and OutFilename (respectively)
	//
	// When BypassHarvest is true, the caller (CMDLINE, etc) will need to
	// provide these values via "-indir" and "infile"
	EnrichedDir      string
	EnrichedFilename string
	FormattedXMLDir  string

	ErrorsDir string

	UnprocessedDir string

	EnrichAllFiles    bool
	HarvestFileSuffix string

	//// Location to read the Resumption Token
	//// Location to store the Resumption Token
	//RTFileIn  string
	//RTFileOut string

	// MaxRetries, RetryInterval and MaxPages are used
	// when processing an OAI Harvest
	MaxRetries    int
	RetryInterval int
	MaxPages      int

	// INI file location providing AWS credentials used to
	// connect to our Data Service Center (DSC)
	DSCIniFile string

	// Instead of running the entire process, we can skip
	// parts if needed
	BypassEnrichment bool
	BypassHarvest    bool

	// ResumptionToken fields
	ResToken        string
	ResTokenInFile  string
	ResTokenOutFile string

	// Not used yet
	TextOutputOnly bool

	// The name of the desired "timestamp" file
	TimestampFilename string

	// Process Stats file directory
	ProcessStatsDir string

	// Location of the location file
	LocFile string

	// Deletes Directory
	DeletesDir string

	// OAI-PMH published set name from Alma
	OaiSet string

	// DSC table name
	DscTable string

	// Skip enrichment and make the the delete file
	DeleteOnly bool

	// Set the incremental harvest date
	FromDate string

	// Set the name of the AWS table to read availability data from
	AwsTableName string

	// Folder where individual MARC record XML files are stored
	MarcRecordsDir string

	// Any remaining arguments after parsing the flag options
	CmdLineArgs []string

	// Test Mail Service
	MailTest bool

	// PMH Command line options
	PMHHostname    string
	PMHDataset     string
	PMHUrlProtocol string
}

// NewCmdLineService sets up the CLI flags for the program
func NewCmdLineService() *CmdLineService {
	cs := CmdLineService{}

	// General flags (applicable application wide)
	flag.StringVar(&cs.ConfigFilename, "cfg", "oph_config.yml", "config file location")
	flag.StringVar(&cs.DSCIniFile, "dsc-ini", "../aws_credentials", "INI file with Data Service Center creds")
	flag.BoolVar(&cs.TextOutputOnly, "text", false, "true = only display text output from XML processing")
	flag.StringVar(&cs.ErrorsDir, "errors-dir", "errors", "Directory where various error notes are kept")

	// PMH settings
	flag.StringVar(&cs.PMHHostname, "pmh-host", "", "override the default 'Duke Alma' PHM hostname")
	flag.StringVar(&cs.PMHDataset, "pmh-dataset", "trln_discovery_spec", "Name of OAI-PMH set published from Alma")
	flag.StringVar(&cs.PMHUrlProtocol, "pmh-url-protocol", "", "https:// or http://")

	// Flags for harvest and enrich
	flag.StringVar(&cs.HarvestDir, "harvest-dir", "harvest", "location of xml file(s)")
	flag.StringVar(&cs.HarvestFilename, "harvest-file", "harvest.xml", "records XML filename")
	flag.StringVar(&cs.EnrichedDir, "enriched-dir", "enriched", "Directory location of OAI harvested XML file")
	flag.StringVar(&cs.EnrichedFilename, "enriched-file", "enriched.xml", "Harvest records XML filename")
	flag.IntVar(&cs.MaxRetries, "retries", 5, "Max Retries")
	flag.IntVar(&cs.RetryInterval, "interval", 2, "Interval between retries, in seconds")
	flag.IntVar(&cs.MaxPages, "pages", 3, "Number of pages to 'harvest'")
	flag.StringVar(&cs.DscTable, "dsc-table", "alma-availability-production-20241031", "Name of DSC Table to do lookup for availability number")
	flag.StringVar(&cs.AwsTableName, "aws-table-name", "", "Name of table in AWS")

	// Flags for harvest
	flag.BoolVar(&cs.BypassHarvest, "skip-harvest", false, "Skip the OAI harvest process")
	flag.StringVar(&cs.HarvestFileSuffix, "harvest-suffix", "xml", "Suffix of harvest files to process, when enrich-all=true")
	flag.StringVar(&cs.ResTokenInFile, "resTokenIn", "", "File to store last known OAI 'resumption' token")
	flag.StringVar(&cs.ResTokenOutFile, "resTokenOut", "resumptionToken.txt", "Filename to write resumption token")
	flag.StringVar(&cs.ResToken, "resToken", "", "Use this token to resume reading OAI data")
	flag.StringVar(&cs.OaiSet, "oai-set", "trln_discovery_spec", "Name of OAI-PMH set published from Alma")
	flag.StringVar(&cs.FromDate, "changes-from", "", "Date to start harvesting changes")

	// Flags for enrich
	flag.BoolVar(&cs.EnrichAllFiles, "enrich-all", false, "Enrich all files XML files in 'harvest-dir'")
	flag.BoolVar(&cs.BypassEnrichment, "skip-enrich", false, "Skip the availability enrichment process")
	flag.StringVar(&cs.TimestampFilename, "stampfile", "oai-timestamp.txt", "Use this file to record job timestamp")
	flag.StringVar(&cs.LocFile, "location-file", "locations.json", "Location of location file")
	flag.StringVar(&cs.DeletesDir, "deletes-dir", "deletes", "Location of deletes files")
	flag.BoolVar(&cs.DeleteOnly, "delete-only", false, "true = only process deletes")
	flag.StringVar(&cs.FormattedXMLDir, "formatted-xmldir", "", "directory to store 'formatted XML' if desired")
	flag.StringVar(&cs.ProcessStatsDir, "proc-stats-dir", "", "Store process stats in this directory")
	flag.StringVar(&cs.MarcRecordsDir, "records-dir", "marc", "Folder where individual MARC record XML files are stored.")

	// Miscellaneous or not currently used
	flag.StringVar(&cs.UnprocessedDir, "unprocessed-dir", "unprocessed", "Folder for un-enriched XML files")
	flag.BoolVar(&cs.MailTest, "test-mail", false, "Testing for mail service")

	// Parse the command-line flags
	flag.Parse()

	// Once we parse, then collect any additional leftover
	// command-line arguments
	cs.CmdLineArgs = flag.Args()

	// return the (pointer address to the) CmdLineService
	return &cs
}
