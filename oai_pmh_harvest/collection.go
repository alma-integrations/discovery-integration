package main

import "encoding/xml"

// Datafield represents a MARC (data) field.
type Datafield struct {
	Text      string     `xml:",chardata"`
	Tag       string     `xml:"tag,attr"`
	Ind1      string     `xml:"ind1,attr"`
	Ind2      string     `xml:"ind2,attr"`
	Subfields []Subfield `xml:"subfield"`
}

// Subfield represents a subfield located
// in a MARC (data)field
type Subfield struct {
	Text string `xml:",chardata"`
	Code string `xml:"code,attr"`
}

// Collection represents a "collection" of
// MARC records
type Collection struct {
	XMLName xml.Name `xml:"collection"`
	//Text    string   `xml:",chardata"`
	Xmlns  string       `xml:"xmlns,attr"`
	Record []MarcRecord `xml:"record"`
	/*
		Record []struct {
			//Text         string `xml:",chardata"`
			Header struct {
				Text       string `xml:",chardata"`
				Status     string `xml:"status,attr"`
				Identifier string `xml:"identifier"`
			} `xml:"header"`
			Leader       string `xml:"leader"`
			Controlfield []struct {
				Text string `xml:",chardata"`
				Tag  string `xml:"tag,attr"`
			} `xml:"controlfield"`
			Datafields []Datafield `xml:"datafield"`
		} `xml:"record"`
	*/
}

// functions that act on Collection here
