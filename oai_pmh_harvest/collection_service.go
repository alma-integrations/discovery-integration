package main

import (
	"fmt"
	"strings"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// MaxRecordsBatchCount -- how many MarcRecords are queued up
// before an API job is triggered
const MaxRecordsBatchCount = 50

const AVAILABLE_LABEL = "Available"
const UNAVAILABLE_LABEL = "Unavailable"
const CHECK_HOLDINGS_LABEL = "Check holdings"
const AVAILABLE_LIB_USE_ONLY = "Available - Library Use Only"

// CollectionService takes care of:
// - processing an SOA API response, ensuring availability is appropriately set,
// - writing "batched" MarcRecords to disk (via MarcRecordFileWriter)
type CollectionService struct {
	CurrentCollection Collection

	AlmaNumberMap map[string]*MarcRecord

	NewCollection Collection

	CurrentRecord *MarcRecord

	AvailLabel map[string]string
}

// NewCollectionService creates a new struct
func NewCollectionService(config *Config) *CollectionService {
	cs := &CollectionService{AlmaNumberMap: map[string]*MarcRecord{}}
	cs.AvailLabel = config.AvailabilityLabel

	// Create an empty collection that'll keep track of the valid MarcRecords
	cs.CurrentCollection = Collection{}

	journal.Info.Printf("Availability Labels:\n%+v\n", cs.AvailLabel)
	journal.Info.Println("NewCollectionService completed...")

	return cs
}

// ResetCollection ...
func (cs *CollectionService) ResetCollection() {
	cs.CurrentCollection = Collection{}
}

// SetCurrentRecord ...
func (cs *CollectionService) SetCurrentRecord(r *MarcRecord) {
	cs.CurrentRecord = r
}

// MarkCurrentRecordForBatch ...
func (cs *CollectionService) MarkCurrentRecordForBatch(almaNumber string) {
	cs.AlmaNumberMap[almaNumber] = cs.CurrentRecord
}

// AddCurrentRecordToCollection ...
func (cs *CollectionService) AddCurrentRecordToCollection() {
	cs.CurrentCollection.Record = append(cs.CurrentCollection.Record, *cs.CurrentRecord)
}

// ProcessMarcRecordForAlmaNumber receives an almaNumber, then
// locates the previously stored record (from AlmaNumberMap), and finally
// applies availability data.
func (cs *CollectionService) ProcessMarcRecordForAlmaNumber(almaNumber string, availItem SoaAvailabilityItem) {
	marcRecord := cs.AlmaNumberMap[almaNumber]

	availableLabel := CHECK_HOLDINGS_LABEL

	for i := range availItem.Availabilities {
		availableLabel = cs.AvailLabel[availItem.Availabilities[i].Status]

		// Add 852x subfields to each known holding
		marcRecord.AddHoldingAvailabilityLabel(availableLabel)
	}
}

// ResetAlmaNumberMap re-inits the map
func (cs *CollectionService) ResetAlmaNumberMap() {
	cs.AlmaNumberMap = make(map[string]*MarcRecord)
	journal.Debug.Println("collection service almaNumberMap reset...")
}

// BatchIsReady is true when len of AlmaNumberMap == MaxRecordsBatchCount
func (cs *CollectionService) BatchIsReady() bool {
	return len(cs.AlmaNumberMap) == MaxRecordsBatchCount
}

// BatchIsEmpty is true when len(cs.AlmaNumberMap) == 0
func (cs *CollectionService) BatchIsEmpty() bool {
	return len(cs.AlmaNumberMap) == 0
}

// APIQueryString returns the query suitable for the SOA call
func (cs *CollectionService) APIQueryString() string {
	params := []string{}
	journal.Debug.Printf("APIQueryString() / length of map is [%d]\n", len(cs.AlmaNumberMap))
	for k := range cs.AlmaNumberMap {
		params = append(params, fmt.Sprintf("s.id=01DUKE_INST_CAT%%20%s", k))
	}
	return "&" + strings.Join(params[:], "&")
}
