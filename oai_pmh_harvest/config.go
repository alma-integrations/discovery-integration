package main

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

// Config models our application's config file
type Config struct {
	// HarvestUrl -- the API we use to harvest OAI records
	HarvestUrl string `yaml:"oai_pmh_url"`

	// PMH - Protocol for Metadata Harvesting
	PMH struct {
		URLProtocol    string `yaml:"protocol"`
		Hostname       string `yaml:"hostname"`
		RequestPath    string `yaml:"request_path"`
		MetadataPrefix string `yaml:"metadata_prefix"`
		Dataset        string `yaml:"data_set"`
	} `yaml:"pmh"`

	// SoaUrl -- API used when querying for availability (single record use)
	SoaUrl string `yaml:"soa_url"`

	LogDir string `yaml:"log_dir"`

	// SoaURLPrefix -- API when using in 50-count batch mode
	SoaURLPrefix           string `yaml:"soa_url_prefix"`
	SoaAvailItemIDPrefix   string `yaml:"soa_avail_item_id_prefix"`
	DscCredentialsFile     string
	AwsTableName           string            `yaml:"aws_tablename"`
	DeletedFilenamePattern string            `yaml:"deleted_filename_pattern"`
	AvailabilityLabel      map[string]string `yaml:"availability"`

	// mailServiceOptions
	MailService struct {
		FromEmail     string   `yaml:"fromEmail"`
		FromEmailName string   `yaml:"fromEmailName"`
		ToEmail       []string `yaml:"toEmail"`
		SMTPHost      string   `yaml:"smtpHost"`
		SMTPPort      string   `yaml:"smtpPort"`
		EmailSubject  string   `yaml:"emailSubject"`
		EmailPW       string   `yaml:"password"`
		EmailBody     string   `yaml:"emailBody"`
	} `yaml:"mail_service"`

	// marcRecord settings
	MarcRecord struct {
		LibrarySubfield  string `yaml:"loc_b_subfield"`
		LocationSubfield string `yaml:"loc_n_subfield"`
	}
}

// NewConfig returns a new (application-specific) Config
func NewConfig(cs *CmdLineService) *Config {
	c := &Config{}
	c.parseConfigSources(cs.ConfigFilename)
	c.applyCommandLineFlags(cs)

	return c
}

// ParseConfigSource
func (c *Config) parseConfigSources(filename string) error {
	if len(filename) == 0 {
		return fmt.Errorf("%s was not specified", filename)
	}
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(c)
	if err != nil {
		return err
	}

	return nil
}

// use applyCommandLineFlags (or options) to back-fill
// any "config" settings specified at the command line
// (command line options have precedent over config file values)
func (c *Config) applyCommandLineFlags(cs *CmdLineService) {
	// may be implemented in the future
	c.DscCredentialsFile = cs.DSCIniFile
	if len(cs.AwsTableName) > 0 {
		c.AwsTableName = cs.AwsTableName
	}

	// PMH
	if len(cs.PMHHostname) > 0 {
		c.PMH.Hostname = cs.PMHHostname
	}
	if len(cs.PMHDataset) > 0 {
		c.PMH.Dataset = cs.PMHDataset
	}
	// if, for some crazy reason, someone REALLY wants to use
	// http instead of https...
	if len(cs.PMHUrlProtocol) > 0 {
		c.PMH.URLProtocol = cs.PMHUrlProtocol
	}
}

// String prints the representation of this config
func (c *Config) String() string {
	return fmt.Sprintf("%+v\n", c)
}
