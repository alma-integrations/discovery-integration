module gitlab.oit.duke.edu/alma-integrations/discovery-integration/oai_pmh_harvest/connectToDSC

go 1.21.3

require github.com/aws/aws-sdk-go v1.51.21

require github.com/jmespath/go-jmespath v0.4.0 // indirect
