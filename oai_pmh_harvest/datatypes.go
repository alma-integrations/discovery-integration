package main

import "encoding/xml"

type OAIResponse struct {
	XMLName xml.Name `xml:"OAI-PMH"`
	Records []Record `xml:"ListRecords>record"`
	Token   string   `xml:"ListRecords>resumptionToken"`
}

type Record struct {
	// XMLName xml.Name `xml:"record"` // Removed XMLName field
	Metadata struct {
		XMLName xml.Name     `xml:"metadata"`
		Record  NestedRecord `xml:"record"`
	} `xml:"metadata"`
	Header struct {
		Text       string `xml:",chardata"`
		Status     string `xml:"status,attr"`
		Identifier string `xml:"identifier"`
	} `xml:"header"`
}

type NestedRecord struct {
	XMLName xml.Name `xml:"record"`
	Data    string   `xml:",innerxml"`
}

type dataBase struct {
	XMLName   xml.Name    `xml:"OAI-PMH"`
	ID_string string      `xml:"ListRecords>record>header>identifier"`
	D_fields  []datafield `xml:"ListRecords>record>metadata>record>datafield"`
}

type datafield struct {
	Tag      string     `xml:"tag,attr"`
	Value    string     `xml:",chardata"`
	S_fields []subfield `xml:"subfield"`
}

type subfield struct {
	Tag   string `xml:"code,attr"`
	Value string `xml:",chardata"`
}

type LocCodes []struct {
	Locations []struct {
		Name string `json:"name"`
		Code string `json:"code"`
	} `json:"locations"`
	Name string `json:"name"`
	Code string `json:"code"`
}
