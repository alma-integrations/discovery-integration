package main

import (
	"fmt"
	"os"
	"time"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// DeletedFilesService handles adding BIB IDs to a file
type DeletedFilesService struct {
	OutFile *os.File
}

// NewDeletedFilesService creates a new one
func NewDeletedFilesService(cli *CmdLineService, c *Config) *DeletedFilesService {
	if cli.BypassEnrichment == true {
		journal.Info.Println("No need for 'deleted records' file tracker since we're bypassing enrichment")
		return nil
	}

	dfs := &DeletedFilesService{}

	formattedDatetime := time.Now().Format("200601021504")
	deletedFilePattern := "%s/delete_%s_%s.txt"

	deletedFileName := fmt.Sprintf(deletedFilePattern, cli.DeletesDir, cli.EnrichedDir, formattedDatetime)
	file, err := os.OpenFile(deletedFileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	journal.Info.Printf("[Deleted Files Service] opened %s for writing deleted BIBs\n", deletedFileName)
	dfs.OutFile = file
	return dfs
}
