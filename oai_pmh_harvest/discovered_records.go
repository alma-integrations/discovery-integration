package main

import (
	"fmt"
	"os"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// DiscoveredRecordsWriter writes a single XML to disk for a MarcRecord
type DiscoveredRecordsWriter struct {
	OutFile *os.File
}

// NewDiscoveredRecordsWriter accepts a CmdLineService instance
// so we can get the MarcRecordsDir setting
func NewDiscoveredRecordsWriter(parentMarcDirname string) *DiscoveredRecordsWriter {
	w := &DiscoveredRecordsWriter{}

	filename := fmt.Sprintf("%s/discovered.txt", parentMarcDirname)

	var openErr error
	w.OutFile, openErr = os.OpenFile(filename, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0644)
	if openErr != nil {
		journal.Error.Printf("Unable to open %s: %s\n", filename, openErr)
	}

	return w
}

// RecordEntry ...
func (w *DiscoveredRecordsWriter) RecordEntry(marcRecord *MarcRecord, sourceEnrichedFile string, enrichedDir string) {
	if w.OutFile == nil {
		return
	}

	lineToWrite := fmt.Sprintf("%s|%s|%s/%s|formatted-xml/%s\n",
		marcRecord.MmsID(),
		marcRecord.DukeBibID(),
		enrichedDir,
		sourceEnrichedFile,
		sourceEnrichedFile,
	)

	_, writeErr := w.OutFile.WriteString(lineToWrite)
	if writeErr != nil {
		journal.Error.Printf("Unable to write 'discovered' record line: %s\n", writeErr)
	}
}

// CloseFile ...
func (w *DiscoveredRecordsWriter) CloseFile() {
	w.OutFile.Close()
}
