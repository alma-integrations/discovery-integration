package main

import (
	"encoding/json"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	dsc_credentials "gitlab.oit.duke.edu/dul-go/dsc-credentials"
	journal "gitlab.oit.duke.edu/dul-go/journal"
)

type DataSvcCenter struct {
	dynamoDB  *dynamodb.DynamoDB
	tableName string
}

type DSC_Config struct {
	CredentialsFile string
}

// NewDataSvcCenter returns a representation of access to our "Data Service Center"
func NewDataSvcCenter(c *Config) (*DataSvcCenter, error) {
	dsc := &DataSvcCenter{}

	dsc.tableName = c.AwsTableName
	journal.Debug.Println(dsc.tableName)
	journal.Debug.Println(c.DscCredentialsFile)

	// read credentials from a file designated in our configuration
	dsc_creds, c_err := dsc_credentials.NewCredentials(c.DscCredentialsFile)
	if c_err != nil {
		journal.Error.Println(c_err)
		return nil, c_err
	}
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
		Credentials: credentials.NewStaticCredentials(dsc_creds.AccessKeyId,
			dsc_creds.SecretAccessKey, ""),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeInternalServerError:
				journal.Error.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
				return nil, aerr
			default:
				journal.Error.Println(aerr.Error())
				return nil, aerr
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			journal.Error.Println(err.Error())
			return nil, err
		}
	}

	dsc.dynamoDB = dynamodb.New(sess)
	journal.Info.Println("Connected to Data Service Center...")

	return dsc, nil
}

// GetAlmaNumber returns an Alma number mapped to the
// (provided) MMS number
func (dsc *DataSvcCenter) AlmaNumberFromMMS(mms string) (string, string, error) {
	result, err := dsc.dynamoDB.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(dsc.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"mms": {
				S: aws.String(mms),
			},
		},
	})
	if err != nil {
		journal.Error.Println("Got error calling (aws) GetItem:", err)
		return "", "", err
	}
	if result.Item == nil {
		journal.Error.Println("MMS", mms, "not found")
		return "", "", err
	}

	almaNumberAttr, ok := result.Item["almaNumber"]
	if !ok {
		journal.Debug.Println(mms, "almaNumber key not found in result.Item")
		for key := range result.Item {
			journal.Debug.Println("Key:", key)
		}
		// handle the case where "almaNumber" is not present in the map
		return "", "", nil
	}

	if almaNumberAttr.S == nil {
		journal.Debug.Println(mms, "almaNumber is not a string or is nil")
		return "", "", nil
	}
	almaNumber := *almaNumberAttr.S

	totalItemsAttr, ok := result.Item["totalItems"]
	if !ok {
		journal.Debug.Println("totalItems key not found in result.Item")
		for key := range result.Item {
			journal.Debug.Println("Key:", key)
		}
		// handle the case where "totalItems" is not present in the map
		return "", "0", nil
	}

	// At some point this was written as a string and then it was written as a number
	var totalItemsStr string
	if totalItemsAttr.S != nil {
		totalItemsStr = *totalItemsAttr.S
	} else if totalItemsAttr.N != nil {
		totalItemsStr = *totalItemsAttr.N
	} else {
		journal.Debug.Println("totalItems is neither a string nor a number")
		return "", "0", nil
	}

	return almaNumber, totalItemsStr, nil
}

// UpsertItem will either update or insert a new item into the DSC
func (dsc *DataSvcCenter) UpsertItem(jsonStr string) error {
	journal.Debug.Println("Upserting item " + jsonStr)

	var jsonMap map[string]interface{}
	err := json.Unmarshal([]byte(jsonStr), &jsonMap)
	if err != nil {
		journal.Error.Println("Error unmarshalling JSON:", err)
		return err
	}

	jsonMap["updatedate"] = time.Now().Format(time.RFC3339)

	av, err := dynamodbattribute.MarshalMap(jsonMap)
	if err != nil {
		journal.Error.Println("[dsc.UpsertItem] unable to marshal item:", err)
		return err
	}
	journal.Debug.Println("av:")
	for key, value := range av {
		journal.Debug.Printf("\t%s: %v\n", key, value)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(dsc.tableName),
	}

	_, err = dsc.dynamoDB.PutItem(input)
	if err != nil {
		journal.Error.Println("Got error calling PutItem:", err)
		return err
	}

	return nil
}
