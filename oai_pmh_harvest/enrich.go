package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// EnrichAvailability ...
func EnrichAvailability(cli *CmdLineService, cfg *Config, dsc *DataSvcCenter,
	jobTimer *JobTimer, delFilesSvc *DeletedFilesService, mailService *MailService) {
	if !cli.BypassEnrichment {
		jobTimer.Start()
		applyAvailabilityData(cli, cfg, dsc, delFilesSvc, mailService)
		jobTimer.End()

		// open our timestamp file
		timestampFile, fErr := os.OpenFile(cli.TimestampFilename, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0644)
		if fErr != nil {
			journal.Warning.Printf("Unable to open file for recording timer data: %s\n", fErr)
		} else {
			_, wErr := timestampFile.Write([]byte(fmt.Sprintf("%s", jobTimer)))
			if wErr != nil {
				journal.Warning.Printf("Unable to record time stats for the 'enrich' job")
			}
		}
	}
}

// applyAvailabilityData will fetch "availability" information
// (currently) from a "Summon over Alma" API.
func applyAvailabilityData(cli *CmdLineService, configData *Config, dsc *DataSvcCenter,
	delFilesSvc *DeletedFilesService, mailService *MailService) {
	var entries []string
	var badPattErr error

	defer delFilesSvc.OutFile.Close()

	if cli.EnrichAllFiles {
		entries, badPattErr = filepath.Glob(cli.HarvestDir + "/*." + cli.HarvestFileSuffix)
		if badPattErr != nil {
			journal.Error.Printf("%s", badPattErr)
		}
	} else {
		entries = append(entries, fmt.Sprintf("%s/%s", cli.HarvestDir, cli.HarvestFilename))
	}

	// load locations.json into LocCodes struct
	// this is used to figure out which availability goes
	// with which 852
	// from: https://requests.library.duke.edu/api/locationcodes
	locFile, err := os.Open(cli.LocFile)
	if err != nil {
		log.Fatal(err)
	}
	defer locFile.Close()

	locContent, locFileReadErr := io.ReadAll(locFile)
	if locFileReadErr != nil {
		// Handle error
		journal.Error.Printf("Unable to read locations from '%s'\n", cli.LocFile)
	}
	locations := LocCodes{}
	err = json.Unmarshal(locContent, &locations)
	if err != nil {
		log.Fatal(err)
	}

	// Record what "collection" file has a given MMS ID
	discoveredRecordsWriter := NewDiscoveredRecordsWriter(cli.MarcRecordsDir)

	// Record errors that LSIS is concerned about when processing a
	// MARC records
	errorsLogService := NewErrorsLogService(cli)

	// BIG 'OL PROCESSING LOOP
	// where all the magic takes place
	for _, oaiPmhRecordsFilename := range entries {
		xmlFile, err := os.Open(oaiPmhRecordsFilename)
		// check the "err" or use "xmlFile, _ := ..." if we're not expecting any errors
		if err != nil {
			journal.Error.Printf("Unable to open %s: %s\n", oaiPmhRecordsFilename, err)
			continue
		}
		defer xmlFile.Close()

		// Grab the tail of the harvest file to write the enrich file
		//enrichFileSuffix := ".xml"
		var enrichedFilename string
		fileWithoutPath := strings.Split(oaiPmhRecordsFilename, "/")
		if strings.Contains(fileWithoutPath[len(fileWithoutPath)-1], "-") {
			// As far as the resulting "enriched" file name
			// this line is all we care about.
			// Preserving the "timestamp" portion of the original file name
			// helps identify where a job stopped (b/c of an SoA API fail).
			enrichedFilename = strings.Replace(fileWithoutPath[len(fileWithoutPath)-1], "harvest", "enriched", 1)
		} else {
			//enrichFileSuffix = ".xml"
			enrichedFilename = "enriched.xml"
			journal.Debug.Println("Harvest file does not contain a \"-\" so defaulting to enrich.xml as enriched output")
		}
		journal.Debug.Printf("enrichedFile name: [%s]\n", enrichedFilename)

		processStarted := time.Now()

		// Reads the XML data from "xmlFile"
		bytes, err := io.ReadAll(xmlFile)
		if err != nil {
			journal.Error.Println("Error reading file: " + err.Error())
			continue
		}

		// Unmarshal (or decode) the string into a struct
		// we can use.
		var collection Collection
		xml.Unmarshal(bytes, &collection)

		collectionSvc := NewCollectionService(configData)
		var mmsID string
		recordCount := len(collection.Record)

		for ndx := range collection.Record {
			collection.Record[ndx].ScanRecordLeader()
			collection.Record[ndx].ScanControlfields()
			collection.Record[ndx].ScanDatafields()

			// get the MMS ID
			mmsID = collection.Record[ndx].MmsID()
			dukeBibID := collection.Record[ndx].DukeBibID()

			// Test for "Suppressed Record"
			if collection.Record[ndx].IsSuppressedRecord() {
				journal.Debug.Printf("[%d / %d] SUPPRESSED: Record %d / MMS ID: %s\n", ndx+1, recordCount, ndx+1, mmsID)

				// Make a note of it in our "deletedFiles" reporting
				if _, err = delFilesSvc.OutFile.WriteString(dukeBibID + "|SUPPRESSED\n"); err != nil {
					journal.Error.Printf("Error writing deleted bib to file: %s\n", err)
				}
				// continue to the next (Marc)Record
				continue
			}

			if collection.Record[ndx].IsDeletedRecord() {
				journal.Debug.Printf("[%d / %d] DELETED: Record %d / MMS ID: %s\n", ndx+1, recordCount, ndx+1, mmsID)

				// Make a note of it in our "deletedFiles" reporting
				if _, err = delFilesSvc.OutFile.WriteString(dukeBibID + "|DELETED\n"); err != nil {
					journal.Error.Printf("Error writing deleted bib to file: %s\n", err)
				}
				// continue to the next (Marc)Record
				continue
			}

			// This is the current record we're processing
			// basically, setting a marker at "this" record
			collectionSvc.SetCurrentRecord(&collection.Record[ndx])

			// If this record does not have physical copies, add it to the "output" <collection>
			// and then continue to the next record
			if !collection.Record[ndx].HasPhysicalCopies() {
				collectionSvc.AddCurrentRecordToCollection()
				continue
			}

			// HSE: Keeping this as a reference for the time being
			// if collection.Record[ndx].IsOnlineResource() {
			// 	collectionSvc.AddCurrentRecordToCollection()
			// 	continue
			// }

			// If this record does not have valid items, skip over to the next one
			if collection.Record[ndx].HasNoItems() {
				errorsLogService.ReportMissingItems(&collection.Record[ndx], enrichedFilename, cli.EnrichedDir)
				// continue to the next record
				continue
			}

			if collection.Record[ndx].HasValidMmsID() &&
				collection.Record[ndx].HasPhysicalCopies() &&
				!cli.DeleteOnly {

				// Query the DSC to get the AlmaNumber for this mmsID
				almaNumber, totalAvaItems, aErr := dsc.AlmaNumberFromMMS(mmsID)
				if aErr != nil {
					journal.Debug.Printf("ALMA number for MMS ID %s is not present: %s\n", mmsID, aErr)

					// make note of a missing ALMA number for this mmsID
					errorsLogService.ReportMissingAlmaNumber(mmsID, "")
					continue
				}

				// At this stage, all validations are completed, so we can safely
				// indicate we've "discovered" this record and the file where it was located.
				discoveredRecordsWriter.RecordEntry(&collection.Record[ndx], enrichedFilename, cli.EnrichedDir)

				// HSE -- this is were the "TotalItems" is added as a 941 field
				collection.Record[ndx].AddDatafieldWithSubfields("941", " ", " ", []MarcSubfield{{Code: "t", Text: totalAvaItems}})

				// This line adds the current <record> to the batch queue in CollectionService
				collectionSvc.MarkCurrentRecordForBatch(almaNumber)

				// When the batch is ready, call the API for getting "availabilites"
				if collectionSvc.BatchIsReady() {
					apiQueryString := collectionSvc.APIQueryString()
					journal.Info.Printf("[%d / %d] Calling API...\n", ndx+1, recordCount)

					// See: soa_request_service.go
					result, err := SoaAvailItemsResponse(configData.SoaURLPrefix, apiQueryString, cli)

					if err != nil {
						journal.Error.Printf("!!!!!!!!!!!!!!!!!!!! ===============> ERROR! %s\n", err)
						continue
					}

					// each avaItem will have an ID that contains an Alma number
					for _, avaItem := range result.AvailabilityItems {
						almaNumberKey := strings.TrimPrefix(avaItem.ID, configData.SoaAvailItemIDPrefix)
						collectionSvc.ProcessMarcRecordForAlmaNumber(almaNumberKey, avaItem)
					}

					collectionSvc.ResetAlmaNumberMap()
				}
			}

			// Push the current record to the "copied" collection
			// managed by collectionSvc.
			// -- this allows us to preserve the original, while
			// only keeping non-delete records in the copy.
			collectionSvc.AddCurrentRecordToCollection()
		}

		// Make sure the collectionService does one last API call if
		// alma numbers have been added since the last call, but
		// before we reach "MaxBatchCount"

		// Also keep in mind that the CollectionService will also make sure these
		// individual records are sent to disk.
		if !collectionSvc.BatchIsEmpty() {

			apiQueryString := collectionSvc.APIQueryString()

			// See: soa_request_service.go
			result, err := SoaAvailItemsResponse(configData.SoaURLPrefix, apiQueryString, cli)
			if err != nil {
				journal.Error.Printf("!!!!!!!!!!!!!!!!!!!! ===============> ERROR! %s\n", err)
				continue
			}
			for _, avaItem := range result.AvailabilityItems {
				almaNumberKey := strings.TrimPrefix(avaItem.ID, configData.SoaAvailItemIDPrefix)

				collectionSvc.ProcessMarcRecordForAlmaNumber(almaNumberKey, avaItem)
			}

			collectionSvc.ResetAlmaNumberMap()
		}

		var parentEnrichedDir string
		//var fileSlug string

		parentEnrichedDir = cli.EnrichedDir

		// If we're not processing a "deleted record", then save the enriched file
		// preserving the "timestamp portion of the original 'enriched' filename.
		//
		// With this technique, we'll be able to tell where a job stopped, in the event of
		// an error with the SoA API call.
		if cli.DeleteOnly == false {
			// Prove that the record count for the original collection is
			// different than that of the CollectionService's copy
			journal.Info.Printf("Records in original collection: %d | Records in collSvc copy: %d\n", len(collection.Record), len(collectionSvc.CurrentCollection.Record))

			//out, err := xml.Marshal(&collection)
			out, err := xml.Marshal(&collectionSvc.CurrentCollection)
			if err != nil {
				log.Fatal(err)
			}

			var formattedXMLFilename = enrichedFilename

			enrichedFilename = fmt.Sprintf("%s/%s", parentEnrichedDir, enrichedFilename)
			err = os.WriteFile(enrichedFilename, out, 0666)
			if err != nil {
				journal.Error.Printf("[soa] unable to write file - %s: %s", enrichedFilename, err)
				log.Fatal(err)
			}

			if len(cli.FormattedXMLDir) > 0 {
				formattedXML, marshalErr := xml.MarshalIndent(&collectionSvc.CurrentCollection, "", "  ")
				if marshalErr == nil {
					err = os.WriteFile(
						fmt.Sprintf("%s/%s", cli.FormattedXMLDir, formattedXMLFilename),
						formattedXML,
						0666,
					)
					if err != nil {
						journal.Error.Printf("Error writing 'formatted xml' file: %s\n", err)
					}
				} else {
					journal.Info.Printf("ERROR: %s\n", marshalErr)
				}
			}
		}

		processEnded := time.Now()
		processHowLong := processEnded.Sub(processStarted)

		var sb strings.Builder
		sb.WriteString(fmt.Sprintf("File: %s\n", oaiPmhRecordsFilename))
		sb.WriteString(fmt.Sprintf("%d|%s\n", processStarted.Unix(), processStarted.Format(time.UnixDate)))
		sb.WriteString(fmt.Sprintf("%d|%s\n", processEnded.Unix(), processEnded.Format(time.UnixDate)))
		sb.WriteString(fmt.Sprintf("%s\n", processHowLong.String()))

		// subject, message, toEmail, fromEmail
		// mailService.SendMail("asdf", "asdf", "dlc32@duke.edu", "hse9@duke.edu")

		// Write out a delete file for POD
		// format needs to be like delete_202406201500_31_mins.txt
		//formattedDatetime := processStarted.Format("200601021504")
		//deletedFileName := fmt.Sprintf("%s/deleted_%s_31_mins.txt", cli.PODDir, formattedDatetime)
		//writeListToFile(deletedRecords, deletedFileName)

		if cli.ProcessStatsDir != "" {
			procStatsFilename := fmt.Sprintf("%s/procstats-%s.txt",
				cli.ProcessStatsDir,
				strings.Replace(path.Base(oaiPmhRecordsFilename), ".xml", "", 1),
			)

			wErr := os.WriteFile(procStatsFilename, []byte(sb.String()), 0644)
			if wErr != nil {
				journal.Warning.Printf("Unable to record time stats for the 'enrich' job")
			}
		} else {
			journal.Info.Println(sb.String())
		}

		collectionSvc.ResetCollection()
	}
	discoveredRecordsWriter.CloseFile()
}
