package main

import (
	"fmt"
	"os"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// ErrorsLogService is tasked with writing error notices for the following:
// - missing Alma numbers
type ErrorsLogService struct {
	// File for reporting "missing alma number for a given MMS ID"
	fileMissingAlma *os.File

	// File for reporting "missing items"
	fileMissingItems *os.File
}

// NewErrorsLogService ...
func NewErrorsLogService(cli *CmdLineService) *ErrorsLogService {
	eService := &ErrorsLogService{}
	var openErr error

	eService.fileMissingAlma, openErr = os.OpenFile(cli.ErrorsDir+"/missing_alma_numbers.log",
		os.O_TRUNC|os.O_WRONLY|os.O_CREATE,
		0644,
	)
	if openErr != nil {
		journal.Error.Printf("Unable to open %s/missing_alma_numbers.log: %s\n", cli.ErrorsDir, openErr)
	}

	eService.fileMissingItems, openErr = os.OpenFile(cli.ErrorsDir+"/missing_items.log",
		os.O_TRUNC|os.O_WRONLY|os.O_CREATE,
		0644,
	)
	if openErr != nil {
		journal.Error.Printf("Unable to open %s/missing_items.log: %s\n", cli.ErrorsDir, openErr)
	}

	return eService
}

// ReportMissingAlmaNumber ...
func (el *ErrorsLogService) ReportMissingAlmaNumber(mmsID string, holdingsID string) {
	if el.fileMissingAlma == nil {
		return
	}

	_, writeErr := el.fileMissingAlma.WriteString(fmt.Sprintf("%s\n", mmsID))
	if writeErr != nil {
		journal.Error.Printf("Unable to file missing alma number report for %s: %s\n", writeErr)
	}
}

// ReportMissingItems ...
func (el *ErrorsLogService) ReportMissingItems(marcRecord *MarcRecord, sourceEnrichedFile string, enrichedDir string) {
	if el.fileMissingItems == nil {
		return
	}

	lineToWrite := fmt.Sprintf("%s|%s|%s/%s|formatted-xml/%s\n",
		marcRecord.MmsID(),
		marcRecord.DukeBibID(),
		enrichedDir,
		sourceEnrichedFile,
		sourceEnrichedFile,
	)

	_, writeErr := el.fileMissingItems.WriteString(lineToWrite)
	if writeErr != nil {
		journal.Error.Printf("Unable to file missing items report for %s: %s\n", writeErr)
	}
}
