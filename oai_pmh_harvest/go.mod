module gitlab.oit.duke.edu/alma-integrations/discovery-integration/oai_pmh_harvest

go 1.21.3

require (
	github.com/aws/aws-sdk-go v1.51.21
	gitlab.oit.duke.edu/dul-go/dsc-credentials v0.0.0-20240515092344-37a4de932bc3
	gitlab.oit.duke.edu/dul-go/journal v0.0.0-20190219194120-2a5b5450b878
	go.uber.org/dig v1.17.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
