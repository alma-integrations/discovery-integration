package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// HarvestRecords does the dirty work of harvesting OAI data.
func HarvestRecords(cli *CmdLineService, cfg *Config, dsc *DataSvcCenter, jobTimer *JobTimer) {
	if cli.BypassHarvest {
		return
	}
	lastResumptionToken := ""

	if cli.ResToken != "" {
		// caller likely used:
		// $ cat <file-with-token> | oai_pmh_harvest ...
		lastResumptionToken = cli.ResToken

	} else if cli.ResTokenInFile != "" {
		// read from file
		file, fErr := os.Open(cli.ResTokenInFile)
		if fErr != nil {
			journal.Warning.Printf("%s\n", fErr)
		} else {
			data := make([]byte, 1024)
			_, rErr := file.Read(data)
			if rErr != nil {
				journal.Warning.Printf("%s\n", rErr)
			} else {
				lastResumptionToken = strings.TrimSpace(string(data[:]))
			}
		}
	}

	// Run the OAI-PMH-HARVEST task, downloading files
	// harvested from Alma
	lastResumptionToken = harvestAlmaMetadata("ListRecords", cli, cfg, lastResumptionToken)

	// Save the last "resumption token" to disk if asked to
	if cli.ResTokenOutFile != "" {
		journal.Info.Printf("saving lastResumptionToken to %s\n", cli.ResTokenOutFile)
		// store the "harvest" token in a file location
		// designated by cli.ResumptionTokenFilename

		file, errs := os.Create(cli.ResTokenOutFile)
		if errs != nil {
			//journal.Error.Printf("Failed to create file: ", errs)
			journal.Error.Printf("%s\n", errs)
		}
		defer file.Close()

		_, errs = file.WriteString(lastResumptionToken)
		if errs != nil {
			journal.Error.Printf("Failed to write resumption token to file: ", errs)
		}
	}
	journal.Info.Printf("resumption token: %s\n", lastResumptionToken)
	fmt.Printf("%s\n", lastResumptionToken)

}

// harvestAlmaMetadata fetches catalog metadata from Alma
func harvestAlmaMetadata(verb string, cli *CmdLineService, configData *Config, recentToken string) string {
	// Start timer
	startTime := time.Now()

	// Open output file in the output folder
	outputFile, err := os.Create(cli.HarvestDir + "/" + cli.HarvestFilename)
	if err != nil {
		journal.Error.Println("Error creating output file:", err)
		return ""
	}
	defer outputFile.Close()

	// Write XML header to output file
	if _, err := outputFile.WriteString(xml.Header); err != nil {
		journal.Error.Println("Error writing XML header to output file:", err)
		return ""
	}

	// Write the opening <collection> tag with namespace
	if _, err := outputFile.WriteString(`<collection xmlns="http://www.loc.gov/MARC21/slim">` + "\n"); err != nil {
		journal.Error.Println("Error writing opening collection tag to output file:", err)
		return ""
	}

	// Initialize the resumption token
	//resumptionToken := ""
	resumptionToken := recentToken
	lastToken := resumptionToken

	// Loop until reaching the maximum number of pages
	for page := 1; page <= cli.MaxPages; page++ {
		// Make request with appropriate URL including resumption token

		// oai_pmh_url: https://duke.alma.exlibrisgroup.com/view/oai/01DUKE_INST/request
		//url := configData.HarvestUrl + "?verb=ListRecords"
		url := fmt.Sprintf("%s://%s/%s?verb=%s",
			configData.PMH.URLProtocol,
			configData.PMH.Hostname,
			configData.PMH.RequestPath,
			verb,
		)
		journal.Debug.Println(url)

		// Resumption Tokens include the query string portion needed to continue
		if resumptionToken != "" {
			url += "&resumptionToken=" + resumptionToken
		} else {
			// build the query string for the initial query...
			//url += "&set=trln_discovery_spec&metadataPrefix=marc21"
			//url += "&set=" + cli.OaiSet + "&metadataPrefix=marc21" + "&from=" + cli.FromDate
			url += "&set=" + configData.PMH.Dataset + "&metadataPrefix=" + configData.PMH.MetadataPrefix + "&from=" + cli.FromDate

		}
		journal.Debug.Println("Request URL:", url) // Log the request URL

		// Make HTTP request with retry logic
		response, err := makeHTTPRequestWithRetry(url, cli)
		if err != nil {
			journal.Error.Println("Error making HTTP request:", err)
			return ""
		}
		defer response.Body.Close()

		// Read response body
		body, err := io.ReadAll(response.Body)
		if err != nil {
			journal.Error.Println("Error reading response body:", err)
			return ""
		}

		// Parse XML response
		var oaiResponse OAIResponse
		if err := xml.Unmarshal(body, &oaiResponse); err != nil {
			journal.Error.Println("Error unmarshalling XML response:", err)
			return ""
		}

		// Update the resumption token for the next request
		resumptionToken = oaiResponse.Token

		// Save records to output file
		for _, record := range oaiResponse.Records {
			// Marshal the nested record
			recordHeader, err := xml.MarshalIndent(record.Header.Status, "", "  ")
			if err != nil {
				journal.Error.Println("Error marshalling record header:", err)
				return resumptionToken
			}

			recordXML, err := xml.MarshalIndent(record.Metadata.Record, "", "  ")
			if err != nil {
				journal.Error.Println("Error marshalling record:", err)
				return resumptionToken
			}
			recordHeaderWithXML := append(recordHeader[:], recordXML[:]...)

			// Write the record to the output file
			if _, err := outputFile.Write(recordHeaderWithXML); err != nil {
				journal.Error.Println("Error writing to output file:", err)
				return resumptionToken
			}

			// Add newline for readability
			if _, err := outputFile.WriteString("\n"); err != nil {
				journal.Error.Println("Error writing newline to output file:", err)
				return resumptionToken
			}
		}

		lastToken = resumptionToken

		journal.Info.Println("page", page, "completed.")

		// Check if there are no more resumption tokens
		if resumptionToken == "" {
			journal.Debug.Println("No more resumption tokens. Exiting the loop.")
			break
		}
	}

	// Write the closing </collection> tag
	if _, err := outputFile.WriteString("</collection>\n"); err != nil {
		journal.Error.Println("Error writing closing collection tag to output file:", err)
		return lastToken
	}

	journal.Info.Println("All pages completed.")

	// End timer
	endTime := time.Now()
	// Calculate and log execution time
	executionTime := endTime.Sub(startTime)
	journal.Info.Println("OAI-PMH Execution Time:", executionTime)

	return lastToken
}
