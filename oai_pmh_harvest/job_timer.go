package main

import (
	"fmt"
	"strings"
	"time"
)

// JobTimer simply keeps track of when the
// job started, ended -- and writing out the results
type JobTimer struct {
	Started time.Time
	Ended   time.Time
	HowLong time.Duration
}

// NewJobTimer returns a new JobTimer
func NewJobTimer() *JobTimer {
	return &JobTimer{}
}

// Start captures the job starting time
func (j *JobTimer) Start() {
	j.Started = time.Now()
}

// End captures the job ending time, and calculates the
// duration
func (j *JobTimer) End() {
	j.Ended = time.Now()
	j.HowLong = j.Ended.Sub(j.Started)
}

// String ... use a strings.Builder to create a string
// representing how long a job took, etc
func (j *JobTimer) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("%d|%s\n", j.Started.Unix(), j.Started.Format(time.UnixDate)))
	sb.WriteString(fmt.Sprintf("%d|%s\n", j.Ended.Unix(), j.Ended.Format(time.UnixDate)))
	sb.WriteString(fmt.Sprintf("%s\n", j.HowLong.String()))

	return sb.String()
}
