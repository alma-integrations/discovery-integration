package main

import (
	"encoding/json"
	"io"
	"log"
	"os"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// Location is a "broad location"
type Location struct {
	Name string `json:"name"`
	Code string `json:"code"`
}

// LocationCode is a (kinda) name:code mapping
type LocationCode struct {
	Locations []Location `json:"locations"`
	Name      string     `json:"name"`
	Code      string     `json:"code"`
}

// LocationCodeService manages location codes
type LocationCodeService struct {
	LocCodes []LocationCode
}

// NewLocationCodeService creates a new service to manage
// location codes, reading from "infile"
func NewLocationCodeService(cli *CmdLineService) *LocationCodeService {
	// load locations.json into LocCodes struct
	// this is used to figure out which availability goes
	// with which 852
	// from: https://requests.library.duke.edu/api/locationcodes
	locFile, err := os.Open(cli.LocFile)
	if err != nil {
		log.Fatal(err)
	}
	defer locFile.Close()

	locContent, locFileReadErr := io.ReadAll(locFile)
	if locFileReadErr != nil {
		// Handle error
		journal.Error.Printf("[NewLocationCodeService] Unable to read locations from '%s'\n", cli.LocFile)
	}
	locationCodes := []LocationCode{}
	err = json.Unmarshal(locContent, &locationCodes)
	if err != nil {
		log.Fatal(err)
	}

	return &LocationCodeService{LocCodes: locationCodes}
}
