package main

import (
	"fmt"
	"net/smtp"
	"strings"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

type MailService struct {
	Host          string
	Port          string
	Subject       string
	Message       string
	ToEmail       []string
	FromEmail     string
	FromEmailName string
}

// NewMailService ...
func NewMailService(cli *CmdLineService, c *Config) *MailService {
	ms := &MailService{}

	// smtp server configuration.
	ms.Host = c.MailService.SMTPHost
	ms.Port = c.MailService.SMTPPort
	ms.Subject = c.MailService.EmailSubject
	ms.Message = c.MailService.EmailBody
	ms.ToEmail = c.MailService.ToEmail
	ms.FromEmail = c.MailService.FromEmail
	ms.FromEmailName = c.MailService.FromEmailName

	return ms
}

// SendMail ...
func (ms *MailService) SendMail() {
	// This is set up to handle multiple emails
	toHeader := strings.Join(ms.ToEmail, ",")
	msg := "From: " + ms.FromEmail + "\n" +
		"To: " + toHeader + "\n" + // use toHeader
		"Subject: " + ms.Subject + "\n" +
		ms.Message

	err := smtp.SendMail(ms.Host+":"+ms.Port, nil, ms.FromEmail, ms.ToEmail, []byte(msg))
	if err != nil {
		fmt.Println(err)
		return
	}
	journal.Debug.Println("Email Sent to " + toHeader)
}
