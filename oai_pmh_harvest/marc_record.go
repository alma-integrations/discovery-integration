package main

import (
	"bytes"
	"encoding/xml"
	"regexp"
	"strings"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// CFieldTagMmsID - the tag that contains the MMS ID
const CFieldTagMmsID = "001"

// Datafield tag constants

// DFieldTagRelatedRec -- tag for related record field
const DFieldTagRelatedRec = "773"

// DFieldTagCarrierType -- tag for determining online resource
const DFieldTagCarrierType = "338"

// DFieldTagPhysicalItem = "940"
const DFieldTagPhysicalItem = "940"

// DFieldTagElecInventory = "943"
const DFieldTagElecInventory = "943"

// DFieldTagHolding = 852
const DFieldTagHolding = "852"

// Subfield constants

// SFieldTagOnlineRelType for relator type indicating "online"
const SFieldTagOnlineRelType = "a"

// Other Duke/Alma specific constants

// AlephBornMmsIDSuffix for Aleph-born recs
const AlephBornMmsIDSuffix = "0108501"

// AlmaRecPrefix -- Alma record prefix (for all records)
const AlmaRecPrefix = "99"

// LEADER field constants

// LEADER_RECSTATUS ...
const LEADER_RECSTATUS = 5

// LEADER_ENCODINGLVL
// Awaiting feedback
// const LEADER_ENCODINGLVL = 17

// MarcDatafield presents <datafield tag="xxx">...</datafield>
type MarcDatafield struct {
	XMLName   xml.Name       `xml:"datafield"`
	Text      string         `xml:",chardata"`
	Tag       string         `xml:"tag,attr"`
	Ind1      string         `xml:"ind1,attr"`
	Ind2      string         `xml:"ind2,attr"`
	Subfields []MarcSubfield `xml:"subfield"`

	// When Tag is 852, and 'b' is SCL or ARCH
	isRubensteinHolding bool
}

// IsRubensteinHolding tells us if this holding belongs to
// Rubenstein Library (SCL or ARCH)
func (d *MarcDatafield) IsRubensteinHolding() bool {
	return d.isRubensteinHolding && d.Tag == DFieldTagHolding
}

// SetIsRubensteinHolding bool
func (d *MarcDatafield) SetIsRubensteinHolding(b bool) {
	d.isRubensteinHolding = b
}

// AddSubfieldWithValue adds a new MarcSubfield
// with code and text
func (d *MarcDatafield) AddSubfieldWithValue(code string, text string) {
	subfield := MarcSubfield{Code: code, Text: text}
	d.Subfields = append(d.Subfields, subfield)
}

// MarcSubfield represents <datafield ...><subfield code="x"></subfield>>/datafield>
type MarcSubfield struct {
	XMLName xml.Name `xml:"subfield"`
	Text    string   `xml:",chardata"`
	Code    string   `xml:"code,attr"`
}

// MarcRecord represents a MARC data stream (or struct, or...)
type MarcRecord struct {
	//Text         string `xml:",chardata"`
	XMLName xml.Name `xml:"record"`
	Header  struct {
		Text       string `xml:",chardata"`
		Status     string `xml:"status,attr"`
		Identifier string `xml:"identifier"`
	} `xml:"header"`
	Leader       string `xml:"leader"`
	Controlfield []struct {
		Text string `xml:",chardata"`
		Tag  string `xml:"tag,attr"`
	} `xml:"controlfield"`
	Datafields []MarcDatafield `xml:"datafield"`

	mmsID                string
	isOnlineResource     bool
	hasPhysicalCopies    bool
	hasOnlineCopies      bool
	hasRubensteinHolding bool

	// Holdings is an array of pointers to MarcDatafields with tag=852
	holdings []*MarcDatafield

	// Items in an array of pointers to MarcDatafields with tag=940
	physicalItems []*MarcDatafield

	electronicItems []*MarcDatafield
}

// HasRubensteinHolding ...
// SurveyDatafields() will determine if this record
// has one or more 852b that are ARCH or SCL
func (r *MarcRecord) HasRubensteinHolding() bool {
	return r.hasRubensteinHolding
}

// AddHoldingAvailabilityLabel - add an "availability label" to the
// list of holdings:
// change label to 'Available - Library Use Only' when a holding
// belongs to Rubenstein Library
func (r *MarcRecord) AddHoldingAvailabilityLabel(label string) {
	if r.holdings == nil {
		journal.Warning.Printf("Avaiability Items exists, but record (%s) has no 852 fields...\n", r.MmsID())
		return
	}
	for i := range r.holdings {
		if r.holdings[i].IsRubensteinHolding() {
			// THIS REALLY, REALLY SHOULD BE STRING LOCATED IN A CONFIG!!!!!!!!!
			label = "Available - Library Use Only"
		}
		r.holdings[i].AddSubfieldWithValue("x", label)
	}
}

// MmsID return the MMS ID for a record
func (r *MarcRecord) MmsID() string {
	return r.mmsID
}

// AddDatafieldWithSubfields adds a new Datafield for this "tag", and
// appends 'subfields'
func (r *MarcRecord) AddDatafieldWithSubfields(tag string, ind1 string, ind2 string, subfields []MarcSubfield) {
	datafield := MarcDatafield{Tag: tag, Ind1: ind1, Ind2: ind2, Subfields: subfields}
	r.Datafields = append(r.Datafields, datafield)
}

// DukeBibID returns a Bib ID that's aware of Aleph-born or Alma-born
// MMS ID conventions
//
// Note: copies HSE9's code from oai_pmh_harvest
func (r *MarcRecord) DukeBibID() string {
	dukeBibID := r.mmsID

	if len(r.mmsID) == 18 {
		if bytes.HasSuffix([]byte(r.mmsID), []byte("0108501")) {
			dukeBibID = strings.TrimSuffix(r.mmsID, "0108501")
			dukeBibID = strings.TrimPrefix(dukeBibID, "99")
		} else {
			dukeBibID = r.mmsID
		}
	} else {
		dukeBibID = r.mmsID
	}

	return "DUKE" + dukeBibID
}

// HasValidMmsID is true when the MMS ID is not empty
func (r *MarcRecord) HasValidMmsID() bool {
	return len(r.mmsID) > 0
}

// HasValidItems returns true when a record has at least
// one physical item (940) or one electronic item (943)
func (r *MarcRecord) HasValidItems() bool {
	// after scanning the datafields looking for 940s, 943s
	// determine if this record is valid or not
	return len(r.electronicItems) > 0 || len(r.physicalItems) > 0
}

// HasNoItems returns true when a record has at least
// one physical item (940) or one electronic item (943)
func (r *MarcRecord) HasNoItems() bool {
	// after scanning the datafields looking for 940s, 943s
	// determine if this record is valid or not
	return len(r.electronicItems) == 0 && len(r.physicalItems) == 0
}

// IsSuppressedRecord checks the Leader string, position 5
// copied from HSE9's code in oai_pmh_harvest.go
func (r *MarcRecord) IsSuppressedRecord() bool {
	return string(r.Leader[LEADER_RECSTATUS]) == "d"
}

// IsDeletedRecord checks the Header.Status string and
// returns true if it's "deleted"
func (r *MarcRecord) IsDeletedRecord() bool {
	return r.Header.Status == "deleted"
}

// IsBriefRecord checks the Leader string, position 5
// copied from dlc32's code IsDeletedRecord()
// func (r *MarcRecord) IsBriefRecord() bool {
// 	return string(r.Leader[LEADER_ENCODINGLVL]) == "3"
// }

// HasPhysicalCopies is true when the record has at least
// one(1) 940 datafield
func (r *MarcRecord) HasPhysicalCopies() bool {
	return r.hasPhysicalCopies
}

// IsOnlineResource is true when the record has at least
// one(1) 338 datafield
func (r *MarcRecord) IsOnlineResource() bool {
	return r.isOnlineResource
}

// ScanDatafields is our (custom) opporutnity to look at the
// record's fields and set booleans like "isOnline", "hasPhysicalItems", etc
// Uses HSE9's code from oai_pmh_harvest.go
//
// Note:
// Using XPath would be better since we wouldn't have to iterate through all the
// datafields.
func (r *MarcRecord) ScanDatafields() {
	r.isOnlineResource = false
	r.hasPhysicalCopies = false
	r.hasOnlineCopies = false

	re := regexp.MustCompile(`(ARCH|SCL)`)

	for i := range r.Datafields {

		// determine "is online" status
		if r.Datafields[i].Tag == DFieldTagCarrierType {
			for j := range r.Datafields[i].Subfields {
				// Match and set boolean and then skip if we find "online resource"
				if r.Datafields[i].Subfields[j].Code == SFieldTagOnlineRelType && r.Datafields[i].Subfields[j].Text == "online resource" {
					r.isOnlineResource = true
				}
			}
		}

		// determine physical item status
		if r.Datafields[i].Tag == DFieldTagPhysicalItem {
			r.hasPhysicalCopies = true
			r.physicalItems = append(r.physicalItems, &r.Datafields[i])
		}

		// add this field to electronicInventory when appropriate
		if r.Datafields[i].Tag == DFieldTagElecInventory {
			r.hasOnlineCopies = true
			r.electronicItems = append(r.electronicItems, &r.Datafields[i])
		}

		// determine (suspected) Rubenstein status
		// OR, bigger picture (some day) -- determine if a record needs
		// Item-level availability awareness
		if r.Datafields[i].Tag == DFieldTagHolding {
			// this line allow us to keep track of the 852 fields
			// the (new) CollectionService type will use this late
			r.holdings = append(r.holdings, &r.Datafields[i])

			// Now determine if we have a Rubenstein holding.
			for j := range r.Datafields[i].Subfields {
				if r.Datafields[i].Subfields[j].Code == "b" && re.MatchString(strings.TrimSpace(r.Datafields[i].Subfields[j].Text)) {
					r.Datafields[i].SetIsRubensteinHolding(true)

					r.hasRubensteinHolding = true
				}
			}
		}
	}
}

// ScanControlfields allows us to loop (once) over the control fields
func (r *MarcRecord) ScanControlfields() {
	for i := range r.Controlfield {
		if r.Controlfield[i].Tag == CFieldTagMmsID {
			r.mmsID = r.Controlfield[i].Text
		}
	}
}

// ScanLeaderField ...
func (r *MarcRecord) ScanRecordLeader() {
}
