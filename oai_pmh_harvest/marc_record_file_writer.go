package main

import (
	"encoding/xml"
	"fmt"
	"os"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// MarcRecordFileWriter writes a single XML to disk for a MarcRecord
type MarcRecordFileWriter struct {
	recordsDirectory string
}

// NewMarcRecordFileWriter accepts a CmdLineService instance
// so we can get the MarcRecordsDir setting
func NewMarcRecordFileWriter(cli *CmdLineService) *MarcRecordFileWriter {
	w := &MarcRecordFileWriter{recordsDirectory: cli.MarcRecordsDir}
	return w
}

// WriteMarcRecord ...
func (w *MarcRecordFileWriter) WriteMarcRecord(marcRecord *MarcRecord) error {
	filename := fmt.Sprintf("%s/%s.xml", w.recordsDirectory, marcRecord.DukeBibID())

	collection := Collection{}
	collection.Record = append(collection.Record, *marcRecord)

	xmlDoc := []byte(xml.Header)

	xmlOut, err := xml.MarshalIndent(&collection, "", "  ")
	if err != nil {
		journal.Error.Printf("[WriteMarcRecord] error marshaling XML: %s\n", err)
		return err
	}

	xmlDoc = append(xmlDoc, xmlOut...)
	err = os.WriteFile(filename, xmlDoc, 0666)
	if err != nil {
		journal.Error.Printf("[WriteMarcRecord] unable to write to %s: %s\n", filename, err)
		return err
	}
	return nil
}
