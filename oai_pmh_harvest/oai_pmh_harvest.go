package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	journal "gitlab.oit.duke.edu/dul-go/journal"
	"go.uber.org/dig"
)

// BuildContainer uses "Dependency Injections" to build
// our known dependencies
//
// Source tips (on dependency injection):
// - https://blog.drewolson.org/dependency-injection-in-go
// - https://github.com/uber-go/dig
func BuildContainer(container *dig.Container) {
	container.Provide(NewCmdLineService)
	container.Provide(NewConfig)
	container.Provide(NewMarcRecordFileWriter)
	container.Provide(NewDeletedFilesService)
	container.Provide(NewLocationCodeService)
	container.Provide(NewJobTimer)
	container.Provide(NewDataSvcCenter)
	container.Provide(NewMailService)
	journal.Info.Println("all dependencies are built...")
}

func main() {
	// Intialize logging settings
	journal.InitLogging(io.Discard, // DEBUG
		os.Stdout, // INFO
		os.Stdout, // WARNING
		os.Stdout, // ERROR
	)

	container := dig.New()
	BuildContainer(container)

	// This will return an AlmaNumber for an MMS ID and then exit
	err := container.Invoke(func(cli *CmdLineService, cfg *Config, dsc *DataSvcCenter) {
		if len(cli.CmdLineArgs) > 0 {
			command := cli.CmdLineArgs[0]
			if command == "almanumber" {
				mmsID := cli.CmdLineArgs[1]
				// fetch the alma number for the given MMS ID
				almaNumber, _, aErr := dsc.AlmaNumberFromMMS(mmsID)
				if aErr != nil {
					journal.Error.Printf("Error retrieving ALMA number for MMS %s: %s\n", mmsID, aErr)
				}
				fmt.Printf("[%s] Alma Number: %s\n", mmsID, almaNumber)
				os.Exit(0)
			}
		}
	})

	err = container.Invoke(HarvestRecords)
	if err != nil {
		journal.Error.Printf("[Invoke HarvestRecords] Error with harvesting: %s\n", err)
	}

	err = container.Invoke(EnrichAvailability)
	if err != nil {
		journal.Error.Printf("[Invoke EnrichAvailability] Error with enriching availability")
	}

	os.Exit(0)
}

// makeHTTPRequestWithRetry makes an HTTP request with retry logic
func makeHTTPRequestWithRetry(url string, cli *CmdLineService) (*http.Response, error) {
	client := &http.Client{}
	var resp *http.Response
	var err error
	journal.Info.Printf("HTTP Request for: %s\n", url)
	for attempt := 1; attempt <= cli.MaxRetries; attempt++ {
		resp, err = client.Get(url)
		if err != nil {
			journal.Error.Printf("Attempt %d failed: %v\n", attempt, err)
			time.Sleep(time.Duration(cli.RetryInterval) * time.Second) // Wait before retrying
			continue
		}
		if resp.StatusCode != http.StatusOK {
			journal.Warning.Printf("Attempt %d returned non-OK status: %d\n", attempt, resp.StatusCode)
			resp.Body.Close()                                          // Explicitly close the response body when the status code is not OK
			time.Sleep(time.Duration(cli.RetryInterval) * time.Second) // Wait before retrying
			continue
		}
		// Response is (supposedly) OK
		// Now we need to verify the content

		// Successful response, the caller needs to close the body
		return resp, nil
	}
	return nil, fmt.Errorf("maximum number of retries exceeded")
}
