package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	journal "gitlab.oit.duke.edu/dul-go/journal"
)

// MakeSoaRequest ...
func MakeSoaRequest(url string, cli *CmdLineService) (*http.Response, error) {
	client := &http.Client{}
	for attempt := 1; attempt <= cli.MaxRetries; attempt++ {
		journal.Info.Printf("[MakeSoaRequest] attempt %d\n", attempt)
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			journal.Error.Printf("Attempt %d failed: %v\n", attempt, err)
			time.Sleep(time.Duration(cli.RetryInterval) * time.Second) // Wait before retrying
			continue
		}
		req.Header.Set("Accept", "application/json")
		resp, err := client.Do(req)
		if err != nil {
			fmt.Printf("Error in JSON: %s", err)
			// really need to return nil, err here
			return nil, err
		}
		if resp.StatusCode != http.StatusOK {
			journal.Error.Printf("Attempt %d returned non-OK status: %d\n", attempt, resp.StatusCode)
			resp.Body.Close()                                          // Explicitly close the response body when the status code is not OK
			time.Sleep(time.Duration(cli.RetryInterval) * time.Second) // Wait before retrying
			continue
		}
		// Successful response, the caller needs to close the body
		journal.Info.Println("[MakeSoaRequest] done")
		return resp, nil
	}
	return nil, fmt.Errorf("maximum number of retries exceeded")
}

// SoaAvailItemsResponse ...
func SoaAvailItemsResponse(urlPrefix string, queryStr string, cli *CmdLineService) (*SoaResponse, error) {
	url := urlPrefix + queryStr
	response, err := MakeSoaRequest(url, cli)
	if err != nil {
		journal.Error.Println("Error making HTTP request:", err)
		return nil, err
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)

	var result SoaResponse
	// sometimes the response is wrapped in a callback
	adjustedBody := string(body[:])
	removeEnd := strings.TrimSuffix(adjustedBody, ")")
	removeFront := strings.TrimPrefix(removeEnd, "callback(")
	if err := json.Unmarshal([]byte(removeFront), &result); err != nil {
		journal.Error.Println("Can not unmarshal JSON")
		return nil, err
	}
	return &result, nil
}
