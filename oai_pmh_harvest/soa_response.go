package main

import "fmt"

// SoaAvailabilityItem ...
type SoaAvailabilityItem struct {
	ID             string            `json:"id"`
	Availabilities []SoaAvailability `json:"availabilities"`
}

// SoaAvailability ...
type SoaAvailability struct {
	DisplayString  string `json:"displayString"`
	Status         string `json:"status"`
	StatusMessage  string `json:"statusMessage"`
	Location       string `json:"location"`
	CallNumber     string `json:"callNumber"`
	LocationString string `json:"locationString"`
	IsSuppressed   bool   `json:"isSuppressed"`
}

// String -- represent the SoaAvailability in string format
func (s SoaAvailability) String() string {
	return fmt.Sprintf("DisplayString: %s | Status: %s | StatusMessage: [%s] | Location: %s | LocationString: [%s] | Call Number: %s | IsSuppressed: %t\n",
		s.DisplayString,
		s.Status,
		s.StatusMessage,
		s.Location,
		s.LocationString,
		s.CallNumber,
		s.IsSuppressed,
	)
}

type SoaResponse struct {
	Version           string                `json:"version"`
	SessionID         string                `json:"sessionId"`
	QueryTime         int                   `json:"queryTime"`
	TotalRequestTime  int                   `json:"totalRequestTime"`
	AvailabilityItems []SoaAvailabilityItem `json:"availabilityItems"`
	/*
		AvailabilityItems []struct {
			ID             string `json:"id"`
			Availabilities []struct {
				DisplayString  string `json:"displayString"`
				Status         string `json:"status"`
				StatusMessage  string `json:"statusMessage"`
				Location       string `json:"location"`
				CallNumber     string `json:"callNumber"`
				LocationString string `json:"locationString"`
				IsSuppressed   bool   `json:"isSuppressed"`
			} `json:"availabilities"`
		} `json:"availabilityItems"`
	*/
	ClientID string `json:"clientId"`
}
