#!/bin/bash

# print-ava-composse.sh [options] START LAST [lib-datastore-path]

set -e

# Get the basename for logging purposes
filename=$(basename "$0")

# Process flags first (getopt)
OPTS=`getopt -o l:c:j -l loader-vm:,compose-filename:,print-json -n '${filename}' -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

LOADER_VM=""
COMPOSE_FILENAME="docker-compose.yml"
PRINT_JSON_OPT=''
DESTINATION_DIR="/opt/docker-compose/projects/primo"

while true; do
  case "$1" in
    -l | --loader-vm ) LOADER_VM="$2"; shift 2 ;;
    -c | --compose-filename ) COMPOSE_FILENAME="$2"; shift 2;;
    -j | --print-json ) PRINT_JSON_OPT="--print-json" ; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

START=${1:-1}
LAST=${2:-592}
ALMA_PIPELINE_SHARE=${3:-/share/alma-pipeline}

echo "---" > compose.yml
printf "services:\n" >> compose.yml

for (( i=$START; i<=$LAST; i++ ))
do
  # make a unique timestamp string
  ts=$(date '+%s')

  printf "  primo-${i}:\n" >> compose.yml
  printf "    container_name: primo-${i}\n" >> compose.yml
  printf "    image: alma-primo\n" >> compose.yml
  printf "    volumes:\n" >> compose.yml
  printf "      - '${ALMA_PIPELINE_SHARE}/primo/dir_${i}:/var/data/primo/dir_${i}:rw'\n" >> compose.yml
  printf "      - '${ALMA_PIPELINE_SHARE}/ava/stats:/var/data/stats:rw,z'\n" >> compose.yml
  printf "      - '${ALMA_PIPELINE_SHARE}/ava/json:/var/data/json:rw,z'\n" >> compose.yml
  printf "    command: >\n" >> compose.yml
  printf "      availability -delete-files=true -delete-tars=true -dsc-ini /usr/local/etc/aws_credentials -tar-dir /var/data/primo/dir_${i} ${PRINT_JSON_OPT} --stampfile /var/data/stats/avail-run-${i}.txt --json-out /var/data/json/ava-batch-${i}-${ts}.json\n" >> compose.yml
  printf "\n" >> compose.yml
done

if [[ -z "$LOADER_VM" ]] ; then
  cat compose.yml
else
  rsync -avp compose.yml ${LOADER_VM}.lib.duke.edu:${DESTINATION_DIR}/${COMPOSE_FILENAME}
fi
rm compose.yml
