#!/bin/bash

# print-enrich-compose.sh [option] SOA_FOLDER BOX1[, BOX2, ....]
# 
# [options]
# -l, --loader-vm: server (VM) name (but don't include "lib(brary).duke.edu"
# -c, --compose-filename: defaults to 'docker-compose-enrich.yml
# -p, --alma-pipeline-dir: base directory for alma pipeline (share dir)

set -e

# Get the basename for logging purposes
filename=$(basename "$0")

# Process flags first (getopt)
OPTS=`getopt -o l:c:p: -l loader-vm:,compose-filename:,alma-pipeline-dir: -n '${filename}' -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

LOADER_VM=""
COMPOSE_FILENAME="docker-compose-enrich.yml"

# by default, and as these files are originally intended to
# run on the "lsis-loader" VMs, 
ALMA_PIPELINE_DIR=/share/alma-pipeline

DEV_RUN=""

while true; do
  case "$1" in
    -l | --loader-vm ) LOADER_VM="$2"; shift 2 ;;
    -c | --compose-filename ) COMPOSE_FILENAME="$2"; shift 2 ;;
    -p | --alma-pipeline-dir ) ALMA_PIPELINE_DIR="$2"; shift 2 ;;
    -d | --dev-run ) DEV_RUN=true; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [[ $# -lt 2 ]]; then
  echo "$filename: requires an <SOA_FOLDER>, and at least 1 'box' (folder) to process"
  exit 1
fi

SOA_FOLDER=$1
shift

#LAST=$2
#SOA_FOLDER=$3

echo "---" > tmp_compose.yml
printf "services:\n" >> tmp_compose.yml
#for (( i=$START; i<=$LAST; i++ ))
#do
for i in "${@}"; do
  # remove any characters from $i
  i=${i//[[:alpha:]]/}

  printf "  enrich-${i}:\n" >> tmp_compose.yml
  printf "    container_name: enrich-${i}\n" >> tmp_compose.yml
  printf "    image: alma-primo\n" >> tmp_compose.yml
  printf "    volumes:\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/harvest${i}:/app/harvest${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/enrich${i}:/app/enrich${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/procStats${i}:/app/procStats${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/marc${i}:/app/marc${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/stats${i}:/app/stats${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/errors${i}:/app/errors${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/deletes${i}:/app/deletes${i}:rw'\n" >> tmp_compose.yml
  printf "      - '${ALMA_PIPELINE_DIR}/soa-enrich/${SOA_FOLDER}/formatted${i}:/app/formatted${i}:rw'\n" >> tmp_compose.yml
  printf "    command:\n" >> tmp_compose.yml
  printf "      enrich ${i}\n" >> tmp_compose.yml
  # printf "    entrypoint: [\"oai_pmh_harvest\",\"-skip-harvest\",\"-enrich-all=true\",\"-harvest-dir=harvest${i}\",\"-enriched-dir=enrich${i}\",\"-dsc-ini=/usr/local/etc/aws_credentials\",\"--proc-stats-dir=procStats${i}\",\"-stampfile=enrich-out-${i}.txt\",\"--pod-dir=pod${i}\"]\n" >> tmp_compose.yml
  printf "\n" >> tmp_compose.yml
done

if [[ -z "$LOADER_VM" ]] ; then
  cat tmp_compose.yml
  rm tmp_compose.yml
else
  rsync -avp tmp_compose.yml ${LOADER_VM}.lib.duke.edu:/srv/discovery-integration/${COMPOSE_FILENAME}
fi
