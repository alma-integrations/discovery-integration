#!/bin/bash

set -e
# Get the basename for logging purposes
filename=$(basename "$0")

# Process flags first (getopt)
OPTS=`getopt -o D:M:o:t:NC:O: -l alma-pipeline-dir:,dir-create-mode:,output-file:,timestamp:,no-create-dir,changes-from:,oai-set: -n '${filename}' -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi
eval set -- "$OPTS"

ALMA_PIPELINE_SHARE="/share/alma-pipeline"
DIR_CREATE_MODE="0777"
OUTPUT_FILE=""
NO_CREATE_DIR=false
TIMESTAMP_DIRNAME=$(date +%Y-%m-%d-%H-%00-00)
CHANGES_FROM=""

while true; do
  case "$1" in
    -D | --alma-pipeline-dir ) ALMA_PIPELINE_SHARE="$2"; shift 2 ;;
    -M | --dir-create-mode ) DIR_CREATE_MODE="$2"; shift 2 ;;
    -o | --output-file ) OUTPUT_FILE="$2"; shift 2 ;;
    -N | --no-create-dir ) NO_CREATE_DIR=true; shift ;; 
    -t | --timestamp ) TIMESTAMP_DIRNAME="$2"; shift 2 ;;
    -C | --changes-from ) CHANGES_FROM="$2"; shift 2 ;;
    -O | --oai-set ) OAI_SET="$2"; shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

# TIMESTAMP_DIRNAME=${1:-$(date -d @${NOW_EPOCH} +"%Y-%m-%d-%H-00-00")}

# Verify that CHANGES_FROM matches the format required by Ex Libris' API 
# for doing the record harvesting.
# Exit with an error when an incorrect format is detected
if [[ -n "$CHANGES_FROM" ]]; then
  if [[ ! $CHANGES_FROM =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$ ]]; then
    echo "CHANGES_FROM timestring is not formatted correctly (yyyy-mm-ddTHH:MM:SSZ)"
    exit 1
  fi
fi
# DEV/DevOps NOTE:
# the 'gitlab-runner' user has full SUDO rights on 'lsis-ci-01.lib.duke.edu'

# The goal of this script is two-fold
# -----
# 1) create "timestamped" token and harvest folders under "alma/pipeline/harvest"
# 2) write out a 'docker-compose' file that will mount the folders appropriately

# to run this:
# docker compose -f docker-compose-harvest.yml up [--detach]

TIMESTAMP_UNIX=$(date +%s)

# TIMESTAMP=${TIMESTAMP//:/\\:}

echo "---" > tmp_compose.yml
echo "services:" >> tmp_compose.yml
echo "  harvest-ci:" >> tmp_compose.yml
echo "    container_name: harvest-ci" >> tmp_compose.yml
echo "    image: alma-primo" >> tmp_compose.yml
echo "    build:" >> tmp_compose.yml
echo "      context: ." >> tmp_compose.yml
echo "    volumes:" >> tmp_compose.yml
echo "      - '$ALMA_PIPELINE_SHARE/tokens:/app/tokens:rw'" >> tmp_compose.yml
echo "      - '$ALMA_PIPELINE_SHARE/timestamps:/app/timestamps:rw'" >> tmp_compose.yml
echo "      - '$ALMA_PIPELINE_SHARE/harvest/$TIMESTAMP_DIRNAME:/app/harvest:rw'" >> tmp_compose.yml
echo "      - '$ALMA_PIPELINE_SHARE/logs:/app/logs:rw'" >> tmp_compose.yml
echo "    environment:" >> tmp_compose.yml
echo "      - CHANGES_FROM=$CHANGES_FROM" >> tmp_compose.yml
echo "      - OAI_SET=$OAI_SET" >> tmp_compose.yml
echo "    # Remember, this command can be found in the 'entrypoint.sh' file" >> tmp_compose.yml
echo "    command:" >> tmp_compose.yml
echo "      - harvest" >> tmp_compose.yml

if [[ -z "$OUTPUT_FILE" ]] ; then
  cat tmp_compose.yml
  rm tmp_compose.yml
else
  mv tmp_compose.yml $OUTPUT_FILE
fi

echo "[$filename] done." >&2
