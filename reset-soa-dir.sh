#!/bin/bash

# Reset the contents of an /share/alma-pipeline/soa-dir/TIMESTAMP_DIRNAME such that 
# only the originally harvested XML file(s) remain

if [ -z "$1" ]; then
  echo "TIMESTAMP_DIRNAME argument (YYYY-mm-dd-HH-MM-SS) is required"
  exit
fi

ALMA_PIPELINE_SHARE="/share/alma-pipeline"

if [ ! -e "$ALMA_PIPELINE_SHARE/soa-enrich/$1" ]; then
  echo "$1 does not exist. exiting..."
fi

cd $ALMA_PIPELINE_SHARE/soa-enrich/$1
rm -f .argot deletes*/* enrich*/* enrich*/.processed enrich*/.locked errors*/* formatted*/* marc*/* procStats*/* stats*/*

echo "done"
