#!/bin/bash

shift_timestamp_queue () {
    local filename="$1"

    if [ -z "$filename" ]; then
        echo "no 'queue' file detected. exiting..."
        exit
    fi

    mapfile -t queue < $ALMA_PIPELINE_SHARE/timestamps/${filename}-queue.txt
    echo "removing ${queue[0]} from timestamps/${filename}-queue.txt..."
    unset queue[0]

    # ... and write the remaining items back to argot-queue.txt

    # only include a newline "\n" when array elements remain.
    # otherwise, we'll run the risk of having an empty element at the top 
    # of 'argot-queue.txt', and as a result, halting the pipeline operation   
    newline_trail=""
    [ ${#queue[@]} -gt 0 ] && newline_trail="\n"
    printf "%s$newline_trail" "${queue[@]}" > $ALMA_PIPELINE_SHARE/timestamps/${filename}-queue.txt
}
