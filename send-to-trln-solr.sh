#!/bin/bash

# send-to-trln-solr.sh
# originating author: Derrek Croney (dlc32)
# contributing author(s): Stewart Engart (hse9)

# Get the basename for logging purposes
. ./scripts/liblog.sh

filename=$(basename "$0")

# Process flags first (getopt)
SHORTOPTS="o:A:S:bsdf:I:i:CT:B"
LONGOPTS="outputdir:,argot-dir:,spofford-cfg:,bypass-ingest,skip-argot,dry-run,file-ext,ingest-interval:,ingest-batch-count:,show-config,stop-ingest-at:,disable-batch"
OPTS=`getopt -o ${SHORTOPTS} -l ${LONGOPTS} -n '${filename}' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1; fi

eval set -- "$OPTS"

# Not certain we need OUTPUTDIR since the actual (spofford) output directory
# is specified in the designated config file (and that is taking precedent)
OUTPUTDIR="."

# By default for spofford config files, use a config file that 
# points to either a locally-hosted instance, or a sandbox instance.
# 
# Use the "--spofford-cfg (or -S) to use a different config
SPOFFORD_CONFIG=".spofford-client.yml"

# The 'argot' directory is relative to $WHERE (aka the argument passed to us)
ARGOTDIR="generated-argot"

# When ingesting argot files, this is the default extension for our file (search) glob
FILE_EXT="json"

ALMA_PIPELINE_SHARE="/share/alma-pipeline"

# FLAGs
BYPASS_INGEST=false
SKIP_ARGOT=false
DRY_RUN=false
INGEST_INTERVAL="20m"
INGEST_BATCH_COUNT=5
DISABLE_BATCH=false
STOP_INGEST_AT=false
SHOW_CONFIG=false

while true; do
  case "$1" in
    -o | --outputdir ) OUTPUTDIR="$2"; shift 2 ;;
    -A | --argot-dir ) ARGOTDIR="$2"; shift 2;;
    -d | --dry-run ) DRY_RUN=true; shift ;;
    -f | --file-ext ) FILE_EXT="$2"; shift 2;;
    -b | --bypass-ingest ) BYPASS_INGEST=true; shift ;;
    -s | --skip-argot ) SKIP_ARGOT=true; BYPASS_INGEST=false; shift ;;
    -S | --spofford-cfg ) SPOFFORD_CONFIG="$2"; shift 2;;
    -I | --ingest-interval ) INGEST_INTERVAL="$2"; shift 2;;
    -i | --ingest-batch-count ) INGEST_BATCH_COUNT="$2"; shift 2;;
    -B | --disable-batch ) DISABLE_BATCH=true; shift;;
    -C | --show-config ) SHOW_CONFIG=true; shift;;
    -T | --stop-ingest-at ) STOP_INGEST_AT=$(date -d "$2" +%s); shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

# Currently, this Bash script will process XML files located in 
# the directory provided in the first argument $1

# $1 represents the parent directory holding XML files

WHERE=$1

info "output directory = [$OUTPUTDIR]"
info "skip-argon = [$SKIP_ARGOT]"
info "bypass-ingest = [$BYPASS_INGEST]"
info "Spofford Config = [$SPOFFORD_CONFIG]"
info "Argot files (relatively) located at : ${ARGOTDIR}"
[ ! "$DISABLE_BATCH" == true ] && info "Ingest Batch Count = [$INGEST_BATCH_COUNT]" && info "Ingest (batch) interval = [$INGEST_INTERVAL]"
[ ! "$BYPASS_INGEST" == true ] && [ ! "$STOP_INGEST_AT" == false ] && info "Stop Ingest at = [$(date -d @${STOP_INGEST_AT})]"
info "WHERE = [$WHERE]"

[ "$SHOW_CONFIG" == true ] && exit 0;

# Adjust the Input File Separator so we can glob a file listing
OFS=$IFS
IFS=$'\n'

if [ "$SKIP_ARGOT" = false ]; then

  # starting with the 2024-09-18-09-00-00 data load, 
  # the 'ingest-queue' directory includes a "formatted-xml" 
  # directory where all formatted XML files are sent to.
  #
  # make sure we don't include those XML files in this search.
  paths=($(find $WHERE -name "*.xml" ! -path "*formatted-xml/*" ! -path "*xml-processed/*"))

  # Create an "argot" folder if needed
  [ ! -d "$WHERE/$ARGOTDIR" ] && mkdir -m 0777 $WHERE/$ARGOTDIR

  # Create a "formatted-xml" folder if needed
  [ ! -d "$WHERE/formatted-xml" ] && mkdir -m 0777 $WHERE/formatted-xml

  info "processing ${#paths[@]} XML files / starting..."

  # We'll create two(2) "linted" XML files from the original enriched XML
  # 1) a non-human-formatted file, used for marc-to-argot transformation,
  #    to be removed after MTA
  # 2) a human-formatted file, one we'll "keep" for our "records"
  counter=1
  how_many_files=${#paths[@]}
  for (( i = 0; i < ${#paths[@]} ; i++ )); do
    info "processing ${paths[$i]}..."
    basen=`basename ${paths[$i]}`

    # Make sure to remove the '.xml' part of the filename
    # Doing this allows the ingest app to recognize the appropriate
    # routing
    argot_fname="add-${basen//\.xml/}.json"

    info "[$filename] [$counter of $how_many_files] xmllint processing ${paths[$i]} / resulting file: $WHERE/xmllint-${basen}...\n"

    # We're using the non-formatted, but linted, XML file for MTA
    # and we'll remove it later
    outp=`xmllint --recover --output $WHERE/xmllint-${basen} ${paths[$i]}`

    # Make sure any <collection xmlns=""> strings are corrected
    sed -i 's|xmlns=""|xmlns="http://www.loc.gov/MARC21/slim"|g' $WHERE/xmllint-${basen}

    info "[$filename] mta processing file: $WHERE/xmllint-${basen} / resulting file: $WHERE/$ARGOTDIR/$argot_fname ...\n"
    outp=`mta create duke $WHERE/xmllint-${basen} $WHERE/$ARGOTDIR/$argot_fname`
    echo $outp

    debug "removing linted file: xmllint-${basen}..."
    rm $WHERE/xmllint-${basen}

    # But, we are keeping the linted and formatted copy for later reference
    # Place the resulting file in the 'formatted-xml' directory
    outp=`xmllint --recover --format --output $WHERE/formatted-xml/${basen} ${paths[$i]}`
    sed -i 's|xmlns=""|xmlns="http://www.loc.gov/MARC21/slim"|g' $WHERE/formatted-xml/${basen}

    ((counter++))
  done
fi

if [ "$BYPASS_INGEST" = true ]; then
  # Restore the IFS
  IFS=$OFS
  exit 0
fi

paths=($(find $WHERE/$ARGOTDIR -name "*.${FILE_EXT}"))
info "[$filename] processing ${#paths[@]} JSON files / starting..."

success_pattern="Upload accepted"
counter=1
how_many_files=${#paths[@]}

last_file_processed=""

for i in "${paths[@]}"
do
  # if NOW is after STOP_INGEST_AT, then stop ingesting...
  NOW=$(date +%s)
  if [ ! "$STOP_INGEST_AT" == false ] && [ $NOW -gt $STOP_INGEST_AT ]; then
    info "[$filename] stop ingest at $(date -d @${STOP_INGEST_AT})"
    break
  fi

  # Call 'spofford ingest' with the ":output" options, placing the resulting 
  # file in the Alma share area
  # ----------------------------------
  info "[$filename] [$counter of $how_many_files] $spofford ingest --verbose --config=${SPOFFORD_CONFIG} $i"
  if [ "$DRY_RUN" = false ]; then
    # Run "spofford ingest", and test for successful upload.
    outp=`spofford ingest --verbose --config=${SPOFFORD_CONFIG} $i`
    if [[ "$outp" =~ $success_pattern ]]; then
      info "[$filename] UPLOADED $i"
    else
      error "[$filename] FAILED UPLOAD on $i"
      echo $outp
    fi
    #printf "[$filename] $outp\n"
  else
    # Display what would be the call made to "spofford ingest", but don't run it
    info "[$filename][DRY-RUN] spofford ingest --verbose --config=${SPOFFORD_CONFIG} $i"
  fi

  # Keep track of the last file processed
  # (this will be reported at the end of the run)
  last_file_processed=$(basename $i)

  info "[$filename] done with $i..."

  if [[ ! "$DISABLE_BATCH" == true ]]; then
    mod=$(($counter % INGEST_BATCH_COUNT))
    [ $mod -eq 0 ] && warn "Waiting ${INGEST_INTERVAL} before running next batch of records..." && sleep ${INGEST_INTERVAL}
  fi
  ((counter++))
done

IFS=$OFS
info "[$filename] processing done..."

# Tell us the last "ingest" file process, when applicable
[ "$BYPASS_INGEST" == false ] && echo "Last file processed: $last_file_processed"

# we really don't need this, because it's implied, but...
exit

