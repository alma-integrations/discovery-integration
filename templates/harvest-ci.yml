can run harvest:
  stage: harvest
  # this 'run_harvest' job only runs when triggered by a web request or schedule
  rules:
    - if: '$HARVEST == null'
      when: never
    - if: $CI_PIPELINE_SOURCE =~ /(api|trigger|web|schedule)/
  tags:
    - harvest

  # verify no other "harvest" job is running 
  # by inspecting a marker file of some sort
  script:
    - if [ -e $ALMA_PIPELINE_DIR/run/harvest.pid ]; then echo "Another job ($(cat $ALMA_PIPELINE_DIR/run/harvest.pid)) is currently running. Skipping."; exit 1; fi

run harvest:
  stage: harvest

  needs: 
    - "can run harvest"

  # this 'run_harvest' job only runs when triggered by a web request or schedule
  rules:
    - if: '$HARVEST == null'
      when: never
    - if: $CI_PIPELINE_SOURCE =~ /(api|trigger|web|schedule)/

  # added 7/25/2024
  # if a harvest job takes more than 12 hours:
  # - the job will "appear" to fail, however
  # - the job will continue to run
  # - the logging provided by a pipeline job will stop
  #
  # - for those with access to lsis-ci-01.lib.duke.edu, 
  #   you can "docker ps" to verify that the job is still running
  #   and use "docker logs -f -n1000 <service_name>" to view logs
  timeout: 24 hours

  variables:
    # See: Settings > CI/CD > Variables
    REF_FILE_VAR: $aws_credentials

  # This is how the "server environment" looks when we get to 'script':
  # - running on lsis-ci-01.lib.duke.edu
  # - running under the /home/gitlab-runner directory
  # 
  # TODO: place the contents of this "script" block in a separate *.sh file
  #       OR - use a CI template
  script:

    # Allow for CHANGES_FROM to be provided by a web triggered process
    # 
    # Read CHANGES_FROM from the previously written 'next-soa-run.txt'
    - if [ -e $ALMA_PIPELINE_DIR/timestamps/last-harvest.txt ]; then CHANGES_FROM=$(cat $ALMA_PIPELINE_DIR/timestamps/last-harvest.txt); fi

    # If, for some reason, CHANGES_FROM is not set (above), default to the top of 2024
    - CHANGES_FROM=${CHANGES_FROM:-"2024-01-01T00:00:00Z"}
 
    # Developer transparency and/or debugging
    # Show the world what date was selected.

    # copy the AWS credentials file to the runner's current working directory
    - cp $aws_credentials .
    
    # We don't care about the previous tokens, so clear 'em out
    - rm -f ${ALMA_PIPELINE_DIR}/tokens/*

    # if TIMESTAMP is provided in the "New Pipeline" form, use it, othewise, 
    # create a timestamp string here and pass it to `print-harvest-compose.sh`
    # format: YYYY-mm-dd-HH-MM-SS
    #
    # Then place that timestamp value into our "Hey, I'm running" marker file
    # (see 'can run harvest' job above)
    # DEPRACATED LINE- TIMESTAMP=${TIMESTAMP:-$(date +%Y-%m-%d-%H-%M-%S)}
    - NOW_EPOCH=$(date -d 'now' +%s)
    - TIMESTAMP_DIRNAME=${TIMESTAMP_DIRNAME:-$(date -d @${NOW_EPOCH} +"%Y-%m-%d-%H-00-00")}
    - TIMESTAMP_TZ=${TIMESTAMP_TZ:-$(date -d @${NOW_EPOCH} +"%Y-%m-%dT%H:00:00Z")}

      #- date_in_tz=$(date -d @$(date -d 'now' +%s) +"%Y-%m-%dT%H:00:00Z")
      #- TIMESTAMP=${TIMESTAMP:-$(date -d 'now'  +"%Y-%m-%dT%H:00:00Z")}
    - echo ${TIMESTAMP_DIRNAME} > ${ALMA_PIPELINE_DIR}/run/harvest.pid

    - "echo TIMESTAMP_TZ: ${TIMESTAMP_TZ}"
    - "echo OAI_SET: ${OAI_SET:-trln_discovery_spec}"
    - "echo CHANGES_FROM: ${CHANGES_FROM}"
    - "echo TIMESTAMP_DIRNAME: $TIMESTAMP_DIRNAME"

    # Create a docker-compose file (on the fly) as well as creating a timestamp directory 
    # under ${ALMA_PIPELINE_DIR}/harvest
    #
    # CREATE TEH TIMESTAMP_DIRNAME folder (YYYY-mm-dd-HH-MM-SS format)
    - mkdir -m 0777 $ALMA_PIPELINE_DIR/harvest/${TIMESTAMP_DIRNAME}

    - ./print-harvest-compose.sh --timestamp $TIMESTAMP_DIRNAME --no-create-dir --oai-set ${OAI_SET:-trln_discovery_spec} --changes-from $CHANGES_FROM --output-file docker-compose-harvest.yml

    # RUN THE HARVEST JOB by way of the docker container
    # Build the docker image, the run it (using docker-compose-harvest.yml as context)
    - docker compose -f docker-compose-harvest.yml up --remove-orphans harvest-ci --exit-code-from harvest-ci
    # do we need/want to remove docker-compose-harvest.yml

    - |-
      # Run harvest XML file corrections (like removing errant `<record></record>`
      # per "hse9" (Stewart)
      sed -i '/<record><\/record>/d' ${ALMA_PIPELINE_DIR}/harvest/${TIMESTAMP_DIRNAME}/*.xml
      
      # call make-soa-dirs.sh to generate the directories needed by the subsequent "enrich" job
      mkdir -m 0777 ${ALMA_PIPELINE_DIR}/soa-enrich/${TIMESTAMP_DIRNAME}
      mkdir -p -m 0777 ${ALMA_PIPELINE_DIR}/formatted-xml/${TIMESTAMP_DIRNAME}
      # mkdir -m 0777 ${ALMA_PIPELINE_DIR}/logs/${TIMESTAMP_DIRNAME}

      ./make-soa-dirs.sh ${ALMA_PIPELINE_DIR}/harvest/${TIMESTAMP_DIRNAME} ${ALMA_PIPELINE_DIR}/soa-enrich/${TIMESTAMP_DIRNAME}

      # This step identifies the folders to be processed by each of the 
      # ten(10) "loader" VMs
      ./make-payload-dirs.pl ${TIMESTAMP_DIRNAME} -S soa-enrich

      # Place the "TIMESTAMP" value in a 'marker' file
      echo $TIMESTAMP_DIRNAME > ./next-soa-run.txt
      echo $TIMESTAMP_DIRNAME > ${ALMA_PIPELINE_DIR}/timestamps/next-soa-run.txt
      echo $TIMESTAMP_DIRNAME >> $ALMA_PIPELINE_DIR/timestamps/enrich-queue.txt

    - echo $TIMESTAMP_TZ > ${ALMA_PIPELINE_DIR}/timestamps/last-harvest.txt
    - echo $TIMESTAMP_TZ > ./last-harvest.txt

    - "echo TIMESTAMP_TZ: ${TIMESTAMP_TZ} >> ./job-params.txt"
    - "echo OAI_SET: ${OAI_SET:-trln_discovery_spec} >> ./job-params.txt"
    - "echo CHANGES_FROM: ${CHANGES_FROM} >> ./job-params.txt"

    - "echo 'Timestamp directories: $ALMA_PIPELINE_DIR/harvest/${TIMESTAMP_DIRNAME}'"
    - "echo '$ALMA_PIPELINE_DIR/soa-enrich/${TIMESTAMP_DIRNAME}'"

  after_script:
    - rm ${ALMA_PIPELINE_DIR}/run/harvest.pid

  tags:
    - harvest

  artifacts:
    paths:
      - last-harvest.txt
      - docker-compose-harvest.yml
      - job-params.txt

