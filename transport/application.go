package main

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.oit.duke.edu/dul-go/journal"
)

// Application represents a application's context
type Application struct {
	CLI    *CommandLineService
	Config *Config

	shellOutput []string
}

// NewApplication returns a new app context
// Receives a CLI service and Config
func NewApplication(cli *CommandLineService, config *Config) *Application {
	journal.InitLogging(os.Stdout, os.Stdout, os.Stdout, os.Stdout)
	return &Application{
		CLI:    cli,
		Config: config,
	}
}

// ShellOutput returns any relevant output captured by the
// shell commands
func (app *Application) ShellOutput() []string {
	return app.shellOutput
}

// Verify Commands
func (a *Application) VerifyCommands() error {
	cmds := []string{"xmllint", "mta", "argot", "spofford"}
	for _, c := range cmds {
		ec := exec.Command("which", c)
		_, err := ec.Output()
		if err != nil {
			return fmt.Errorf("[VerifyCommands] %s was not found on this system.\n", c)
		}
	}
	return nil
}

// DoXMLLint calls 'xmllint' on the "inFile", rewriting it to "outFile"
func (app *Application) DoXMLLint() {
	// xmllint --recover --output
	shellCommand := "xmllint"
	p1 := "--recover"
	p2 := "--output"

	cmd := exec.Command(shellCommand, p1, p2, "", "")
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// Print the output
	fmt.Println(string(stdout))
}

// ValidateArgot ensures the argot received post "marc-to-argot"
// is valid.
func (app *Application) ValidateArgot() {
	// argot validate -qv
	shellCommand := "argot validate"
	p1 := "-qv"

	cmd := exec.Command(shellCommand, p1, argotInfile)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// Print the output
	fmt.Println(string(stdout))
}

// SendToSpofford invokes the spofford client (hosted on our side)
// sending the in[Zip?]file over to the spofford server
func (app *Application) SendToSpofford() {
	// spofford ingest --config
	shellCommand := "spofford ingest"
	p1 := "--config"

	cmd := exec.Command(shellCommand, p1, spoffordConfig, spoffordInfile)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// Print the output
	fmt.Println(string(stdout))
}

// TransformMarcToArgot ..
func (app *Application) TransformMarcToArgot() {
	// mta create duke
	shellCommand := "mta create"
	p1 := "duke"

	cmd := exec.Command(shellCommand, p1, mtaInfile, mtaOutfile)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// Print the output
	fmt.Println(string(stdout))
}
