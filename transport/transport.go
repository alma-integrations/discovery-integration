package main

import (
	"fmt"
	"os"
	"strings"

	"go.uber.org/dig"
)

const xmllintOutfile = "out"
const xmllintInfile = "in"

const argotInfile = "in"

const spoffordInfile = "in"
const spoffordConfig = "config"

const mtaOutfile = "out"
const mtaInfile = "in"

// BuildContainer builds all startup dependencies
func BuildContainer() *dig.Container {
	container := dig.New()

	// Add dependent services, etc here via
	// container.Provide(...)

	container.Provide(NewCommandLineService)
	container.Provide(NewConfig)
	container.Provide(NewApplication)

	return container
}

// DoNothing really does nothing
func DoNothing() {
	fmt.Println("I did nothing of the sort!")
}

func main() {
	// build our dependency chain
	container := BuildContainer()

	// The functions referred to below are located in commands.go
	err := container.Invoke(func(app *Application) error {
		// You can invoke (call) a function like 'DoNothing()'...
		DoNothing()

		err := app.VerifyCommands()
		if err != nil {
			return err
		}

		// ..or you can call the methods on the application.
		// At this point, the app has access to the config and
		// command-line data
		app.DoXMLLint()
		app.TransformMarcToArgot()
		app.ValidateArgot()
		app.SendToSpofford()

		// Perhaps we've captured all the shell commands' output
		// and can access it from the app(lication). Then we could
		// something like this:
		myFancyOutput := app.ShellOutput()
		fmt.Println(strings.Join(myFancyOutput[:], "\n"))

		return nil
	})

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
