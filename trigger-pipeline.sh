#!/bin/bash

set -e

ALMA_PIPELINE_SHARE="/share/alma-pipeline"

case $1 in
AVA)
    curl -X POST \
        --fail \
        -F token=TOKEN \
        -F ref=main \
        -F "variables[AVA]=yes" \
        -F "variables[NO_BUILD]=yes" \
        https://gitlab.oit.duke.edu/api/v4/projects/PROJECT_ID/trigger/pipeline
    ;;

HARVEST)
    curl -X POST \
        --fail \
        -F token=TOKEN \
        -F ref=main \
        -F "variables[HARVEST]=yes" \
        -F "variables[NO_BUILD]=yes" \
        https://gitlab.oit.duke.edu/api/v4/projects/PROJECT_ID/trigger/pipeline
    ;;

ENRICH)
    # Perhaps we read the first line from /share/alma-pipeline/enrich-queue.txt, then
    # see if there are any ".locked" files in any of the enriched* directories
    mapfile -t enrich_queue < $ALMA_PIPELINE_SHARE/timestamps/enrich-queue.txt
    TIMESTAMP_DIRNAME="${enrich_queue[0]}"
    echo "TIMESTAMP_DIRNAME=[$TIMESTAMP_DIRNAME]"

    # under /$ALMA_PIPELINE_SHARE/soa-enrich/$TIMESTAMP_DIRNAME, each "enrichNN" folder will contain
    # a .locked "marker" file when a harvest is currently running "availability enrichment" (soa-enrich)
    locked_files=($(find $ALMA_PIPELINE_SHARE/soa-enrich/$TIMESTAMP_DIRNAME -type f -name "*.locked"))
    if [ ${#locked_files[@]} -gt 0 ]; then
        echo "Jobs are still running for the $TIMESTAMP_DIRNAME batch. Exiting"
        exit 0
    fi

    # if each enrichNN folder has a '.processed' file, then we're done with this harvest batch
    enrich_folders=($(find $ALMA_PIPELINE_SHARE/soa-enrich/$TIMESTAMP_DIRNAME -type d -name "enrich*"))
    processed_files=($(find $ALMA_PIPELINE_SHARE/soa-enrich/$TIMESTAMP_DIRNAME -type f -name "*.processed"))
    if [ ${#processed_files[@]} -ne 0 ] && [ ${#enrich_folders[@]} -eq ${#processed_files[@]} ]; then
        # create an array of date strings
        mapfile -t enrich_queue < $ALMA_PIPELINE_SHARE/timestamps/enrich-queue.txt

        # pop the first element...
        # send the first element to 'argot-queue.txt
        echo ${enrich_queue[0]} >> $ALMA_PIPELINE_SHARE/timestamps/argot-queue.txt
        # now unset the first element
        unset enrich_queue[0]

        # ... and write the remaining items back to enrich-queue.txt

        # only include a newline "\n" when array elements remain.
        # otherwise, we'll run the risk of having an empty element at the top
        # of 'enrich-queue.txt', and as a result, halting the pipeline operation
        newline_trail=""
        [ ${#enrich_queue[@]} -gt 0 ] && newline_trail="\n"
        printf "%s$newline_trail" "${enrich_queue[@]}" > $ALMA_PIPELINE_SHARE/timestamps/enrich-queue.txt
    fi

    curl -X POST \
        --fail \
        -F token=TOKEN \
        -F ref=main \
        -F "variables[ENRICH]=yes" \
        -F "variables[NO_BUILD]=yes" \
        https://gitlab.oit.duke.edu/api/v4/projects/PROJECT_ID/trigger/pipeline
    ;;

*)
    echo -n "Unknown pipeline: $1"
    ;;
esac
